@extends('layouts.user')
@section('content')

<!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-6 col-8 align-self-center">
                        <h3 class="text-themecolor mb-0 mt-0">Hlavní nabídka</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Domů</a></li>
                            <li class="breadcrumb-item active">Hlavní nabídka</li>
                        </ol>
                    </div>
                    {{-- <div class="col-md-6 col-4 align-self-center">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle btn-sm float-right ml-2"><i class="ti-settings text-white"></i></button>
                        <button class="btn float-right hidden-sm-down btn-success"><i class="mdi mdi-plus-circle"></i> Create</button>
                        <div class="dropdown float-right mr-2 hidden-sm-down">
                            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> January 2019 </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton"> <a class="dropdown-item" href="#">February 2019</a> <a class="dropdown-item" href="#">March 2019</a> <a class="dropdown-item" href="#">April 2019</a> </div>
                        </div>
                    </div> --}}
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->
                {{--if user doesnt have a membership plan, here should contain all membership and premium tips for the day--}}


                                    @if(count($active_memberships))
                                        @if(isset($grouped_tips))
                                            @if(count($grouped_tips))

                                                @foreach($grouped_tips as $tips)

                                                    <div class="row">
                                                        <div class="col-lg-10 m-auto">
                                                            <div class="card">
                                                                <div class="card-body">
                                                                    <div class="d-flex no-block">
                                                                        <h4 class="card-title">{{$tips[0]->package->name}} </h4>

                                                                    </div>
                                                                    <div class="table-responsive mt-5">
                                                                        <table class="table stylish-table">
                                                                            <thead>
                                                                            <tr>
                                                                                <th colspan="2">Název tipu</th>
                                                                                <th>Kurz</th>
                                                                                <th>Náš tip</th>
                                                                                <th>Datum a čas</th>
                                                                                <th>Důvěra</th>

                                                                            </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                @foreach($tips as $tip)
                                                                                    {{-- @if(!$tip->single_purchase_tip) --}}


                                                                                        <tr class="active">
                                                                                            <td colspan="2">{{$tip->name}}</td>
                                                                                            <td>
                                                                                                {{$tip->course}}
                                                                                            </td>
                                                                                            <td>{{$tip->tip}}</td>
                                                                                        <td>{{getDateFormat($tip->date, 'd-M-Y')}}  <span style='font-size:12px'>{{getDateFormat($tip->time, 'h:i A')}}</span></td>
                                                                                            <td>{{$tip->confidence}}%</td>
                                                                                        </tr>
                                                                                    {{-- @endif --}}
                                                                                @endforeach

                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>

                                                @endforeach


                                            @else
                                            <div class="row">
                                                <div class="col-lg-10 m-auto">
                                                    <div class="card">
                                                            <div class="card-body">
                                                                <div class="d-fle no-block text-center ">
                                                                    <h4 class="card-title w-100">There are currently no tips</h4>
                                                                    <small>Tips will be posted soon.</small>

                                                                </div>
                                                            </div>

                                                    </div>
                                                </div>
                                            </div>



                                            @endif



                                        @endif
                                    @else
                                     <div class="row">
                                                <div class="col-lg-10 m-auto">
                                                    <div class="card">
                                                            <div class="card-body">
                                                                <div class="d-fle no-block text-center ">
                                                                    <h4 class="card-title w-100">Momentálně nemáte aktivovaný účet</h4>
                                                                    <small>pro zpřístupnění tipů aktivujte účet
                                                                            Upgrade now - aktivovat účet
                                                                            </small>

                                                                </div>
                                                                <div>
                                                                    <div class="alert alert-default text-center">
                                                                    <a class="btn btn-lg btn-success" href="{{route('user.upgrade')}}">Upgrade Now</a>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                    </div>
                                                </div>
                                        </div>
                                    @endif



                                    @if(isset($grouped_premium_tips))
                                        @if(count($grouped_premium_tips))
                                        @foreach($grouped_premium_tips as $tips)

                                        <div class="row">
                                            <div class="col-lg-10 m-auto">
                                                <div class="card">
                                                    <div class="card-body">
                                                        <div class="d-fle no-block">
                                                            <h4 class="card-title">{{$tips[0]->package->name}}</h4>
                                                            <small>Pro zobrazení tipů je nutné aktivovat balíček  <strong>{{strtoupper($tips[0]->package->name)}}</strong> druhá možnost je zakoupit samostatný tip z balíčku jednorázovou platbou přes tlačítko zakoupit.</small>

                                                            </div>
                                                        <div class="table-responsive mt-5">
                                                            <table class="table stylish-table">
                                                                <thead>
                                                                <tr>
                                                                    <th colspan="2">Název tipu</th>
                                                                    <th>Kurz</th>
                                                                    <th>Náš tip</th>
                                                                    <th>Datum a čas</th>
                                                                    <th>Důvěra</th>
                                                                    <th>Action</th>

                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                    @foreach($tips as $tip)
                                                                    @if($tip->single_purchase_tip)



                                                                            <tr class="active">
                                                                                <td colspan="2">{{$tip->name}}</td>
                                                                                <td>
                                                                                    @if(checkIfBought($tip->id)) <i class="fa fa-lock-open"></i>  {{$tip->course}} @else <i class="fa fa-lock"></i> @endif
                                                                                </td>
                                                                                <td> @if(checkIfBought($tip->id)) <i class="fa fa-lock-open"></i>  {{$tip->tip}} @else <i class="fa fa-lock"></i> @endif</td>
                                                                            <td>{{getDateFormat($tip->date, 'd-M-Y')}}  <span style='font-size:12px'>{{getDateFormat($tip->time, 'h:i A')}}</span></td>
                                                                                <td>{{$tip->confidence}}%</td>
                                                                                <td>
                                                                                @if(!checkIfBought($tip->id))
                                                                                <button data-id="{{$tip->id}}" data-amount="{{base64_encode($tip->amount)}}" data-href="{{route('payment.tip.store',$tip->id)}}" class='btn buy-tip btn-xs btn-success'>Zakoupit</button>
                                                                                @else
                                                                                    <a class='btn btn-xs btn-link'>Zakoupeno</a>
                                                                                @endif
                                                                            </td>

                                                                @endif
                                                                    @endforeach

                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>

                                    @endforeach

                                            {{-- @foreach($grouped_premium_tips as $tip)
                                            <div class="row">
                                                    <div class="col-lg-10 m-auto">
                                                        <div class="card">
                                                            <div class="card-body">
                                                @if($tip->single_purchase_tip)
                                                    <div class="d-fle no-block">
                                                    <h4 class="card-title">{{$tip->package->name}}</h4>
                                                    <small>You can purchase a premium tip if you do not want to upgrade</small>

                                                    </div>
                                                    <div class="table-responsive mt-5">
                                                        <table class="table stylish-table">
                                                            <thead>
                                                            <tr>
                                                                <th colspan="2">Název tipu</th>
                                                                <th>Kurz</th>
                                                                <th>Náš tip</th>
                                                                <th>Datum a čas</th>
                                                                <th>Důvěra</th>
                                                                <th>Action</th>

                                                            </tr>
                                                            </thead>
                                                            <tbody>

                                                            <tr class="active">
                                                                <td colspan="2">{{$tip->name}}</td>
                                                                <td>
                                                                    @if(checkIfBought($tip->id)) <i class="fa fa-lock-open"></i>  {{$tip->course}} @else <i class="fa fa-lock"></i> @endif
                                                                </td>
                                                                <td> @if(checkIfBought($tip->id)) <i class="fa fa-lock-open"></i>  {{$tip->tip}} @else <i class="fa fa-lock"></i> @endif</td>
                                                            <td>{{getDateFormat($tip->date, 'd-M-Y')}}  <span style='font-size:12px'>{{getDateFormat($tip->time, 'h:i A')}}</span></td>
                                                                <td>{{$tip->confidence}}%</td>
                                                                <td>
                                                                @if(!checkIfBought($tip->id))
                                                                <a href="{{route('payment.tip.store',$tip->id)}}" class='btn btn-xs btn-success'>Purchase</a>
                                                                @else
                                                                    <a class='btn btn-xs btn-link'>Purchased</a>
                                                                @endif
                                                            </td>
                                                            </tr>

                                                            </tbody>
                                                        </table>
                                                    </div>

                                                @endif

                                            </div>

                                        </div>
                                    </div>
                                </div>

                                            @endforeach --}}


                                        @endif



                                    @endif







<div class="modal fade " style="background:#433f3fd9" tabindex="-1" role="dialog" id='paypall' aria-labelledby="myLargeModalLabel" aria-hidden="true" >
    <div class="modal-dialog modal-dialog-centered" >
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" >Select a Payment Method</h4>

                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">


                <div id="paypal-button-container"></div>

            </div>
            {{-- <div class="modal-footer">
                <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
            </div> --}}
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<form id='payment_form' method="POST">
    @csrf
    <input type="hidden" name='tip_id'>
    <input type="hidden"  name="order_id" />
    {{-- <input type="submit" /> --}}
</form>


@endsection

@push('scripts')
    <script src="https://www.paypal.com/sdk/js?client-id={{config('paypal.key')}}&vault=true&currency=CZK">
    </script>


    <script>
        $(document).ready(function(){
            $(document).on('click', '.buy-tip', function(){
                $('#paypal-button-container').html('');
                $('#lev').text($(this).attr('data-name'));
                $('#paypall').modal();
                //get tip amount
                var amt = atob($(this).attr('data-amount'));
                var tip = $(this).attr('data-id');
                paypal.Buttons({
                    createOrder: function(data, actions) {
                        // Set up the transaction
                        return actions.order.create({
                            purchase_units: [{
                                amount: {
                                    value: amt,

                                }
                            }]
                        });
                    },
                    onApprove: function(data, actions) {
                        console.log(data);
                        $.toast({
                            heading: 'Success',
                            text: 'Payment was successful. Please hold on.',
                            position: 'top-right',
                            loaderBg:'#ff6849',
                            icon: 'success',
                            hideAfter: 3500,
                            stack: 6
                        });
                        return actions.order.capture().then(function(details) {
                            // Show a success message to your buyer
                            console.log(details);
                            $('[name=order_id]').val(data.orderID);
                            $('[name=tip_id]').val(tip);
                            u = '{{route('payment.tip.store')}}/'+tip+'/'+data.orderID;
                            $('#payment_form').attr('action', u);
                            $('#payment_form').submit();
                            // alert('Transaction completed by ' + details.payer.name.given_name);
                        });
                        // give value to customer


                        // alert('You have successfully created subscription ' + data.subscriptionID);
                    }
                }).render('#paypal-button-container');
            })

        });

    </script>
@endpush
