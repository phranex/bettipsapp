@extends('layouts.user')
@section('content')
<div class="row page-titles">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="text-themecolor mb-0 mt-0">Invoice</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Invoice</li>
            </ol>
        </div>

    </div>
<div class="row">
        <div class="col-md-12">
            <div class="card card-body printableArea">
                <h3><b>INVOICE</b> <span class="float-right">#5669626</span></h3>
                <hr>
                <div class="row">
                    <div class="col-md-12">
                        <div class="float-left">
                            <address>
                                <h3> &nbsp;<b class="text-danger">Monster Admin</b></h3>
                                <p class="text-muted ml-1">E 104, Dharti-2,
                                    <br> Nr' Viswakarma Temple,
                                    <br> Talaja Road,
                                    <br> Bhavnagar - 364002</p>
                            </address>
                        </div>
                        <div class="float-right text-right">
                            <address>
                                <h3>To,</h3>
                                <h4 class="font-bold">Gaala &amp; Sons,</h4>
                                <p class="text-muted ml-4">E 104, Dharti-2,
                                    <br> Nr' Viswakarma Temple,
                                    <br> Talaja Road,
                                    <br> Bhavnagar - 364002</p>
                                <p class="mt-4"><b>Invoice Date :</b> <i class="fa fa-calendar"></i> 23rd Jan 2019</p>
                                <p><b>Due Date :</b> <i class="fa fa-calendar"></i> 25th Jan 2019</p>
                            </address>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="table-responsive mt-5" style="clear: both;">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th class="text-center">#</th>
                                        <th>Description</th>
                                        <th class="text-right">Quantity</th>
                                        <th class="text-right">Unit Cost</th>
                                        <th class="text-right">Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="text-center">1</td>
                                        <td>Milk Powder</td>
                                        <td class="text-right">2 </td>
                                        <td class="text-right"> $24 </td>
                                        <td class="text-right"> $48 </td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">2</td>
                                        <td>Air Conditioner</td>
                                        <td class="text-right"> 3 </td>
                                        <td class="text-right"> $500 </td>
                                        <td class="text-right"> $1500 </td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">3</td>
                                        <td>RC Cars</td>
                                        <td class="text-right"> 20 </td>
                                        <td class="text-right"> %600 </td>
                                        <td class="text-right"> $12000 </td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">4</td>
                                        <td>Down Coat</td>
                                        <td class="text-right"> 60 </td>
                                        <td class="text-right">$5 </td>
                                        <td class="text-right"> $300 </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="float-right mt-4 text-right">
                            <p>Sub - Total amount: $13,848</p>
                            <p>vat (10%) : $138 </p>
                            <hr>
                            <h3><b>Total :</b> $13,986</h3>
                        </div>
                        <div class="clearfix"></div>
                        <hr>
                        <div class="text-right">
                            <button class="btn btn-danger" type="submit"> Proceed to payment </button>
                            <button id="print" class="btn btn-default btn-outline" type="button"> <span><i class="fa fa-print"></i> Print</span> </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection


@push('scripts')

    <Script>
            $('.learn-more').click(function(){
                $('#learn-more').modal();
            });
    </script>
@endpush
