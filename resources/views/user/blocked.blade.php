@extends('layouts.user')
@section('content')

<div class="row">
    <!-- Column -->
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                   <div class="jumbotron">
                        <h1>
                            Your access has been restricted. Please contact support.
                        </h1>
                   </div>
                </div>
            </div>
        </div>
    </div>


@endsection
