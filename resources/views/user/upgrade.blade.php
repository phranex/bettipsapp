@extends('layouts.user')
@section('content')

<div class="row page-titles">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="text-themecolor mb-0 mt-0">Druh členství</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Domů</a></li>
                <li class="breadcrumb-item active">Druh členství</li>
            </ol>
        </div>


    </div>
    <div class="row">
            <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">balíčku</h4>
                            {{-- <h6 class="card-subtitle">Add<code>.table-bordered</code>for borders on all sides of the table and cells.</h6> --}}
                            <div class="table-responsive">
                                <table class="table table-bordered no-wrap">
                                    <thead>
                                        <tr>
                                            <th>Název balíčku</th>
                                            <th>Typ</th>

                                            <th class="text-nowrap">Akce</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(isset($packages))
                                            @if(count($packages))
                                                @foreach ($packages as $package)
                                                    <tr>
                                                            <td>{{$package->name}}</td>
                                                            <td>
                                                                {{$package->description}}
                                                                {{-- <div class="progress progress-xs margin-vertical-10 ">
                                                                    <div class="progress-bar bg-danger" style="width: 35%; height:6px;"></div>
                                                                </div> --}}
                                                            </td>

                                                            <td class="text-nowrap">
                                                            <a href="#" data-id='{{$package->id}}' data-name='{{$package->name}}' data-toggle="tooltip"  class='learn-more' data-original-title="Close"> <i class="fa fa-close text-danger"></i>AKTIVOVAT členství</a>


                                                            </td>
                                                    </tr>
                                                @endforeach
                                            @else
                                                    <tr>
                                                        <td class='alert alert-info' colspan="3"> No Package created yet</td>
                                                    </tr>

                                            @endif
                                        @endif

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
    </div>


    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" id='learn-more' aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog modal-xl">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="package_name"></h4>

                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">

                            <div class="table-responsive">
                                    <table class="table table-striped no-wrap">
                                        <thead>
                                            <tr>
                                                <th>Úroveň členství</th>
                                                <th>Doba</th>
                                                {{-- <th>Procento</th> --}}
                                                <th>Cena</th>
                                                <th class="text-nowrap">Akce</th>
                                            </tr>
                                        </thead>
                                        <tbody id='level-holder'>




                                        </tbody>
                                    </table>
                            </div>
                            {{-- <div id="paypal-button-container"></div> --}}

                    </div>
                    {{-- <div class="modal-footer">
                        <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
                    </div> --}}
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
    </div>


    <div class="modal fade " style="background:#433f3fd9" tabindex="-1" role="dialog" id='paypall' aria-labelledby="myLargeModalLabel" aria-hidden="true" >
            <div class="modal-dialog modal-dialog-centered" >
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" >Select a Payment Method to <small>Subscribe to <span id='lev' style="font-weight:bold"></span></small></h4>

                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">


                            <div id="paypal-button-container"></div>

                    </div>
                    {{-- <div class="modal-footer">
                        <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
                    </div> --}}
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
    </div>

    <form id='payment_form' method="POST">
        @csrf
        <input type="hidden" name='level_id'>
        <input type="hidden"  name="subscription_id" />
        {{-- <input type="submit" /> --}}
    </form>






@endsection
@push('scripts')

    <Script>
            $('.learn-more').click(function(){
                var package_id = $(this).attr('data-id');
                $('#level-holder').html('');
                var package_name = $(this).attr('data-name');
                $('#package_name').text(package_name);

                $('#package_id').val(package_id);
                //get all levels associated with this package
                 getLevels(package_id)
                $('#learn-more').modal();
            });

            p_url= "{{route('payment.store')}}";
            p_url2= "{{route('payment.store-credit')}}";


            $(document).on('click', '.credit', function(event){
                var link = $(this).attr('data-href');
                var bal = $(this).attr('data-balance');
                var level_price = $(this).attr('data-price');
                if(parseInt(bal) < parseInt(level_price)){
                        $.toast({
                            heading: 'Error',
                            text: 'Insufficient amount in Balance',
                            position: 'top-right',
                            loaderBg:'#ff6849',
                            icon: 'error',
                            hideAfter: 3500

                        });
                }else{
                    window.location.href = link;
                }
            });

            $(document).on('click', '.more-info', function(){

                var target = $(this).attr('data-target');
                $('.desc').not(target).hide();

                $(target).toggle();
            });




            function getLevels(id){
                var url = '{{route('package.get-levels')}}' + '?id=' +id;
                $.ajax({
                    url: url,
                    success: function(result){
                        if(result.status){
                            var html = '';
                            console.log(result);
                            if(result.data.length){
                                for(i = 0; i < result.data.length; i++){
                                var price = convert(result.data[i].price);
                                html += `<tr>
                                                <td>${result.data[i].name}</td>
                                                <td>
                                                        ${result.data[i].period} dni
                                                </td>


                                                <td>${price} </td>
                                                <td class="text-nowrap">
                                                    <button href='${p_url}/${result.data[i].id}' data-level=${result.data[i].id} data-name='${result.data[i].name}' data-plan='${result.data[i].plan_id}' class='btn btn-xs activate btn-success'> Aktivovat </button>
                                                    <button data-price='${result.data[i].price}' data-href='${p_url2}/${result.data[i].id}' data-balance='{{getUserCreditBalance(auth()->id())}}' class='btn btn-xs credit activate btn-outline-info'> Aktivovat za kredity </button>
                                                    <button data-target='#desc${result.data[i].id}' class='btn btn-xs more-info  btn-outline-default'>Více podrobnosti</button>
                                                    <div style='display:none' id='desc${result.data[i].id}' class='alert  desc alert-primary m-t-10'>
                                                        ${result.data[i].description}
                                                    </div>
                                                </td>
                                            </tr>`;
                                }

                            }else{
                                html += "<tr><td colspan='5'><div class='alert text-center'>No membership level created for the package yet</div></td></tr>";
                            }
                            $('#level-holder').html(html);

                        }
                    },
                    error: function(e){
                        console.log(e);
                        $.toast({
                            heading: 'Error',
                            text: 'An unexpected error occurred. Please try again',
                            position: 'top-right',
                            loaderBg:'#ff6849',
                            icon: 'error',
                            hideAfter: 3500

                        });
                    }
                });
            }

    function convert(amt){
        var eur = Math.ceil(amt/25);
        return amt+' CZK | '+ eur +' EUR';
    }

    </script>

<script src="https://www.paypal.com/sdk/js?client-id={{config('paypal.key')}}&vault=true">
</script>


  <script>
    $(document).ready(function(){
       $(document).on('click', '.activate', function(){
        $('#paypal-button-container').html('');
        $('#lev').text($(this).attr('data-name'))
           $('#paypall').modal();
            plan = $(this).attr('data-plan');
            level = $(this).attr('data-level');
        paypal.Buttons({

            createSubscription: function(data, actions) {
                return actions.subscription.create({
                    'plan_id': plan
                });
            },
            onApprove: function(data, actions) {
                console.log(data);
                // give value to customer
                $.toast({
                    heading: 'Success',
                    text: 'Subscription was successful',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'success',
                    hideAfter: 3500,
                    stack: 6
                });
                $('[name=subscription_id]').val(data.subscriptionID);
                $('[name=level_id]').val(level);
                u = '{{route('payment.store')}}/'+level+'/'+data.subscriptionID;
                $('#payment_form').attr('action', u);
                $('#payment_form').submit();
                // alert('You have successfully created subscription ' + data.subscriptionID);
            }
            }).render('#paypal-button-container');
       })

    });

  </script>
@endpush
