@extends('layouts.user')
@section('content')

<!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-6 col-8 align-self-center">
                        <h3 class="text-themecolor mb-0 mt-0">Zůstatek</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Domů</a></li>
                            <li class="breadcrumb-item active">Zůstatek</li>
                        </ol>
                    </div>
                    {{-- <div class="col-md-6 col-4 align-self-center">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle btn-sm float-right ml-2"><i class="ti-settings text-white"></i></button>
                        <button class="btn float-right hidden-sm-down btn-success"><i class="mdi mdi-plus-circle"></i> Create</button>
                        <div class="dropdown float-right mr-2 hidden-sm-down">
                            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> January 2019 </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton"> <a class="dropdown-item" href="#">February 2019</a> <a class="dropdown-item" href="#">March 2019</a> <a class="dropdown-item" href="#">April 2019</a> </div>
                        </div>
                    </div> --}}
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->

                <div class="row">
                    <!-- column -->
                    <div class="col-lg-3 col-md-6">
                        <!-- Card -->
                        <div class="card">
                        <img class="card-img-top img-responsive" src="{{asset('img/balance.png')}}" alt="Card image cap">
                            <div class="card-body">
                                <h4 class="card-title">Kredit <small>{{$balance}} </small></h4>
                                {{-- <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p> --}}
                                <button class="btn btn-primary add-credit">Dobít</button>
                            </div>
                        </div>
                        <!-- Card -->
                    </div>
                    <!-- column -->
                    <div class="col-lg-9 col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Historie</h4>

                                <div class="table-responsive">
                                    <table class="table table-hover no-wrap">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Název transakce</th>
                                                <th>Datum</th>
                                                <th>Cena</th>
                                                <th>číslo objednávky
                                                    </th>
                                                <th>Stav</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if(isset($payments))
                                                @if(count($payments))
                                                    @foreach($payments as $payment)

                                                        <tr>
                                                        <td>{{$loop->iteration}}</td>
                                                        <td>{{ucwords($payment->type)}}</td>
                                                            <td>{{getDateFormat($payment->created_at, 'd-M-Y')}}</td>
                                                        <td>{{$payment->amount}}</td>
                                                        <td>{{$payment->reference_number}}</td>
                                                            <td>
                                                                @if($payment->status == 'completed')
                                                                <i class='fa fa-check'></i> Ověřeno
                                                                @elseif($payment->status == 'pending')
                                                                <i class='fa fa-hourglass '></i> {{ucwords($payment->status)}}
                                                                @else
                                                                <i class='fa fa-times '></i> {{ucwords($payment->status)}}
                                                                @endif
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                    @else
                                                    <tr>
                                                        <Td colspan='6'>
                                                            <div class='alert alert-default text-center'>žádné platby nenalezeny
                                                                </div>
                                                        </td>
                                                    </tr>

                                                @endif

                                            @endif

                                            {{$payments->links()}}

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>



                    <!-- column -->
                </div>


    <div class="modal fade " tabindex="-1" role="dialog" id='credit' aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myLargeModalLabel">Top up Credit</strong> </h4>

                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                <p>Total Credit: <span class='text-bold'>{{$balance}}<span> </p>
                        <form id="credit_form" method='post' action="{{route('credit.store')}}">
                                @csrf
                                    <div class="form-group">
                                        <label>Amount</label>
                                        <input required name='amount' value="" type="number" class="form-control" placeholder="">
                                    </div>
                                <input type='hidden' name='user_id' value='{{auth()->id()}}' />
                            <input type="hidden"  name="order_id" />

                                    <button type="submit" class="btn btn-primary">Submit</button>

                            </form>
                </div>
                {{-- <div class="modal-footer">
                    <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
                </div> --}}
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>




<div class="modal fade " style="background:#433f3fd9" tabindex="-1" role="dialog" id='paypall' aria-labelledby="myLargeModalLabel" aria-hidden="true" >
    <div class="modal-dialog modal-dialog-centered" >
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" >Select a Payment Method</h4>

                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">


                <div id="paypal-button-container"></div>

            </div>
            {{-- <div class="modal-footer">
                <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
            </div> --}}
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>



@endsection


@push('scripts')

    <Script>

            $('.add-credit').click(function(){
                $('#credit').modal();
            });



    </script>

    <script src="https://www.paypal.com/sdk/js?client-id={{config('paypal.key')}}&vault=true&currency=CZK">
    </script>


    <script>
        $(document).ready(function(){
            $(document).on('submit', '#credit_form', function(e){
                e.preventDefault();
                $('#paypal-button-container').html('');
                $('#lev').text($(this).attr('data-name'));
                $('#paypall').modal();
                //get tip amount
                var amt = $('[name=amount]').val();

                paypal.Buttons({
                    createOrder: function(data, actions) {
                        // Set up the transaction
                        return actions.order.create({
                            purchase_units: [{
                                amount: {
                                    value: amt,

                                }
                            }]
                        });
                    },
                    onApprove: function(data, actions) {
                        console.log(data);
                        $.toast({
                            heading: 'Success',
                            text: 'Payment was successful. Please hold on.',
                            position: 'top-right',
                            loaderBg:'#ff6849',
                            icon: 'success',
                            hideAfter: 3500,
                            stack: 6
                        });
                        return actions.order.capture().then(function(details) {
                            // Show a success message to your buyer
                            console.log(details);
                            $('[name=order_id]').val(data.orderID);
                            $('#credit_form').unbind(e).submit();

                            // alert('Transaction completed by ' + details.payer.name.given_name);
                        });
                        // give value to customer


                        // alert('You have successfully created subscription ' + data.subscriptionID);
                    }
                }).render('#paypal-button-container');
            })

        });

    </script>
@endpush


