@extends('layouts.user')
@section('content')
<div class="row page-titles">
    <div class="col-md-6 col-8 align-self-center">
        <h3 class="text-themecolor mb-0 mt-0">Můj profil</h3>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Domů</a></li>
            <li class="breadcrumb-item active">Můj profil</li>
        </ol>
    </div>

</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->
<!-- Row -->
<div class="row">
    <!-- Column -->
    <div class="col-lg-4 col-xlg-3 col-md-5">
        <div class="card">
            <div class="card-body">
                <center class="mt-4">
                    <h4 class="card-title mt-2">{{auth()->user()->name}}</h4>
                    {{-- <h6 class="card-subtitle">Accoubts Manager Amix corp</h6> --}}
                    {{-- <div class="row text-center justify-content-md-center">
                        <div class="col-4"><a href="javascript:void(0)" class="link"><i class="icon-people"></i> <font class="font-medium">254</font></a></div>
                        <div class="col-4"><a href="javascript:void(0)" class="link"><i class="icon-picture"></i> <font class="font-medium">54</font></a></div>
                    </div> --}}
                </center>
            </div>
            <div>
                <hr> </div>
            <div class="card-body"> <small class="text-muted">Emailová adresa </small>
                <h6>{{auth()->user()->email}}</h6>
                {{-- <small class="text-muted p-t-30 db">Phone</small> --}}
                {{-- <h6>+91 654 784 547</h6> <small class="text-muted p-t-30 db">Address</small>
                <h6>71 Pilgrim Avenue Chevy Chase, MD 20815</h6> --}}

            </div>
        </div>
    </div>
    <!-- Column -->
    <!-- Column -->
    <div class="col-lg-8 col-xlg-9 col-md-7">
        <div class="card">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs profile-tab" role="tablist">

                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#settings" role="tab">Nastavení</a> </li>
                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#password" role="tab">Změnit heslo</a> </li>

            </ul>
            <!-- Tab panes -->
            <div class="tab-content">

                <div class="tab-pane active" id="settings" role="tabpanel">
                    <div class="card-body">
                        <form  method="post"  action='{{route('user.update')}}' class="form-horizontal form-material">
                            @csrf
                            <div class="form-group">
                                <label class="col-md-12">Název</label>
                                <div class="col-md-12">
                                <input type="text" name="name"  value='{{auth()->user()->name}}'  class="form-control form-control-line">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="example-email" class="col-md-12">Emailová adresa</label>
                                <div class="col-md-12">
                                    <input type="email"   readonly value='{{auth()->user()->email}}' class="form-control form-control-line" name="example-email" id="example-email">
                                </div>
                            </div>


                            {{-- <div class="form-group">
                                <label class="col-sm-12">Select Country</label>
                                <div class="col-sm-12">
                                    <select class="form-control form-control-line">
                                        <option>London</option>
                                        <option>India</option>
                                        <option>Usa</option>
                                        <option>Canada</option>
                                        <option>Thailand</option>
                                    </select>
                                </div>
                            </div> --}}
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <button class="btn btn-success">Změnit profil</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="tab-pane " id="password" role="tabpanel">
                    <div class="card-body">
                    <form method="post"  action='{{route('user.change-password')}}' class="form-horizontal form-material">
                        @csrf
                            <div class="form-group">
                                <label class="col-md-12">Staré heslo</label>
                                <div class="col-md-12">
                                <input type="password" name ='old_password'  class="form-control form-control-line">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="example-email" class="col-md-12">Nové heslo</label>
                                <div class="col-md-12">
                                    <input type="password" name='password' class="form-control form-control-line" name="example-email" id="example-email">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12">Confirm Password</label>
                                <div class="col-md-12">
                                    <input type="password" value="password" name='password_confirmation' class="form-control form-control-line">
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-sm-12">
                                    <button class="btn btn-success">Změnit heslo</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Column -->
</div>
<!-- Row -->
<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Right sidebar -->
<!-- ============================================================== -->
<!-- .right-sidebar -->

<!-- ============================================================== -->
<!-- End Right sidebar -->




@endsection
