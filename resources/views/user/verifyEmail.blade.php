@extends('layouts.user')
@push('title')
    {{config('app.name')}} | Account Management
@endpush
@push('styles')


@endpush


@section('content')
    <div class="wrapper dashborad-h">
            <div class="container-fluid ">

               <div class="col-md-12 mt-20 m-auto center-block col-xl-4 col-xl-offset-4" style='margin-top:20px !important'>
                        <div class="card m-b-30 text-center">
                            <div class="card-header">
                                Account Management
                            </div>
                            <div class="card-body">
                                <h5 class="card-title text-warning">Activate Activation</h5>
                                @if($status)
                                <div class='text-center'>
                                        Ověřeno successfully <a class='btn btn-less mt-10 btn-corner right-15' href='{{route('user.dashboard')}}'>Dashboard</a>
                                </div>
                                @else
                                    <div>
                                        Could not be verified. You may have already verified your account and there was a problem somewhere.
                                    </div>
                                @endif
                            </div>
                            {{--  <div class="card-footer text-muted">
                                2 days ago
                            </div>  --}}
                        </div>
                    </div>
            </div> <!-- end container -->
        </div>
        <!-- end wrapper -->








    </div>
     <div style='clear:both'></div>
@endsection

@push('scripts')
  <script>
    $(document).ready(function(){
        @auth
       @if(auth()->user()->verified && auth()->user()->blocked == 0)
            window.location.href = '{{route('user.dashboard')}}';
        @endif
        @endauth

    });

  </script>

@endpush
