<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!--[if !mso]><!-->
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<!--<![endif]-->
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title></title>
<style type="text/css">
* {
	-webkit-font-smoothing: antialiased;
}
body {
	Margin: 0;
	padding: 0;
	min-width: 100%;
	font-family: Arial, sans-serif;
	-webkit-font-smoothing: antialiased;
	mso-line-height-rule: exactly;
}
table {
	border-spacing: 0;
	color: #333333;
	font-family: Arial, sans-serif;
}
img {
	border: 0;
}
.wrapper {
	width: 100%;
	table-layout: fixed;
	-webkit-text-size-adjust: 100%;
	-ms-text-size-adjust: 100%;
}
.webkit {
	max-width: 600px;
}
.outer {
	Margin: 0 auto;
	width: 100%;
	max-width: 600px;
}
.full-width-image img {
	width: 100%;
	max-width: 600px;
	height: auto;
}
.inner {
	padding: 10px;
}
p {
	Margin: 0;
	padding-bottom: 10px;
}
.h1 {
	font-size: 21px;
	font-weight: bold;
	Margin-top: 15px;
	Margin-bottom: 5px;
	font-family: Arial, sans-serif;
	-webkit-font-smoothing: antialiased;
}
.h2 {
	font-size: 18px;
	font-weight: bold;
	Margin-top: 10px;
	Margin-bottom: 5px;
	font-family: Arial, sans-serif;
	-webkit-font-smoothing: antialiased;
}
.one-column .contents {
	text-align: left;
	font-family: Arial, sans-serif;
	-webkit-font-smoothing: antialiased;
}
.one-column p {
	font-size: 14px;
	Margin-bottom: 10px;
	font-family: Arial, sans-serif;
	-webkit-font-smoothing: antialiased;
}
.two-column {
	text-align: center;
	font-size: 0;
}
.two-column .column {
	width: 100%;
	max-width: 300px;
	display: inline-block;
	vertical-align: top;
}
.contents {
	width: 100%;
}
.two-column .contents {
	font-size: 14px;
	text-align: left;
}
.two-column img {
	width: 100%;
	max-width: 280px;
	height: auto;
}
.two-column .text {
	padding-top: 10px;
}
.three-column {
	text-align: center;
	font-size: 0;
	padding-top: 10px;
	padding-bottom: 10px;
}
.three-column .column {
	width: 100%;
	max-width: 200px;
	display: inline-block;
	vertical-align: top;
}
.three-column .contents {
	font-size: 14px;
	text-align: center;
}
.three-column img {
	width: 100%;
	max-width: 180px;
	height: auto;
}
.three-column .text {
	padding-top: 10px;
}
.img-align-vertical img {
	display: inline-block;
	vertical-align: middle;
}
@media only screen and (max-device-width: 480px) {
table[class=hide], img[class=hide], td[class=hide] {
	display: none !important;
}
.contents1 {
	width: 100%;
}
.contents1 {
	width: 100%;
}
</style>
<!--[if (gte mso 9)|(IE)]>
	<style type="text/css">
		table {border-collapse: collapse !important;}
	</style>
	<![endif]-->
</head>

<body style="Margin:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;min-width:100%;background-color:#f3f2f0;">
<center class="wrapper" style="width:100%;table-layout:fixed;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;background-color:#f3f2f0;">
  <table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:#f3f2f0;" bgcolor="#f3f2f0;">
    <tr>
      <td width="100%"><div class="webkit" style="max-width:600px;Margin:0 auto;"> 
          
          <!--[if (gte mso 9)|(IE)]>

						<table width="600" align="center" cellpadding="0" cellspacing="0" border="0" style="border-spacing:0" >
							<tr>
								<td style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;" >
								<![endif]--> 
          
          <!-- ======= start main body ======= -->
          <table class="outer" align="center" cellpadding="0" cellspacing="0" border="0" style="border-spacing:0;Margin:0 auto;width:100%;max-width:600px;">
            <tr>
              <td style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;"><!-- ======= start header ======= -->
                
                <table border="0" width="100%" cellpadding="0" cellspacing="0"  >
                  <tr>
                    <td><table style="width:100%;" cellpadding="0" cellspacing="0" border="0">
                        <tbody>
                          <tr>
                            <td align="center"><center>
                                <table border="0" align="center" width="100%" cellpadding="0" cellspacing="0" style="Margin: 0 auto;">
                                  <tbody>
                                    <tr>
                                      <td class="one-column" style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;" bgcolor="#FFFFFF"><!-- ======= start header ======= -->
                                        
                                        <table cellpadding="0" cellspacing="0" border="0" width="100%" bgcolor="#f3f2f0">
                                          <tr>
                                            <td class="two-column" style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;text-align:left;font-size:0;" ><!--[if (gte mso 9)|(IE)]>
													<table width="100%" style="border-spacing:0" >
													<tr>
													<td width="20%" valign="top" style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:30px;" >
													<![endif]-->
                                              
                                              <div class="column" style="width:100%;max-width:80px;display:inline-block;vertical-align:top;">
                                                <table class="contents" style="border-spacing:0; width:100%"  >
                                                  <tr>
                                                    <td style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:5px;" align="left"><a href="#" target="_blank"><img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxISEhUSExMWFRUVFRUWFhcXFxcXGBYbGBUXFxgYFxUYHSggGBolHRUXITEhJSkrLi4uFx8zODMtNygtLisBCgoKDg0OGhAQGy0lICUtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIAOEA4QMBEQACEQEDEQH/xAAcAAACAwEBAQEAAAAAAAAAAAAABwQFBgMBAgj/xABJEAABAwICBQgGBwYEBQUAAAABAAIDBBEhMQUGEkFREyIyYXGBkaEHUmJyscEUI0JDgpLRM1NjorLwRHPC4RUkNVSDFjSTw/H/xAAaAQEAAwEBAQAAAAAAAAAAAAAAAwQFAgEG/8QANREAAgIBAwEFBgUEAwEBAAAAAAECAxEEITESBUFRYXETIjJCkbEUgaHB0TNS4fAjNPEVJP/aAAwDAQACEQMRAD8AeKAEAIAQAgBACAEAICPU1kcfScB3/JRWXVw+JnUYSlwiqn1ljHRa53kPNUp9p1r4VksR0snzsV82s0p6LWt8Sqk+07X8KSJVpI97Icump3YF9uwAKvLXXy2bJY0VruIj6l5zcVA7JvlkihHuQNnePtFFZNcMdMfAlQ6Ynbk/xAPyU0NZfHiRHKiD7iZFrLMOkGu8QrEO07VykyN6WHcT4NZmHpsLezEK1DtOD+JYIpaSXcy0pdIRSdF4PVkfBXq9RXZ8LK8q5R5RLUxwCAEAIAQAgBACAEAIAQAgBACAEAIDxAQK7S0cWF9p3qj5ncqt2rrq25fgSwplIz9ZpuV+AOwOAz8Vl2662fGyLcNPCPO5WOBOJxVJpvdljjg82V5gZPpsROQK8BHnqYmdOWNvvSMHlddxqnLiL+hy5Jcshv0/RjOqg/OD8FKtJe/kf0OfbQ/uR43T9GcqqD84+YT8Jf8A2P6D21f9yJcFZC/oTRO7JGHyuo5UzjzF/RnSnHxJRiPDBRnR87K9wMgG2TAyWFHpeWPDa2hwdj5q3VrLa+/K8yGdMJGgodNxyYHmO4HI9hWpTrq7NnsypOiUeNyzCukB6gBACAEAIAQAgBACAEAIAQHKonawbTjYLic4wWZHsYuTwjO6Q0u9+DOa3zP6LJv1kp7R2RcroUd2VWwqOCxkCywJNgBmTgB2kp0+AyZvSuulJDdrSZnjdH0e95wVuvQWT52XmQy1EY8bmVr9fKp+EYZCN1htu8XYeSvQ7PqXxZZBLUzfGxn6zSM837WaV/UXut+UWCtwqhD4YpfkQucpcsiCMcB4KTLOMIk0tFLJfk43vta+w0m1+NlHOyEPiaR0ot8I7/8ABan/ALeX8hXP4in+9fU96J+BDqaQsdsyR7LsDsubY45FSRmpLMXlHOMbYOlJWSxG8UskfuvcB4XsvJwjP4kn6o9UnHh4L+g14rI+mWTD227LvzN+YVSegpl8O3oTR1E1zuajRmvVLLYSh0Dva5zPzjLvVKzs+yPw7/cnjqYy52NRHZzQ5pDmnJzSCD3hU3FrZkyeT62F5g9yWOj9JvjwPObwO7sKt0aqdez3RDZTGRo6SrbILtPaN47Vr1WxsWYlKUHF4ZIUpyCAEAIAQAgBACAEAICNW1jYxjidw4qG66Na35O4QcjOVUzpDdx7BuCyLJyseZF2MVFbHERqPB1ky+smudPSkxs+vnGbWnmRn+JIN/stuexWadHKe72RDO9LjcXOmdP1FUfrZDs7o281g/CM+9adVEK/hX5laU5S5ZWKU4BACAEAwfRMf/c/+L5rF7Y+T8y9ouWMK6xC+KL0kf8AUH/5UPwK+m7M/wCsvVmTqf6rMyr5ACAEBN0VpaemdtQyFnFubT2tOBUdlULPiR1GTjwxhava+wzER1AEEhsA/wC5eeG192e3DrWdboZR3huv1LML0+TZGNU8E+T7he5h2mmxXUHKDzE8aUluaLR9eJBY4O4cesLXovViw+SlZW4+hNVgjBACAEAIAQAgBAR6upDBxO4KK21QXmdQj1MoZbuNziVlyzJ5ZbWEsI5TbLGl7iGtaLucTYNA3krxQzsg5Jcit1u17fNtQ0pMcWTpMnye7vY3zKv06VR3luyvO1vgxDRbAK2RHqAEAIABxsMTwGJ8AndkFnSau1kuLKaS3Fw2R/Mq89XRD4pr7kkapy4QwPR7oKel5bl2hu3sbI2gcr3vbLNY3aWpqucfZvOMl3S1ShnJr1llwXOvOrNXPVumii22GONos4A3aDfA9q3dBrKa6VCcsPLM7UUzlNtIxtZo6eH9rDIzrLTb8wwWpXdXZ8Ekyq4yjyiKCpODkEAIAKA1OqWuctJaKS8tPls5viHGMnNvs+Cr3aeM91sySFjiNuhqI542yxOD43ZOHmDwI4FZsq3F4aLKmnwSGtINxgUSaeUe5TLyhqtsWPSHmtOm3rWHyVZw6SWpyMEAIAQAgBAfL3WF15J4R6lkrZWFxuVTknJ5ZMng41JZGx0kjgxjAXOc42DQMyV4q8hyEdrvrg+vfsMuylYeYw4GQj7yX5N3duVqutR9SKUmzLqQ5BACAmaK0VPUu2IIy8jM5Mb7zzgPiorb66Vmx4+/5I6jCUniKN3oj0bxizqqQyH93GSxg7X9J3ksi7teT2qWPN7v+C7Xo++TNho/RcEAtFExg9lov3nMrLsuss+OTZbjVGPCJZKiO8Ah6CAEAHh8cUPGkyi0rqhR1Fy6IMcftxcx3lge8K5Tr76tlLK8HuV7NNXLyMPpvUCphu6E/SGDcBsyge5k/wDCb9S16O1Kp+7P3X+n+ClZpZw3W6Mkd4NwRgQQQR1EHELSK4IAQF3qprNLQS7bOfE4jlYScHji31ZBuO/I9XE4Ka3PVJoe+iayKpiZPC7bjeLg5EcWuG5wyIVR1YJlPJMZFY3GaKOHlBvJZRPuFcjLKIWsH2ujwEAIAQAgI0r7qGTyztI+LLk9Eh6TNcfpchpoHf8ALxus4j754OJ62NOXEqaMcHDZh10eAgAlAbfVXUJ8oE1UCyM4tjye8bi71W9WazNX2h7PMa934+BZp0/Xu+BjUtMyJgjjaGMGTWiwH+/WsCcpSl1SeX4mlCCisI6rg7ABARqvSEMX7SVjPecAfDNSQqsn8MWyOVkY8sqZdc6Bv+IB90EqzHs/Uv5SJ6qvxObdeaA/fEdrHL19m6n+39Tz8VWTqTWWjkwZUxk8Cdn4qKejvh8UGdrUVvhlowgi4II4g3HiFXezwyZST4PV4eggKXWLViCsF3jYltzZW9L8XrDtVvTayyh+7x4f7wV7dPGYqdPaEmo5NiUYOvsPHQeOo7j7Oa+j0+orvj1Q/NeH++JmTrlB4ZWqc4BAabUTWx2j5ucSaeQjlW+ru5Ro4jfxC8ayB/RSNc0OaQWuAc0jEEEXBB4WUDWCQ6xusvYvDPGiSCpzg9QAgBAR6qbZFt5UVs+lYOoRyQ+VVfrJekwfpX1oMEIpYXWlnB23A2McWRsdzn9EdVypqt9yOYnALYDIKc4BABNs0A0/RvqAQG1tW3ndKCF32OEso3v4N+zmbnowXSeMI7hHxGFUQLGtpL8J9xCfGqE4YLCkZTWPXSnpSWN+umGcbCLN/wAyTJvYLlWtP2dbdu/dj4vv9EQWaqMdluzAaV1wrKi4MnJM9SLmjsL+k7yWzToKKt0svxe/+ClO+yfLKF2JucTxOJ8Sri24IQQAgPCEBLodIzQG8Mr4z7LsO9pwPgo5012bTimdRk48M2OhfSM9tm1ce2P3sQs4dbosnfhx6ll39kxe9Lw/B/sy1XrJLaZv6CuinYJIntkYcnNN+4jceorGsqnXLpmsMvwsjNZTJCjOiNpKgiqI3RTMD2OzB3Hc5pza4biFJVbOqSlB4ZxZXGawxP6z6uSUUmy4l8Tr8nJbMeq8DJ488xwX0+k1cdRDK2a5X8eRlW1Ot4ZTK0RAgGf6INZiL6PlOGL6Ynhm+Hu6Te8bgorVtk6i9xocqq/WS9JKpJ74eCmqnnYjnHBKU5wCA8ugM/V1m08ndkFlW29Ui5CGERKnSDY2OkebNY0uceoC64UstJHrWFkQumdJvqp5J35yOuB6rRg1vcFrRj0xSRTbzuQl0eAgN/6KtUxUSfS5heGJ31bTlJIN54tb8VFZYo7HUVkdKh5JODnPYAucQAASScAAMyTwXLh1bHSlgTGvGv5nLoKMlkOTphg+XjseozrzO7cuq9JGL6pb/Y4lbJ7GCAVsiBADcTsi5dwAJPgMUeyy+B6FrT6t1r8W00tusBv9RCry1lEeZr7kiqm+Ednao14/wz+5zD81wtfpn86/U99hZ/aVtZQTQ/tYpI+tzCB+bJTwtrn8Ek/Q4cZLlEYG+SkOQJ/v9OKIDN9H2rElPeolLmOkbYRA2FvWlGRdwG5YHaWsjZ/xwWcd/wDHkX9LTJe+zarJL5G0hXRwRullcGsbmePBoG9x4KSuqVklCCy2cWTjBZkKLWnWaStfvZC08yP/AFP4u+C+m0mjjp4+Mu9mTba7HllGrZECA6U9Q+N7ZIzZ7HBzTwINx/fWvGk9mB96G002pgjnb9tuI9VwwcPFZVmYScWXIbrJPiq9kg8CvIW9LTPXDKNGx9wCMjitaLyslJ7bHq93BC0vUbERO84DvVfU2dFbZJVHqlgyvKLE6jQwY30m6V2YWU7TjMdp/uM3d7reavaCHVJyfd9ytqJYSXiLZapUBASdGUL6iaOBnSkcGg8Bm5x6gAT3KO2xVwc5cI9jHqeD9BaJiZBGyGMWZG0NaOob+05rChqXJ5ZoeywsFvFLdX4WJkEo4FB6U9czO91FA60LDaZw+9ePsX9Rpz4nDcQr8I4RWbyLxdnh6xpJDQCS42aALkk7gBmUAwNAejR7gJKxxYDiIWHn2/iSfZ91uPWs3Va5wWK1v4v9ixVR1bs3FBouCnbswxMjHsjE9rsye9Yd1tljzNtmjCuEeESbqAlBABxwOI4HEeC9W2540nyZrTepFJUXc1vISH7cQABPtx9F3dY9av6ftK6rZvqXg/5K1mlhLdbEHVPUcU8hmqHNle0nkg2+w3+IQcS48Nyl1naXtY9FaaT58fT0I6dK08zNmsovEPS2k4qaIyyus0eLjua0byVLTTO2XRBZ/wB7yKy1VrLE9rHp+Wtk25Oaxt+SiB5sY4n1nne7uGC+n02lhp4dMeXy/H/BlWWOx5ZVKyRggBACA3Xov0rsulpXHBw5WPtGDx3ix8Vn6+GIqa9GWdPLdxGDyqzOot4NJoCp2o9ne027ty19FZ1wx4FLURxLJZq4Qmb1rqcWM6to/AfNZHaVmHGP5lzSx2bKASLM6i3gVOuNdy1ZKQbtYRE38Gf8xK+g0cOmmOe/f6mZfLqsfkUytEQIDf8Aoq0Zcy1Thl9TH5OkcP5R3FY3a92Omper/Yu6OGW5DGa5YylgvtFDr7rKaSlIjNpp7xxn1RbnyfhBw6yFrdnxdk/Jc/wUdU+lYEu0LcKJ9NaSQACSSAAMySbADruQgHl6PtR2UTBNMA6peMTmIgfsM6+JUFks7I7ijVzwrPtqyi1CZWTx2udwxJyA7TuWXZU0y1GaxuZuu1toYiQZ2uIzDLv8xguoaDUT3UfrseS1Nce8ix690BNuUcOsxusu32bqFvhfU5/F1l3o/SMM4vDKyQeybkdozCqWU2VvE4tE0bIy4ZKUZICAg6Z0tFSxGWV1gMAB0nu3NaN5+CmoondPoh/4RW2xrWWJ7WDTktZLykmAH7OMHmsHzdxK+n0+nhRHpjz3vxMmycpvLKxWDgEAIAQAgJeiK7kJ4ptzHgu904O8iVHdX7SuUfFHUJdMkxyOfj/fcvmOo1sFtqzU2l2fWB8Rj+qv9n2Yt6fEr6mPuZNYtvcoGF1jn2qh/s2b4D9br5vXTzfLy2NTTxxWiqmqdhrnnJjXO/KCfkqsV1yUV3vH1Jm+lNica4nE5nE9pNz8V9e1h4Ric7nqA8cbC6Ad2qtByFJBHvDA53vP5zvMr5PV2+1ulLz+xsUQ6YJFqqxKJ7X3SXL1r7HmQjkWfhxee939IX1HZ9Xs6Fnl7v8AYyNRPqsZnldIRh+h/QIkldWyC7YTsQg75Lc9/wCEEAdZPBVr7lBqPeSQg5bjka5Rp5O2is1l05DRQOnmOAwa0dKRxya0bz8M12odWxy5YENrPrVU1zjyrtiK/MgYSGN9/fI7rOHAKaFUIcLfxI3JvkpAFIeAgPqKRzHB7HFjhk5pLXDsIXjSksPdBbboYupuu5kc2nqiNs2Ec2QkO5sgGAedxGB6jnh67s7pTsq4714ea8i9RqflmavTemIqSIyynqa0dJ7tzWj57lm0aed8+mH/AIW7bVWssTunNMy1cvKynqYwdGNvqt6+J3r6mjTwoh0R/PzMmc3N5ZXqY4BACAEAIAQHjm3FuOCcMDW1frOVpYHnMxtB7W8w/wBK+W1Uei+cfP77mvS+quLLjRtRsSxu4Ob5mx+K5os6bYy8z2yPVFoYq+rMYWmkJNqWQ8XuP8xXyF0uqyT82bkFiCXkUetMuzRznizZ/M4BTaGPVqYLzz9NyPUPFUhaL6kyAQHaig5SWOP15GN7i4X8lxZLphKXgmexWWkPy1sOGHgvjTcXBxrJ+TjfJ6jHO8ASu4R65KPizmbxFsQheXXcc3EuPaTf5r7LGNjEzk+XmwNs93aiA+9V6IU1NDAPsMG11udznn8xK+as1PtLXPzNSFfTBI0lNJfBXqbcleyOBC+kHWI11Y9wN4YS6KEbrNNnydrnA48AFqxWEVGzNro8BACAEB44XFkBM0lpKWocHzPLy1oY2+4AbhxO871FVVCpNQWMnTk5ckRSnIIAQAgBACAEALwG/wBRZb0tvUlkHjZ3zXz3ascajPik/wBjU0bzX6M0G0s3JZN5/wAVC+k/FIyvYmFkzJ6z8V87LlmquCh12NqN/W+IfzX+SvdmL/8ASvR/Yr6v+k/yF4vpDKBAWmqrL1tMP4o8gSq+reNPP0JKv6iHcV8kbJT64ybNDUkfuiPEgK1olnUQXmQ6h/8AGxKhfVmQStFQ7c8LDk6aMfzhR3S6a5Pyf2OoLMkvMfV18fFm1jY46YrzDSzyDNsLyO3ZsPitDSyzYo+ZW1EfdEAwWAHUvpDMPUAIAQAgBACAEAIAQAgBACAEAIDb+j4/UzDhKPNg/RYPa6/5YPy/c0dE/dfqaiyyS4WH0gq11kfSiK9mJ7SoWtzpMz+vLP8Ak39UkR/m/wB1e7N21C9H9iDV71P1Qul9EZYIC11Udatpj/FHm0qtq1nTz9CSr+oh2lfJmyU2ubNqgqR/DJ8CFa0LxqIepDqf6bEsF9WZBL0PIG1EDjkJoz/MB81FeuqqSXgzqDxJeo+Dmvj0baIGsEBkpahgzdC+3cL/ACVnSz6bYvzRFfHMGIppuAeoL6wxz1ACAEAIAQAgBACAEAIAQAgBACA3Po7b9TMf4rR4M/3WH2tvZD0/c0NF8MvU1WysvBcyTfo5U/QR5CrhtI8cHOHmV7bHE2vNnkJe6mUWuMF6Go9lgf8AkeCptF7uoh/vKI9RvXJCsX0RmAgJGjp+Tmif6ssZ7toX8lxbHqrlHyZ7F4kmPolfGm4uCPpCn5SKSP143t8Wmy7rl0TUvBnNizFoQoaRgcxge7BfZZzuYnB4+9sM8x2jEeaLncD30NXCenimblJG13YbWcO4ghfH31uqyUH3M2qp9cEya1Ro7e+wk9a9DGjqnxW5jiZITxY43sDxabtPYOK+s0t3tqlLv4fqYtkOmTRUKwcAgBACACUB69pBIIIIzBFiN+I3ZrxPO6B4vQCAEAIAQAgBACAYvo8htSF3rzSH8oDfkVh9p7348EjQ0m1fqzTbCz8FnJrf+Frb/DGf7QqdMwWmf1m/iP8A9VLVQxbL6limWYIrKyj5WOSL143s/M0gedlDD3JKXgzuW6aEey9hfPI9owPmvpPQyz1AeOFwQgHlq7XcvSwy+tG2/aOa7zBXyOpr9ndKHmbNMuqCZYgqAkExrro76PWystZr7Ss9197jucHBfVaG32tEX3rZ/kY90OibRSK2RDA9FumQNujecyZIf/sYOv7Q7SsbtXTt4uj6P9mXdJb0voYyYo7rLrhkuSlgj6x6rRV0HJP5rm3dFIBcxutn1tORG9bGkTrKN6UhHaf0JUUUnJVLNgk8x4/ZyDcWPy/DmFqZyVCvXoBAeE278uvqA3lAbrU3Ul73NqKpmywEOjhcLOeRiHSD7Ld+zmd/BZGt7RUU66nl978PT+foW6NM5e9Lg0WuWqbaxvKR2bUNGByEgH2H/I7lR0WudD6Zbx+3mifUadS96PIppY3Mc5j2lrmktc1wsWkZghfSJppNPKZmtNbM+V6AQAgBACAEAE2xQDf1TojHRU7SMTGHntkJf8CFgap9d0n5/bY0aV0wSLukg2ntHFw+OKjrr6ppeZ3OWIs3C+jMrJR6fg5zXcRbwWdra/eUi3RLbBVtjtjwVLoLGRK64aP+j1s7ALNc/lWe7JzvI7QWzRLqrT/L6FCxYk0U6lOAQDE9FWkrslpScWHlo/ddg8dzrH8SxO16d42rv2fr3fp9i9o58xN8sU0DJ+kXQZqKcSsF5ae7gBm+M9NvaLBw7OtaXZupVdjhLiX37v4Kerq6l1LuFSDvX0Zmn3DK5jmvY4tc0hzXDMEZFeSipLD4CbXA8tRNZY66Pc2dg+sj/wBbeLT5LL/BuuW3BaV/UsM2LGqxGODhs51lNHKwxysbIw5teA5p7iuurp4PMZMNpT0baOcbsbJD1Mfdo7GuvZQWayUDuNGSpb6NaUHGWZw4XaPMBVZ9p2dyRLHRxfJdaJ1cpaY7UUID/Xdz39znZdyoXau634pbeHcWYaeEOEWqqk4IDN64aqtrG7bLMqGjmu3SAfYf8nblf0WtdD6Zbxf6eaKt+nU91yKWogfG90b2lj2mzmnMH9OtfSRlGSUovKZmNNbM5ro8BACAEAICRo6hNRNFAPvZGsPYTzj4ArmUumLl4HqWXgfRiGQGAsB2AWHkFh9BoZJ2hae8l/VBPyVnS15sz4EV0sRNDdauCpgjaSh2mHqxUN8OqB1XLEij2FndJayL/wBLeibxxVTR0DyUnuuN2nudh3q3pZYbiQ3LvFmrhACAm6F0k6mnjnbjsHnD1mHB7e8eYChvpV1brff9+46hJxkpDxp5mva17DtNeA5p4g5L5KUXFuL5RtRkpLKOi5PWsin171ZNNIZ4x9RI7d9085tPBp3eC+k7P1fto9EviX6oytRT0PK4MqtErnWkqXxPbJG9zHsN2uabEfqOpMAbGq3pXjcBHXDkn5cs0ExO63gYxnrxb2KKVeeDpSGAKxj2B8bmvYcnMIc094wVG6fSWK8Mr6iVZNtjbLsIkYuVZslSPlcHpUac1lpaQfWyAv3RM58h/COiOt1lao0dt3wLbxeyILNRCHqc9WNZYq1hLQWSN6cTiC5o3OBHSaeIyxC91ejnp3vunw/97xTfGz1LtVCwZ7W3VdlYzaFmTtHMfuPsP4t69yu6PWS08sPeL5X7oq36dTWVyKOqpnxPdHI0sew2c05j9Qdx3r6aEoyipReUzMaaeGcl0eAgBACA3Xom0VtzyVLhzYW7DOt78z3N/qVbVS91RJalvkaWwqPSWMlvoiGzSeJV7TQxHJXtll4J6sEQEICiqItlxCz7I9Mmi1F5RC0jQsnikhf0ZGlp6r5HuNl5GWHkSWUfn+vo3wSvhkFnxuLT12yPYRYrRTTWUVmtyOvTwEBvPRvrEGn6HIeaSTCTuJxMd+vMLH7T0nUvbR57/wCS5pbun3WMYLDRonQ0rZGuY9ocxwLXNORB3FWaU1JNckNmGsMVGu2o0tETNEHS0xxuMXw9UgGbfaHfxX0tNvWt+TKlHpZkApjkEB2oauWB21DI+I79hxaD2tGB7wuZRjJYksnqeOC+g16r24GVr/fjaT4tsqk+z9PLua9GSrUWLvOrvSBXHfEOyP8AUqNdl6fvz9Tr8VYVlfrLWTCz6h9juZaMfy2PmrFekor+GC/PcjlbOXLKhrQO/Pr7TvVkjwd6KrkhkbLE4sew4EeYI3tO8LicIzi4yWUz1PDyhvaqazR1rNzJmAcpHw3bTeLCV8zrNHLTy8Yvh/s/M1NPf7RYfJfKmWSi1r1ZjrWDHYmYPq5P9D/WYfJXNHrJaeXjF8r915la/TqzdcihrqOSGR0UrSx7cwfIg72nivpq7I2RU4vKZltOLwzguzwEB61pJAaLuJAaOJJsB4oB+asaHFJTRwfaA2pDxe7F36dyzrJdUslmCwi2Y25A4rmKy8HTeNy9jZYAcFpRWFgqt5PtengICBpSG42huz7FW1EMrqJapYeCo2lTyWBf+lXQO2wVsY50YDZwN7MmyfhJseo33Kzp7PlZBbDvFkrZCCAP77OscCgGnqJrcKi1NOQJwOY7ITgfCQDMb8xvAwdboPZv2lfw/b/Bfo1G3TIYVLGvKKzuyRYtbhb+ytOCxwVJbi91r9FsExMtI4U8huSwgmF5O+wxjPWMOpTK3HJG4eArtM6vVdIbTwOaNz28+M9j2/OxXcbYS4Zz0tFWCuzw9QAgBAfUTC47LQXOOQaC4+AxRtLd7Bbmr0LqBUzWdOfo7OBs6U9jcmdp8Fm3dp017Q95/RfUs16Wc93sMXQ2hYKVmxCzZv0nHF7utzjifgsS/UWXSzN/wvQ0KqY1rYsFASnOonZGx0j3BjGglznGwaBvJXUYyk1GKy2cykorLE/rhrH9NlBa3ZijuI7jnuvm5x3A7m7u1fTaPSfh4bv3nz4f75mTdb7R5KFXCEEBu/RZoHlJDWyD6uIlsIP25PtP91gNvePUq+oswulElcMvI09pU8lksdFQ3557B8yrWnh8zIbZdxaK2QAgBAeELzAM3XwGN1txxCzLoOEi5XLqRGLwbg2IIIIOIIOBBHAhRdR3gTmuerX0KW8YP0eQnkz6h3xu7N3UtKi72i35KlkOlmdUxGCABxGBBuCDYgjIg7j1oBpakeklvNhrjY4Bs+48OVAyPtKt+GinmJIrH3jVjeHAOaQQRcEG4I4gheYxyepnGd6r2ywiWCKqokzG45jce0LItnuXYwXeZ+v1Zo5sX07L8WjZP8q8hrL6/hkxLT1y7iol9HdEcuVb2PuPNWF2reucP8iJ6OBzHo4pN75j+IBdf/Wu8EPwcSbTai0DMeSL/feT5KKXaWol82PRHUdJWi9o6OOIWijZGPZaB55qnOyU95vJPGuMeEd1wdgh4V+mtMwUrNuZ9vVaMXv6mt+ano09l0sQX+COy6MFuKjWjWWWtdZ3MiabsiBwvuc8/ad5BfR6XSQ0623l3v8Ajw/cy7bZWPLKRWiIEBbas6CfWzCMXawWMr/UbwHtHIeKjtsVcc9/cdQh1PA6qWJkTGxRtDY42hrGjIAf3dZbm28suqKSwiXSxl7g0d/UF1XFzlg8k1FZNLEwNAAyC1oxSWEUm8vJ9r08BACAEBGrqQSNI37jwKitrVkcHcJ9LyZOYFji12BCxpJxeGX44ayiJX00c0bopW7THixG/qIO5w3FI2OLyg4JrDFDrHoGSjk2Xc6N1+Sk3PHA8HjeO8LXpujbHK7uUUbK3B7lUpiMEAIC81c1tq6E2gkvHvhku6M9gzYetvgvGk+T0Y2ivSdSTgNmDqaT2+dEfdlGX4gFnanTWYzDf7liq1J+8aBkzXjaY4OByLSHA94WFZlPD2NGMk1sCiJAQAgBAACHhA0npmnpxeaZjOom7j2MGJU1Wntt2hFsjndCHLMRpv0juN20kewP3sou7tZFkO13gtajslLe558l+7KVmsb2iYipqHyOL5Hue85ucbk9/wAlrxjGC6YpJFRtvdnJengICbofRUtVKIohjm5x6MbfWeeHAZkriy2NceqX/p1CDm8Ib+hdGx0sQhiGAxc49KR29zvkNyxrLnZLqZfhWorCJ7XEkAYk5BcptvCOmsbmq0XRcm3HpHP9FsUU+zjvyULJ9TJynIwQAgBACAEBW6Y0aJW3GDxl19RVXU6dWrK5Jqreh78GRku0kEWIwIWK8xeGaC33RHrKeOZjopWh7HZg+RB3OHFewscZKUXueOKawxZ6y6ryUt3tvJBuf9pnVIP9S2NPqo27PZ+H8FC2lw37igVohBACAEB1pKl8RvE98Z9hxb4gYFczhGaxNZ9T1NrgvKXXevZhyok99gPmLKpPs7Ty7sejJY6ixd5uNR9Y5azleVawGPYts3x2r5g9iydfpIUdPQ3vku6a6VmcmpWcWzB6464VFNUugiEdgxjtpwJN3A3323LY0WgquqVk88soX6icZ9KMjXa01s2D6hwHBlmDxGPmtOvRaeHEF+e5UlbOXLKcjG5xJ3nE+JVnuwRggBACAttX9X5at3N5sYPOkOQ4hvrOUF+ojSt+fAkrqlPjgaGitHxU0YiibYZuJ6Tz6zjvPwWNZbKyXVI0IVqKwiYHKPJ1g1Gg9F7A23jnHIer/utfS6bo96XJRut6tlwXKvFcEAIAQAgBACAEBV6Y0Q2YXGDxkePUVU1OlVqyuSaq5w27jHVETmOLXAgjcsOcZQliRoxakso57Xnn19o3rnJ1gyOndS2PJfTERuOJiP7N3uHOM9WXYtGjtBx92zdePf8A5KlmlT3ht9jFVtHJC7YlYWO4EZ9hyK1K7I2Lqi8lOUHF4kcF2cggBACAYPom/wAT/wCL/UsXtj5PzL2i5YwVimgKL0kf9Qf/AJUPwK+m7M/6y9WZGp/qszSvEAIAQHSnhdI4MY0ucdzRcryUlFZbwepNvCNhoTUjEPqjh+5Yc/8AMkH9LfFZl/aK4q+r/Yt16Xvn9DaRgNAa0BrQLBrRYAcABkstyb3fJcxjZHSMFxAaCScgF6k5PCD2WWazQuhRHZ78X7hub/utnS6RV+9Ln7FC6/q2XBdBXyseoAQAgBACAEAIAQAgIekdHRzNs8Y7iMwoLqIWrEjuFkoPKMdpTQ8kJvbaZ6w+Y3LDv0llLzyvE0qro2epWbSq5JsHOpiZI3YkaHt4OF/Dguo2Si8xeGeOKawzMaR1KjdcwvMZ9V3Ob45haFXacl/UWf0Ks9Gn8Oxna3VuqiziLxxj53ln5LQr1tFnEseuxVlp7I8r6FQ51jZ3NPB2B8CrS3439CE9QE3R2lp6fa5GUx7VtqwGNssworaK7cdazg7jOUeGTf8A1bX/APcv8G/oovwWn/sX6nXt7PEra6skmeZJXF7yAC42vYZDBT11xhHpgsIjk23lkUPF7A3PAYnwCkweFpRaBqZejC4D1n8weePkq1mrpr5l9NyWNFkuEaHR+pDRYzy7XsR4DvccT3LPt7Uz/TX5stQ0ePjZqaKljhbsxMDBvsMT2nMrOstnY8zeS1GEYrCR32lxk6wTtG6NknPNGG9xyH6qejT2XP3Vt4kVlsa+TY6K0THCMMXb3HPu4Bbun0sKVtyZ1l0rPQsVZIgQAgBACAEAIAQAgBACAEB44JgFDpPVmOS7o/q3cB0T3bu5Z1/Z0J7w2f6FqvVSjs9zL1+iZoekw29YYhZF2ltq+JbeJehdCfDIG0q2SXAbSZB8zNa8We1rx7TQ74hdRnKO8W16HjinyV02gKR2dOwe7dv9JViOt1Efnf3Inp6n8pHdqpRn7tw7JHqRdpahfN+iOfwlXh+p4NU6P92//wCV6f8A0tR4r6I8/B1eH6neLV2jblTsPvFzviVxLXaiXzv9DtaapfKWNPEyPBjGM91ob8Aq0rJT+Jt+rJVBR4R1Llyenm0vcjBLotHSynmMJ68h4qaqiy34URzshD4mabRuqrG2dMds+qMG9+8rWo7NjHex5KVmrb2jsaKNgAAAAAyAwAWmkksIpt53Z9r0AgBACAEAIAQAgBACAEAIAQAgBAeEICsrtBQS4lljxbgfJVLdFTZyiaGonDvKWq1OP3cnc4fMKhZ2S/kl9SzHW+KKufVupb9gO91w+dlTn2fqI92fRliOqrfeQZdHTN6UTx+E/JV5UWx5i/oSK2D4aIxaRmCO5R4aO8rxANPA+CYYyjvFQTO6MTz+EqSNNkuIv6HLsguWibBq7Uu+72feICnhoNRL5cerIpamtd5aU2p7vvJAOpov5lW6+yZfPL6EEtau5FzRau08eOztHi7HyV+rQUw7s+pWnqZy7y3a0DACw6lcSS4IOT1egEAIAQAgBACAEAIAQAgBACAEAIAQAgBAeFAuQC87wC9QONZ0CuLPhZ1XyZStzWLYaED4ps1zA6karR3RW1R8Jn2koKUiBD08CB8n0gBACAEAIAQAgBACAEB//9k=" alt="" width="60" height="60" style="border-width:0; max-width:60px;height:auto; display:block" align="left"/></a></td>
                                                  </tr>
                                                </table>
                                              </div>
                                              
                                              <!--[if (gte mso 9)|(IE)]>
													</td><td width="80%" valign="top" style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;" >
													<![endif]-->
                                              
                                              <div class="column" style="width:100%;max-width:518px;display:inline-block;vertical-align:top;">
                                                <table width="100%" style="border-spacing:0" cellpadding="0" cellspacing="0" border="0" >
                                                  <tr>
                                                    <td class="inner" style="padding-top:0px;padding-bottom:10px; padding-right:10px;padding-left:10px;"><table class="contents" style="border-spacing:0; width:100%" cellpadding="0" cellspacing="0" border="0">
                                                        <tr>
                                                          <td align="left" valign="top">&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                        </tr>
                                                      </table></td>
                                                  </tr>
                                                </table>
                                              </div>
                                              
                                              <!--[if (gte mso 9)|(IE)]>
													</td>
													</tr>
													</table>
													<![endif]--></td>
                                          </tr>
                                          <tr>
                                            <td>&nbsp;</td>
                                          </tr>
                                        </table></td>
                                    </tr>
                                  </tbody>
                                </table>
                              </center></td>
                          </tr>
                        </tbody>
                      </table></td>
                  </tr>
                </table>
                
                <!-- ======= end header ======= --> 
                
                <!-- ======= start hero image ======= --><!-- ======= end hero image ======= --> 
                
                <!-- ======= start hero article ======= -->
                
                <table class="one-column" border="0" cellpadding="0" cellspacing="0" width="100%" style="border-spacing:0; border-left:1px solid #e8e7e5; border-right:1px solid #e8e7e5; border-bottom:1px solid #e8e7e5; border-top:1px solid #e8e7e5" bgcolor="#FFFFFF">
                  <tr>
                    <td align="left" style="padding:50px 50px 50px 50px"><p style="color:#262626; font-size:24px; text-align:left; font-family: Verdana, Geneva, sans-serif"><strong>Dear {{$user['firstName']}} {{$user['lastName']}}</strong>,</p>
                      <p style="color:#000000; font-size:16px; text-align:left; font-family: Verdana, Geneva, sans-serif; line-height:22px ">
                        We noticed you asked to get a new activation link. Please click the link below to activate.
                       <br />
                        <br />
                        <br />
                      </p>
                      <table border="0" align="left" cellpadding="0" cellspacing="0" style="Margin:0 auto;">
                        <tbody>
                          <tr>
                            <td align="center"><table border="0" cellpadding="0" cellspacing="0" style="Margin:0 auto;">
                                <tr>
                                  <td width="250" height="60" align="center" bgcolor="#ff7e65" style="-moz-border-radius: 30px; -webkit-border-radius: 30px; border-radius: 30px;">
                                    <a href="{{route('user.verifyEmail',  [base64_encode($user['email']), $user['verifyToken']])}}" style="width:250; display:block; text-decoration:none; border:0; text-align:center; font-weight:bold;font-size:18px; font-family: Arial, sans-serif; color: #ffffff" class="button_link">

                                    Activate Now
                                    <img src="https://gallery.mailchimp.com/fdcaf86ecc5056741eb5cbc18/images/22fbcafe-7099-4263-911a-c321c90b45de.jpg" width="31" height="17" style="padding-top:5px" alt="" border="0"/>
                                    </a>
                                </td>
                                </tr>
                              </table></td>
                          </tr>
                        </tbody>
                      </table>
                      <p style="color:#000000; font-size:16px; text-align:left; font-family: Verdana, Geneva, sans-serif; line-height:22px "><br />
                        <br />
                        <br />
                        <br />
                        <br />
                        Best Regards, <br />
                        {{config('app.name')}}</p></td>
                  </tr>
                </table>
                
                <!-- ======= end hero article ======= --> 
                
                <!-- ======= start footer ======= -->
                
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                  <tr>
                    <td height="30">&nbsp;</td>
                  </tr>
                  <tr>
                    <td class="two-column" style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;text-align:center;font-size:0;"><!--[if (gte mso 9)|(IE)]>
													<table width="100%" style="border-spacing:0" >
													<tr>
													<td width="60%" valign="top" style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;" >
													<![endif]-->
                      
                      <div class="column" style="width:100%;max-width:350px;display:inline-block;vertical-align:top;">
                        <table class="contents" style="border-spacing:0; width:100%">
                          <tr>
                            {{--  <td width="39%" align="right" style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;"><a href="#" target="_blank"><img src="https://gallery.mailchimp.com/fdcaf86ecc5056741eb5cbc18/images/13f425ab-c680-4ae0-88de-7b493d95095f.jpg" alt="" width="59" height="59" style="border-width:0; max-width:59px;height:auto; display:block; padding-right:20px" /></a></td>  --}}
                            <td width="61%" align="left" valign="middle" style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;"><p style="color:#787777; font-size:13px; text-align:left; font-family: Verdana, Geneva, sans-serif"> {{config('app.name')}} &copy; 2018<br />
                               </p></td>
                          </tr>
                        </table>
                      </div>
                      
                      <!--[if (gte mso 9)|(IE)]>
													</td><td width="40%" valign="top" style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;" > 								<![endif]-->
                      
                      <div class="column" style="width:100%;max-width:248px;display:inline-block;vertical-align:top;">
                        <table width="100%" style="border-spacing:0">
                          <tr>
                            <td class="inner" style="padding-top:0px;padding-bottom:10px; padding-right:10px;padding-left:10px;"><table class="contents" style="border-spacing:0; width:100%">
                                <tr>
                                  <td width="32%" align="center" valign="top" style="padding-top:10px"><table width="150" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                      <td width="33" align="center"><a href="#" target="_blank"><img src="https://gallery.mailchimp.com/fdcaf86ecc5056741eb5cbc18/images/630dff47-d818-4954-b59c-a460ba542fa6.jpg" alt="facebook" width="36" height="36" border="0" style="border-width:0; max-width:36px;height:auto; display:block; max-height:36px"/></a></td>
                                      <td width="34" align="center"><a href="#" target="_blank"><img src="https://gallery.mailchimp.com/fdcaf86ecc5056741eb5cbc18/images/85624967-ff81-441d-9324-8e40068af5a1.jpg" alt="twitter" width="36" height="36" border="0" style="border-width:0; max-width:36px;height:auto; display:block; max-height:36px"/></a></td>
                                      <td width="33" align="center"><a href="#" target="_blank"><img src="https://gallery.mailchimp.com/fdcaf86ecc5056741eb5cbc18/images/3595d450-9ad9-4c65-b60e-922f77287d76.jpg" alt="linkedin" width="36" height="36" border="0" style="border-width:0; max-width:36px;height:auto; display:block; max-height:36px"/></a></td>
                                    </tr>
                                  </table></td>
                                </tr>
                              </table></td>
                          </tr>
                        </table>
                      </div>
                      
                      <!--[if (gte mso 9)|(IE)]> 	</td> 											</tr> </table> 									<![endif]--></td>
                  </tr>
                  <tr>
                    <td height="30">&nbsp;</td>
                  </tr>
                </table>
                
                <!-- ======= end footer ======= --></td>
            </tr>
          </table>
          <!--[if (gte mso 9)|(IE)]>
					</td>
				</tr>
			</table>
			<![endif]--> 
        </div></td>
    </tr>
  </table>
</center>
</body>
</html>