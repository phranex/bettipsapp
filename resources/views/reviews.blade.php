@extends('layouts.master')

@push('styles')
<Style>
    .grid-item { width: 200px; }
    .grid-item img{
        padding: 5px;
        border:  3px solid #eee;
    }
    .grid-item--width2 img{
        padding: 5px;
        border:  3px solid #eee;
    }
.grid-item--width2 { width: 400px; }

.grid-item img:hover{
            transform: scale(1.1);
            transition: 1s;
        }
</style>
<link href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.0/css/lightbox.min.css" rel="stylesheet">
<link href="{{asset('assets/plugins/Magnific-Popup-master/dist/magnific-popup.css')}}" rel="stylesheet">
@endpush

@section('content')
<main id="main">

        <!--==========================
          About Us Section
        ============================-->
        <section id="about">
          <div class="container">
                <header class="section-header">
                        <h3>Reference</h3>
                      <p>{{$review}}</p>
                      </header>
            <div class="grid">
                    @isset($reviews)
                    @if(count($reviews))
                        @foreach($reviews as $review)
                            @if($loop->iteration%2 || $loop->iteration == 4)
                        <div class="grid-item">
                                <a  data-lightbox="portfolio" class="image-popup-no-margins btn btn-xs text-white"  href="{{getImageLink($review->path)}}"><img src="{{getImageLink($review->path)}}" class="d-block w-100 img-fluid" alt="..."></a>
                        </div>
                        @else
                        <div class="grid-item grid-item--width2">
                                <a  data-lightbox="portfolio" class="image-popup-no-margins btn btn-xs text-white"  href="{{getImageLink($review->path)}}"><img src="{{getImageLink($review->path)}}" class="d-block w-100 img-fluid" alt="..."></a>

                        </div>
                        @endif



                        @endforeach
                    @else
                    {{-- <div class="alert text-center ">

                            No reviews Yet
                        </div> --}}

                    @endif
                @else
                    {{-- <div class="alert text-center alert-default">

                        No reviews Yet
                    </div> --}}
                @endisset

            </div>
        </div>
</section>
</main>
@endsection


@push('scripts')

<script src="https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.min.js"></script>
{{-- <script src="{{asset('assets/plugins/Magnific-Popup-master/dist/jquery.magnific-popup-init.js')}}"></script>

<script src="{{asset('assets/plugins/Magnific-Popup-master/dist/jquery.magnific-popup.min.js')}}"></script> --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.0/js/lightbox.min.js"></script>


<script>
$('.grid').masonry({
  // options
  itemSelector: '.grid-item',
  columnWidth: 200
});
</script>
@endpush
