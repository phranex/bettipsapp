@extends('layouts.master')

@push('styles')


@endpush


@section('content')



  <main id="main">

   <!--==========================
      Contact Section
    ============================-->
    <section id="contact">
      <div class="container-fluid">

        <div class="section-header">
          <h3>Kontaktujte nás</h3>
        <p>{{$contact}}</p>
        </div>

        <div class="row wow fadeInUp">

          {{-- <div class="col-lg-6">
            <div class="map mb-4 mb-lg-0">
              <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d12097.433213460943!2d-74.0062269!3d40.7101282!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xb89d1fe6bc499443!2sDowntown+Conference+Center!5e0!3m2!1smk!2sbg!4v1539943755621" frameborder="0" style="border:0; width: 100%; height: 312px;" allowfullscreen></iframe>
            </div>
          </div> --}}

          <div class="col-lg-6 m-auto">
            <div class="row">
              <div class="col-md-5 info">
                <i class="ion-ios-location-outline"></i>
                <p>{{$content['address']}}</p>
              </div>
              <div class="col-md-4 info">
                <i class="ion-ios-email-outline"></i>
                <p>{{$content['email']}}</p>
              </div>
              <div class="col-md-3 info">
                <i class="ion-ios-telephone-outline"></i>
                <p>{{$content['phone-number']}}</p>
              </div>
            </div>

            <div class="form">
                @if(session('success'))
                <div class='text-success text-center'>Your message has been sent. Thank you!</div>
                @elseif(session('error'))
                    <div class='text-danger text-center' >{{session('error')}}</div>
               @endif


               <form action="{{route('contact.store')}}" method="post" role="" class="">
                    @csrf
                    <div class="form-row">
                      <div class="form-group col-lg-6">
                        <input type="text" name="name" required class="form-control" id="name" placeholder="Vaše jméno" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                        <div class="validation"></div>
                      </div>
                      <div class="form-group col-lg-6">
                        <input type="email" class="form-control" required name="email" id="email" placeholder="Váš email" data-rule="email" data-msg="Please enter a valid email" />
                        <div class="validation"></div>
                      </div>
                    </div>
                    <div class="form-group">
                      <input type="text" class="form-control" required name="subject" id="subject" placeholder="Nadpis"  />
                      <div class="validation"></div>
                    </div>
                    <div class="form-group">
                      <textarea class="form-control" name="message" rows="5" required   placeholder="Zpráva"></textarea>
                      <div class="validation"></div>
                    </div>
                    <div class="text-center"><button type="submit" title="Odeslat zprávu">Odeslat zprávu </button></div>
                  </form>
            </div>
          </div>

        </div>

      </div>
    </section><!-- #contact -->





</main>

@endsection


@push('scripts')


@endpush
