@extends('layouts.master')

@push('styles')
<style>
    .banner{
        transition: .5s;
        transform: translateX(10);
        transform: translateX(-10);

    }

    .na li p{
        margin:1px;
    }
    generic_price_table{
	background-color: #f0eded;
}

/*PRICE COLOR CODE START*/
#generic_price_table .generic_content{
	background-color: #fff;
}

#generic_price_table .generic_content .generic_head_price{
	background-color: #f6f6f6;
}

/* #generic_price_table .generic_content .generic_head_price .generic_head_content .head_bg{
	border-color: #e4e4e4 rgba(0, 0, 0, 0) rgba(0, 0, 0, 0) #e4e4e4;
} */

#generic_price_table .generic_content .generic_head_price .generic_head_content .head_bg{
    border-color: #d01f1f rgb(239, 24, 27) rgba(217, 16, 16, 0) #ee1d1d;
}

#generic_price_table .generic_content .generic_head_price .generic_head_content .head span{
	color: #525252;
}

#generic_price_table .generic_content .generic_head_price .generic_price_tag .price .sign{
    color: #414141;
}

#generic_price_table .generic_content .generic_head_price .generic_price_tag .price .currency{
    color: #414141;
}

#generic_price_table .generic_content .generic_head_price .generic_price_tag .price .cent{
    color: #414141;
}

#generic_price_table .generic_content .generic_head_price .generic_price_tag .month{
    color: #414141;
}

/* #generic_price_table .generic_content .generic_feature_list ul li{
	color: #a7a7a7;
} */

#generic_price_table .generic_content .generic_feature_list ul li span{
	color: #414141;
}
#generic_price_table .generic_content .generic_feature_list ul li:hover{
	background-color: #E4E4E4;
	border-left: 5px solid #2ECC71;
}

#generic_price_table .generic_content .generic_price_btn a{
	border: 1px solid #2ECC71;
    color: #2ECC71;
}

#generic_price_table .generic_content.active .generic_head_price .generic_head_content .head_bg,
#generic_price_table .generic_content:hover .generic_head_price .generic_head_content .head_bg{
	border-color: #2ECC71 rgba(0, 0, 0, 0) rgba(0, 0, 0, 0) #2ECC71;
	color: #fff;
}

#generic_price_table .generic_content:hover .generic_head_price .generic_head_content .head span,
#generic_price_table .generic_content.active .generic_head_price .generic_head_content .head span{
	color: #fff;
}

#generic_price_table .generic_content:hover .generic_price_btn a,
#generic_price_table .generic_content.active .generic_price_btn a{
	background-color: #2ECC71;
	color: #fff;
}
#generic_price_table{
	margin: 50px 0 50px 0;
    font-family: 'Raleway', sans-serif;
}
.row .table{
    padding: 28px 0;
}

/*PRICE BODY CODE START*/

#generic_price_table .generic_content{
	overflow: hidden;
	position: relative;
	text-align: center;
}

#generic_price_table .generic_content .generic_head_price {
	margin: 0 0 20px 0;
}

#generic_price_table .generic_content .generic_head_price .generic_head_content{
	margin: 0 0 50px 0;
}

#generic_price_table .generic_content .generic_head_price .generic_head_content .head_bg{
    border-style: solid;
    border-width: 90px 1411px 23px 399px;
	position: absolute;
}

#generic_price_table .generic_content .generic_head_price .generic_head_content .head{
	padding-top: 40px;
	position: relative;
	z-index: 1;
}

#generic_price_table .generic_content .generic_head_price .generic_head_content .head span{
    font-family: "Raleway",sans-serif;
    font-size: 28px;
    font-weight: 400;
    letter-spacing: 2px;
    margin: 0;
    padding: 0;
    text-transform: uppercase;
}

#generic_price_table .generic_content .generic_head_price .generic_price_tag{
	padding: 0 0 20px;
}

#generic_price_table .generic_content .generic_head_price .generic_price_tag .price{
	display: block;
}

#generic_price_table .generic_content .generic_head_price .generic_price_tag .price .sign{
    display: inline-block;
    font-family: "Lato",sans-serif;
    font-size: 28px;
    font-weight: 400;
    vertical-align: middle;
}

#generic_price_table .generic_content .generic_head_price .generic_price_tag .price .currency{
    font-family: "Lato",sans-serif;
    /* font-size: 30px; */
    font-size: 35px;
    font-weight: 300;
    letter-spacing: -2px;
    line-height: 60px;
    padding: 0;
    vertical-align: middle;
}

#generic_price_table .generic_content .generic_head_price .generic_price_tag .price .cent{
    display: inline-block;
    font-family: "Lato",sans-serif;
    font-size: 15px;
    font-weight: 400;
    vertical-align: bottom;
}

#generic_price_table .generic_content .generic_head_price .generic_price_tag .month{
    font-family: "Lato",sans-serif;
    font-size: 18px;
    font-weight: 400;
    letter-spacing: 3px;
    vertical-align: bottom;
}

#generic_price_table .generic_content .generic_feature_list ul{
	list-style: none;
	padding: 0;
	margin: 0;
}

#generic_price_table .generic_content .generic_feature_list ul li{
	font-family: "Lato",sans-serif;
	font-size: 18px;
	padding: 15px 0;
	transition: all 0.3s ease-in-out 0s;
}
#generic_price_table .generic_content .generic_feature_list ul li:hover{
	transition: all 0.3s ease-in-out 0s;
	-moz-transition: all 0.3s ease-in-out 0s;
	-ms-transition: all 0.3s ease-in-out 0s;
	-o-transition: all 0.3s ease-in-out 0s;
	-webkit-transition: all 0.3s ease-in-out 0s;

}
#generic_price_table .generic_content .generic_feature_list ul li .fa{
	padding: 0 10px;
}
#generic_price_table .generic_content .generic_price_btn{
	margin: 20px 0 32px;
}

#generic_price_table .generic_content .generic_price_btn a{
    border-radius: 50px;
	-moz-border-radius: 50px;
	-ms-border-radius: 50px;
	-o-border-radius: 50px;
	-webkit-border-radius: 50px;
    display: inline-block;
    font-family: "Lato",sans-serif;
    font-size: 18px;
    outline: medium none;
    padding: 12px 30px;
    text-decoration: none;
    text-transform: uppercase;
}

#generic_price_table .generic_content,
#generic_price_table .generic_content:hover,
#generic_price_table .generic_content .generic_head_price .generic_head_content .head_bg,
#generic_price_table .generic_content:hover .generic_head_price .generic_head_content .head_bg,
#generic_price_table .generic_content .generic_head_price .generic_head_content .head h2,
#generic_price_table .generic_content:hover .generic_head_price .generic_head_content .head h2,
#generic_price_table .generic_content .price,
#generic_price_table .generic_content:hover .price,
#generic_price_table .generic_content .generic_price_btn a,
#generic_price_table .generic_content:hover .generic_price_btn a{
	transition: all 0.3s ease-in-out 0s;
	-moz-transition: all 0.3s ease-in-out 0s;
	-ms-transition: all 0.3s ease-in-out 0s;
	-o-transition: all 0.3s ease-in-out 0s;
	-webkit-transition: all 0.3s ease-in-out 0s;
}
@media (max-width: 320px) {
}

@media (max-width: 767px) {
	#generic_price_table .generic_content{
		margin-bottom:75px;
	}
}
@media (min-width: 768px) and (max-width: 991px) {
	#generic_price_table .col-md-3{
		float:left;
		width:50%;
	}

	#generic_price_table .col-md-4{
		float:left;
		width:50%;
	}

	#generic_price_table .generic_content{
		margin-bottom:75px;
	}
}
@media (min-width: 992px) and (max-width: 1199px) {
}
@media (min-width: 1200px) {
}
#generic_price_table_home{
	 font-family: 'Raleway', sans-serif;
}

.text-center h1,
.text-center h1 a{
	color: #7885CB;
	font-size: 30px;
	font-weight: 300;
	text-decoration: none;
}
.demo-pic{
	margin: 0 auto;
}
.demo-pic:hover{
	opacity: 0.7;
}

#generic_price_table_home ul{
	margin: 0 auto;
	padding: 0;
	list-style: none;
	display: table;
}
#generic_price_table_home li{
	float: left;
}
#generic_price_table_home li + li{
	margin-left: 10px;
	padding-bottom: 10px;
}
#generic_price_table_home li a{
	display: block;
	width: 50px;
	height: 50px;
	font-size: 0px;
}
#generic_price_table_home .blue{
	background: #3498DB;
	transition: all 0.3s ease-in-out 0s;
}
#generic_price_table_home .emerald{
	background: #2ECC71;
	transition: all 0.3s ease-in-out 0s;
}
#generic_price_table_home .grey{
	background: #7F8C8D;
	transition: all 0.3s ease-in-out 0s;
}
#generic_price_table_home .midnight{
	background: #34495E;
	transition: all 0.3s ease-in-out 0s;
}
#generic_price_table_home .orange{
	background: #E67E22;
	transition: all 0.3s ease-in-out 0s;
}
#generic_price_table_home .purple{
	background: #9B59B6;
	transition: all 0.3s ease-in-out 0s;
}
#generic_price_table_home .red{
	background: #E74C3C;
	transition:all 0.3s ease-in-out 0s;
}
#generic_price_table_home .turquoise{
	background: #1ABC9C;
	transition: all 0.3s ease-in-out 0s;
}

#generic_price_table_home .blue:hover,
#generic_price_table_home .emerald:hover,
#generic_price_table_home .grey:hover,
#generic_price_table_home .midnight:hover,
#generic_price_table_home .orange:hover,
#generic_price_table_home .purple:hover,
#generic_price_table_home .red:hover,
#generic_price_table_home .turquoise:hover{
	border-bottom-left-radius: 50px;
    border-bottom-right-radius: 50px;
    border-top-left-radius: 50px;
    border-top-right-radius: 50px;
	transition: all 0.3s ease-in-out 0s;
}
#generic_price_table_home .divider{
	border-bottom: 1px solid #ddd;
	margin-bottom: 20px;
	padding: 20px;
}
#generic_price_table_home .divider span{
	width: 100%;
	display: table;
	height: 2px;
	background: #ddd;
	margin: 50px auto;
	line-height: 2px;
}
#generic_price_table_home .itemname{
	text-align: center;
	font-size: 50px ;
	padding: 50px 0 20px ;
	border-bottom: 1px solid #ddd;
	margin-bottom: 40px;
	text-decoration: none;
    font-weight: 300;
}
#generic_price_table_home .itemnametext{
    text-align: center;
    font-size: 20px;
    padding-top: 5px;
    text-transform: uppercase;
    display: inline-block;
}
#generic_price_table_home .footer{
	padding:40px 0;
}

.price-heading{
    text-align: center;
}
.price-heading h1{
	color: #666;
	margin: 0;
	padding: 0 0 50px 0;
}
.demo-button {
    background-color: #333333;
    color: #ffffff;
    display: table;
    font-size: 20px;
    margin-left: auto;
    margin-right: auto;
    margin-top: 20px;
    margin-bottom: 50px;
    outline-color: -moz-use-text-color;
    outline-style: none;
    outline-width: medium ;
    padding: 10px;
    text-align: center;
    text-transform: uppercase;
}
.bottom_btn{
	background-color: #333333;
    color: #ffffff;
    display: table;
    font-size: 28px;
    margin: 60px auto 20px;
    padding: 10px 25px;
    text-align: center;
    text-transform: uppercase;
}
.demo-button:hover{
	background-color: #666;
	color: #FFF;
	text-decoration:none;

}
.bottom_btn:hover{
	background-color: #666;
	color: #FFF;
	text-decoration:none;
}

    .pb-5, .py-5 {
    padding-bottom: 48px!important;
    padding-bottom: 3rem!important;
}

.calculator .col-calc {
    background: #fff;
    color: #23282e;
}

.calculator .col-calc, .calculator .col-help, #continue-register-modal .col-help {
    padding: 55px;
    height: 100%;
    width: 100%;
    display: inline-block;
}
.calculator .col-calc {
    font-size: 14px;
    font-size: .875rem;
}
.pt-3, .py-3 {
    padding-top: 16px!important;
    padding-top: 1rem!important;
}

.pb-3, .py-3 {
    padding-bottom: 16px!important;
    padding-bottom: 1rem!important;
}

.pt-3, .py-3 {
    padding-top: 16px!important;
    padding-top: 1rem!important;
}
    .calculator {
    border: 2px solid #ef042f;
    border-radius: .25rem;
}

.pagination{
    justify-content: center !important;
}
.no-gutters {
    margin-right: 0;
    margin-left: 0;
}
    .section.dark {
    color: #fff;
    background-color: #23282e;
}

.table.table-dark thead th {
    border: 1px solid #fff;
    padding: 30px 15px;
    text-transform: uppercase;
    text-align: center;
    vertical-align: middle;
}

.table.table-dark {
    font-size: 14px;
    font-size: .875rem;
    border-radius: .25rem;
    border-collapse: separate;
    border-spacing: 0;
}


    .monthly {
    padding: 30px 35px;
    border: 2px solid #787d83;
    border-radius: 4px;
}
.monthly label {
    font-size: 14px;
    font-size: .875rem;
    text-transform: uppercase;
    font-weight: 700;
}

.col-form-label {
    padding-top: calc(.625rem + 1px);
    padding-bottom: calc(.625rem + 1px);
    margin-bottom: 0;
    font-size: inherit;
    line-height: 1.5;
}
form {
    position: relative;
}

.calculator .col-form-label {
    font-size: 14px;
    font-size: .875rem;
}

.input-with-currency {
    position: relative;
    display: inline-block;
}

.input-with-currency input {
    padding-right: 40px;
    text-align: right;
}

.input-with-currency .currency {
    position: absolute;
    top: 50%;
    transform: translateY(-50%);
    right: 0px;
    font-size: 1rem;
    color: #495057;
    padding: 10px;
    padding-left: 0px;
}
.calculator .row-results {
    margin-top: 24px;
    margin-top: 1.5rem;
}
.calculator .row-results .time {
    font-weight: 700;
    text-transform: uppercase;
}
.calculator .col-help h3 {
    color: #ef042f;
    text-transform: none;
    font-size: 30px;
    font-size: 1.875rem;
    margin-top: 50px;
}
.calculator .row-results .result {
    font-weight: 700;
    font-size: 30px;
    font-size: 1.875rem;
    color: #ef042f;
    white-space: nowrap;
}

.col-form-label {
    padding-top: calc(.625rem + 1px);
    padding-bottom: calc(.625rem + 1px);
    margin-bottom: 0;
    font-size: inherit;
    line-height: 1.5;
}
.btn-switcher {
    border: 1px solid #dedede;
    border-radius: .25rem;
    position: relative;
}

.btn-switcher input {
    display: none;
}

.btn-switcher input:checked+.btn {
    background-color: #ef042f;
    -webkit-box-shadow: 0 0 0 1px #ef042f;
    box-shadow: 0 0 0 1px #ef042f;
    border-radius: .2rem;
    height:100%;
}

.btn-switcher, .d-inline-flex {
    display: -webkit-inline-box!important;
    display: -ms-inline-flexbox!important;
    display: inline-flex!important;
}

.monthly .bet {
    color: #ef042f;
    font-weight: 700;
    white-space: nowrap;
}
.monthly .won {
    font-size: 30px;
    font-size: 1.875rem;
    font-weight: 700;
    color: #19bc01;
}

.carousel-item{
    /* height: auto;
    position: relative; */
    height: 500px;
    background-size: cover;

}
.carousel{
    background: #eee;
    background-blend-mode: lighten;
}
.carousel-caption{
    right:unset;
    left: 10%;
    top:40%;
    width:60%;
    text-align: left;

}





.carousel-caption h3{
    font-size: 2rem;
}

.carousel-caption p{
    font-size: 1rem;
    text-shadow: 1px 2px 2px black;
}

.overlay{
    position: absolute;
    height: 100%;
    /* background: #ea424294; */
    background-blend-mode: lighten;
    top:0px;

    width: 100%;
}
.embed-responsive, .img-box {
    position: relative;
    display: block;
    width: 100%;
    min-height: 200px;
    padding: 0;
    overflow: hidden;
}
.img-box {
    background-position: 50% 50%;
    background-size: cover;
    background-repeat: no-repeat;
}

.dark .card {
    background-color: #fff;
    border-color: #fff;
    border-width: 2px;
    color: #23282e;
    border-radius: .25rem;
}



.accordion .card-header {
    background-color: black;
    border-color: #fff;
    color: #fff;
}

.accordion .card-body{
    border: 1px solid #ddd;
}


.accordion .btn-link{
    color:white !important;
    text-decoration: none;
}


</style>

<Style>
    .card.wow span{
        font-weight: bold;
    }
    h3{
        text-transform: uppercase;
        font-weight: bold;
    }
    @media (max-width: 767px) {
  .hidden-sm-down {
    display: none !important; } }
    .grid-item { width: 200px; }
    .grid-item img{
        padding: 5px;
        border:  3px solid #eee;
    }
    .grid-item--width2 img{
        padding: 5px;
        border:  3px solid #eee;
    }
.grid-item--width2 { width: 400px; }

.grid-item img:hover{
            transform: scale(1.1);
            transition: 1s;
        }
</style>
<link href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.0/css/lightbox.min.css" rel="stylesheet">
<link href="{{asset('assets/plugins/Magnific-Popup-master/dist/magnific-popup.css')}}" rel="stylesheet">

@endpush


@section('content')

<!--==========================
    Intro Section
  ============================-->
  <section  class="clearfix banner">
    <div class="containe" >
        @isset($banners)
        @if(count($banners))
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                @for($i = 0; $i < count($banners); $i++)
                    <li data-target="#carouselExampleIndicators" data-slide-to="{{$i}}" class="@if($i == 0) active @endif"></li>
                @endfor

            </ol>
            <div class="carousel-inner">
            @foreach($banners as $banner)
              <div style="background-image:url('{{getImageLink($banner['path'])}}');background-size:cover;background-position: 50% 50%;" class="carousel-item @if($loop->iteration == 1) active @endif">
                {{-- <img src="{{getImageLink($banner['path'])}}" class="d-block w-100 h-100 img-fluid" alt="..."> --}}
                {{-- <div class='overlay'>
                    hhh
                </div> --}}
                <div class="carousel-caption">
                    <h3>{{$banner['caption']}}</h3>
                    <p>{{@$banner['caption-small']}}</p>
                  </div>
              </div>
            @endforeach

            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
        </div>


        @else
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
              <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
              <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
              <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
              <div class="carousel-item active">
                <img src="{{asset('img/intro-bg2.jpg')}}" class="d-block w-100 img-fluid" alt="...">
                <div class='overlay'>
                    hhh
                </div>
                <div class="carousel-caption">
                    <h3>Los Angeles</h3>
                    <p>We had such a great time in LA!</p>
                  </div>
              </div>
              <div class="carousel-item">
                <img src="{{asset('img/intro-bg3.jpg')}}" class="d-block w-100 img-fluid" alt="...">
                <div class='overlay'>

                </div>
                <div class="carousel-caption">
                    <h3>Los Angeles</h3>
                    <p>We had such a great time in LA!</p>
                  </div>
              </div>
              <div class="carousel-item">
                <img src="{{asset('img/intro-bg4.jpg')}}" class="d-block w-100 img-fluid" alt="...">
                <div class='overlay'>

                </div>
                <div class="carousel-caption">
                    <h3>Los Angeles</h3>
                    <p>We had such a great time in LA!</p>
                  </div>
              </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
        </div>
        @endif
        @endisset



    </div>
  </section><!-- #intro -->

  <main id="main">

    <!--==========================
      About Us Section
    ============================-->
    <section id="about">
      <div class="container">

        <header class="section-header">
          <h3>PRVNÍ SKUTEČNĚ PROFESIONÁLNÍ SÁZKOVÉ PORADENSTVÍ</h3>
        <p>{{$content['about-us-subtitle']}}</p>
        </header>

        <div class="row about-container">

          <div class="col-lg-6 content order-lg-1 order-2 m-auto">
            <p>
             {{$content['about-us-description']}}
            </p>

            {{-- <div class="icon-box wow fadeInUp">
              <div class="icon"><i class="fa fa-shopping-bag"></i></div>
              <h4 class="title"><a href="#">Eiusmod Tempor</a></h4>
              <p class="description">Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi</p>
            </div>

            <div class="icon-box wow fadeInUp" data-wow-delay="0.2s">
              <div class="icon"><i class="fa fa-photo"></i></div>
              <h4 class="title"><a href="#">Magni Dolores</a></h4>
              <p class="description">Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>
            </div>

            <div class="icon-box wow fadeInUp" data-wow-delay="0.4s">
              <div class="icon"><i class="fa fa-bar-chart"></i></div>
              <h4 class="title"><a href="#">Dolor Sitema</a></h4>
              <p class="description">Minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat tarad limino ata</p>
            </div> --}}

          </div>

          <div class="col-lg-6 background order-lg-2 order-1 wow fadeInUp">
            <img style='margin: auto;
            display: block;' src="img/about-img.png" class="img-fluid" alt="">
          </div>
        </div>




      </div>
    </section><!-- #about -->

    {{-- <section id="about">
        <div class="container">

          <header class="section-header">

          </header>
          <div class="row">
            <div class="image">
                <a href="{{asset('img/portfolio/1.jpg')}}" data-lightbox="portfolio"><img class="img-fluid" src="{{asset('img/portfolio/1.jpg')}}" /></a>
            </div>
            <div class="image">
                    <a href="{{asset('img/portfolio/2.jpg')}}" data-lightbox="portfolio"><img class="img-fluid" src="{{asset('img/portfolio/2.jpg')}}" /></a>
            </div>
            <div class="image">
                        <a href="{{asset('img/portfolio/3.jpg')}}" data-lightbox="portfolio"><img class="img-fluid" src="{{asset('img/portfolio/3.jpg')}}" /></a>
            </div>
            <div class="image">
                            <a href="{{asset('img/portfolio/4.jpg')}}" data-lightbox="portfolio"><img class="img-fluid" src="{{asset('img/portfolio/4.jpg')}}" /></a>
            </div>

          </div>
        </div>
    </section> --}}

    <div class="container-fluid" data-menu="section-home" style="margin-bottom:20px">
      <div class="row" id="img-boxes">
            <div class="col-sm-12 col-md-6 col-lg img-box with-content" style="background-image: url({{asset('img/portfolio/1.jpg')}});">

      </div>
            <div class="col-sm-12 col-md-6 col-lg img-box with-content" style="background-image: url({{asset('img/portfolio/2.jpg')}});">

      </div>
            <div class="col-sm-12 col-md-6 col-lg img-box with-content" style="background-image: url({{asset('img/portfolio/3.jpg')}});">

      </div>
            <div class="col-sm-12 col-md-6 col-lg img-box with-content" style="background-image: url({{asset('img/portfolio/4.jpg')}});">

      </div> 

      <div class="row" id="img-boxes">
      <!-- <div class="col-sm-12 col-md-6 col-lg img-box with-content" style="background-image: url(https://image.shutterstock.com/image-vector/group-business-men-women-working-260nw-391331749.jpg);">

      </div>
            <div class="col-sm-12 col-md-6 col-lg img-box with-content" style="background-image: url(https://www.shopvac.com/data/uploads/media/image/enter-to-win.png?w=285);">

      </div>
            <div class="col-sm-12 col-md-6 col-lg img-box with-content" style="background-image: url(data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMTEhUTEhMWFhUXFxsXFxgYGR0YHRgaGRcYFxgYGBoaHSggGxolHRYVIjEhJSkrLi4uFx8zODMtNygtLisBCgoKDg0OGxAQGi0fHyUtLS0tLS0rLS0tLS0tLS0tLS0tLS0tLS0tLS0tLTctLS0tLS0tLS0tNy0tLS0uNy0tLf/AABEIAKIBOAMBIgACEQEDEQH/xAAcAAACAgMBAQAAAAAAAAAAAAAEBQMGAAIHAQj/xABCEAABAgMGAwUGBAQFAwUAAAABAhEAAyEEBRIxQVEGYXETIoGRoSMyQrHB0RRS4fAHFWJyM4KSovEkwtIWNGNzsv/EABkBAAMBAQEAAAAAAAAAAAAAAAECAwAEBf/EACQRAAICAgIDAQADAQEAAAAAAAABAhEDIRIxBEFREzJhcRQi/9oADAMBAAIRAxEAPwCxS7qnM3ZqHIhvnGt43atEozJyO4lnNKOWDMXgybfOInAtJYsWAz8oWcS3qsSkSSSozTjI2Qig6Oo/7YdtURjNuVE1ycOJnqV3yhCQCSKkvkK+MWFNzyUjCiUWFA6jzgHhNYTZMRcY1F32TT7xvY0hczFiJaoDFts4nKVaR1Y8dptmW+75FnkLmGWwSKDGSVLUWAqdyPWK3YqgnV6tDXjZavYo+ElSzzIokepMV2yz5iFkJS7t3d9A0FP6TZYbPdSZyQVkpDuMIcunXaFN6CxylFKhPWTUsUAepeLcuYESwnJgx65n1eOfJH4m1Jlu+OYE+D19AYHK2AfXhcUpP4cSysdooAhRCsKWSS9BUYh6w5VwrJSa9qsuzAgD0BiHiieRMRhAZIc1ZsSifkExaLWo4c8xRtKA/WKpIaNt0L7Dw/IOdmAG5UXy6wBfdzWaXZVzJcti4YlSjrlU+EObmWVSgSS9QSdxAXGgazBLgYlj6q+SYEqoaSplGkEKICmAAfP0jeb2QIAZWI5hmHMxBMARVSlAb4P1jaYpAHeClYviOXplEbDo9TYBmGw7vTzj0KSHaYnqdPONFWZOH3iejAesbdgUJfGApmGQD9TGDo9nyi2LtMTVZRHmGgmyTxsG3ALecDzpKMIOFRbMpo+7nURpPtuJISCAHqwy2r1gMZNjkJB0ERzZOKhBP06QqlXgtmJ6ftqxsqbMUKlQOjPGMzW12DACcTdY1QqWkjAHJ1ILeA1MGWW658wUQWBzZ683YesEzrgWEpx0qwqnPPR4HFiuURfY7WpyMOJROZLU0Ag78OkstSmV5NyYwQu6JiQFFupJ+ggI3VNW7Ma6cqQ5KWRekTTbKksokk6GlOmkD2malILLAWzAkvTk2UC2i7ZooUqOm/ygKZZiKEEGCjnn5DXSJfxMt6jEd9/ONgHDsgcgfpAC0NBFiAdmH38YLQmLyW5VI9VKWKgRpNvCfKdcqZhfMFLgnxgielQAAdyXqKDePP5clwZjrP8AUWA6AQq0drolsPFFswusSj4KH1hJxHeM62LSooT3RhHvMN9YdqIGcunIv6RrhlqBw0O2RhuTEcUUk3VOz7o6p/WPf5TNPxpHRMWyWUvhIIVsqh8DrG6pCTmPA/eNbAor4UC23VNRVSlEdYFF1uHCjF+tdhJoJZwtUO/jFatdnMlVAcP79YxpJ1oSKuk7xr/LCM4dTZwahDneMlpScy/X6QRIyk+xGLtfIFoyLF2WxjIakNbOnXZcoEw4HOM66eMJOIElVsmMoBIKZaSaMlLJPg+Iw/snF8mViOObNVoAhSU+a284SWhSFJdZc1JarklzWBLoTFFrbLvJlBMpCZRTgSkAEglx+agq5c+MESJgbN92SR5RTZfGa5clEuVZSrCMONUwJBqTQMTrAVq4lvBbBBkyQdUpKiP8yi3pCcVZ08tUWfjEuZNQFYSSNQCaU8Ih4bsQaZaClS8JZI1KmGXIfOK1YpSgortE5UxSg5KjtkOlTSDUdmsjCVd1u7iUE0ydIIBh0TY/nzVLDGzzRzfdv6eZ8oj4duCTLmdqUTUL91ONWKq3S4GEVZzCicuaXKewRzEqo/1KIhXaUKUWNsmA64EhI80AfONpAoY39acVomKS572ENWiO6KHpHQbLaEKlSwVpKuzQCAXIVhD0EcvTaAnCkLxUzws/nrGqFqDspQfYkfJoydCyyqB1awyBLSQCSkly7BnzZ2iv/wAQJwMuWgJcFRIct7qcIUG6mKPU5knqSfrE0uWpZCUuSaACsBu+hH5Nv6a/hzhZSyeTxJKs6lMhyrYJDmLDd/CtMc5TAZpTp1P2h7LWmW0qT2SFHUVBHXMHq8D8vpWOaT9UVGVw/aGKDLYKFMRakHWbhWdhYrSxoB7z9Is0i70oUStalqVuoln2eALYFSkhJUSl3QoHV38DWHjjvpBlNrti+zcLyQl1zFsCx0Y7GN02GQjEJcrEzAYnIJfMDJVHpu0DLtRLhSySS6qs/lG068FEjvdNG2aOheO/lEP+iP8AoxTIcmWJYIKTUoCShTOnTWPJVvkYSO6ksM6EHVt4a2S0dqhK3qKGpoenr4xR+JpSkT14Q6XcEVHeq3m8c001oryVXVlqu6xy7TIIWVMFqAZRTsXoYg4omJkiUlACQ5NA2QAf1iPg6aOxUF4knG7VGYFaCAeL7NMmrT2KFrSlNTq5OVa5AQjlHpszb4WkQ2riEql4cIHic40u++RKfNyGcRXJ0maiikLTrUEdIkueyrnzkyxrmdkjMxlBHN/0TTSL/c8ztXnlBCUhkjMqIFVczpBajLmpUqbLDAsygPQwRJSJaQkABKQ3QDUxXbyvnGru+6nLnzitpKjsx4pZGa2jhyXMBMs4Tsajo+cVy3XYuUe8G+XgYtNmtpUoBKHJzALA8zHlot6cSUzEYgpRQosSEtmwALdS0ItiZvGSf9lUsk7vMtXdowOp1c/SCVyQ74X61+cFXpdgQO1lF5ZNHzHnpzhRaLWpKXAxVq5hTQb6ZLMQz4aPvUQunpUC+o1EMZNoxJchhzgdiHw95O23QwChraLVLXLLllAONCDyiewze0QCpn16wDKlkuzM9HziMTFIUcgetDGsFDZ8JYmmhgK8ruTOSQSQd4jnW8KAAGEvV/pG8u2MQlT9fvDWaij2+yqlLKSx5jUfvSBhMIy/SLrfFhRMS6GJzLZxT59mwq7zty1jAaJpE9ywoRtURkbSZH5QwOuZjIdWSafouhQS1BzJYCJ5NmxDM93qG8TAabQvFmknWjtEotKzRRFNqP1iZYJMlBdJalWKiPHKNwpPwkCrO7+YUI3Qzg4QSNTpG1sm4E4mGI0dhTmYIAVVodeYUE6igMZZrUElZ1LMK7mIc2cv9eZiNaziGwLvGCHLtYaoxK2NAPCJm0DOdSwHgNYjRKSpBDM+sEGTLYp0Pn5wTG0yxADNT89YXYIPnqCBiK1AOMy/gxj02d1AAOVKYDck0jHL5EW2qNbsu5U1YQjxJyA3MXaXLs9kRhASpdMRXQq6Fmgi7buRJlYBVRqo6k/aA7xUUgY8Kku4SrvAEa8obpFMWJIllWZU4dolKkByAhR97c9OsJryUpKmKAg7M1OTZw5s/FMolpncO+afPSGKEonpdWFaTkAXA5vvFcWSKf0OXFJquirXfxBhITNBUnfVPTccjFgQtExBYpVLOp08M8UJb24bwkdirM+6rMDd9usH3Xd8uWO6Tj1JzfptCeV5GKCuPfwXBjyXUuhbNuEmYWUyD7pUK9GgpNwoSXUpShrox8NIaonDEELzPkfsYaolpGkcqz+RlV8qRf8ADFF9WJpF3JQRgRQ5/Q1jZFzrMzEqayX9wJGmQfxMTWy+CnEESZi8DuWwppzOfhEdit89SpajLR2Uz8hKimjgqO0COJdydla1rRl7YLOlU5amQAMRNW0enURWpXGdixK9uA51ChRhy6xebVZ0zEKQsApUGIOoMfPXGXD6rHaFIHelqJMtWdH90/1CFl48G7BzZ16771kTXKJqFPpiGQyp6+MS2azpCjMSkJKqOAxbnu+ccFmSShiWxbajrzhzdXF1rlMkLxp/KvvBtnzHnCPx2v4yNyXs6xeonTUYUKGF66FXJ8m+cILFdcxczAxBFSToN+fhBVzcVy55EpQ7GaQO6rIuPgOp5H1h6pk0Dvo2fV/vAWWcHUzohmcY8Yk1ksokhkpcHNWvjyga1zpRVhSlKlnPQf5jr0hTftvnghBICSKFPxdTCyTOOjg6N+2jsU1JaEj4zkuUmWaUgknGQQzMQAG1aKdf1iVKX3FMM0nNxsRuIsNiJmElbsA9C5/46QTarGibKKWCR8O4OhMbvTOfJDizntqt6ykUFdWr4R5ZL1KvZ4WLZ/vWDVWHDMIUMnBG0aLu3vOmm0KZOwBEzCD3sJGeo6xAuZiJBmFmegz84OtF2FqM4L9YGVJUC5H1A8oUJrZVIxgrFAKOHq9D1hgZiVKYBxqYAEjEphnyH3g+UBLDN3jkIYADOLqIAxbEaxDarCCnCvCCajUw3u2WyH1JL+eUBz04ppVmEhqRqNZWTikqwLy0/TlGRY70u/t2ADYUuDz2jIdMVoksy6UJJNG1pB1ju8uVKzOm0TXfYQh1Ed5VTy5QfKRiUmX+dQTk+ZavKAkZsGmzkoHeU3Sp8ors68UmYMJURqVs/hHX5fD9nlpCSgKOtHfwyaJ5UmRLDJkywOSU/QRTgZbOSWRM1Z9mgrepIBNdqQ1s3DNuXXsVeiW/1R0Zd5ITRwnowjaw3gFE4cTBnJyLnSNwQ3GVFXsfCKy3arQnk+I+gYQuvaymRNVKwpUUgHE7O4fJot1qChMUmpZTjpm2UIeNlAWnF+eWkjwcGBJUg8SvrlGarCpQZIeg3pSLZwzZQuapahSWKA7qoPQHziozFlK0rALEEKYPTSLzwr3gtiwKJS8t0kHP+2FjsnOPQytEl3CSoHlUGOd8UX6qSvsya4XJbFqQ3pF8mzDXAv0I6xUuJeDJ1oCVowqIcOlbOPERpIeH+lYsl49sjEzFyP1gu6lzjOSmQtSVqOYLdSdwIyycJWqUkjsVEcmV8jFl4Mu0I7Rc5OFb4AFBiAKksd3HlEJvgrOvmuNdlolS1pFVYzRyqhPiMvKB7fbEpYK7qiQATkH2I1pQQQrCASFMBUsX+cV/hq2rmqnzFMoEpKQwLVLMOQjjxx/SVsSqjaF9+y5tnJXLmKU8wBWNNQ/ukaGtHG8X2yW5JQFKUElqglmhbb8CkgTaIWClT0ZxSuh+0Kbfw4JgC1kg/GyqECgV4hjHoJUiTlyHF48W2SVQzMR2TWKpev8AEvCCJMkDYrP/AGiFPFVxpTZQqQpOILZeCjpLtzirXRdExS8CSA47ys2H3gWFJ3VBd68e26crBjUAfhSMA/21PnCSfeE1glS3wkkAe6gnMjdR1MdEu/h2TLFUY1MxUqpP2gubdUhQYyJbf2j6RJ5o2XXjSaORqWomtSYm7JYAURT7Zxbr+4MHvWam6CcuaSflCKyTQgFKgtVGIw4WcEa6xVSUlo55QcXTBbxvAzZmNsLBIDaYQAC+8dM4Fv8ANolGWqs2XmT8SdFHUnQxzH8OKOO6VNirQcxvB9yW82WemYlYIBZWHVBzzGevUQk4KUaNuzrd4WHtUEKNc06AH90ityLLMVRCTShYZeOUWeVOQpKVg4woAg50NRyjDagh3CgknQa+OhiGCdPjIsssoqlsAsV2TQcWIJPVzzoI1nhphSClxmVPV60Ab5wSm3IcFjTJ29W5QLa7VoUY+84SACR0xGsdf6QuiE/0ltkFsswmgrSlly/eCahY0Y7iE6ZmLKH9it6FnBLcNUhRCGYsaDUQmv2wlHfllkqybQ/Yw0iKtECxzgWbPQn4hC2bNcEKNdy79GiIbnStYQoGzLQAQQ76Uz5REJinKiQ6j1YDSNEylUKjhDuN/AQXKkuGYkDVVH8M4Jgbs1YnPu5jR3+UMJFpCQx8GHoYFlhOY7OmxI9Y9mKemMAHR3D+NRGAGSralZaojyAMBAKFsCA6SNoyGMNU21ByxHokwwuhZM+T8IMxOeZ7woBFds8/uEk9A4DxtZbStKwsE4kkEE1YjblATM0dcvFBOMA4XZlZ0gH+WS2GKYTuSpgfLKOczuILwmFQNqwIFO6lALcyzwv/AARm96ZMmTcVDiWojyJijkgxk0jpi75u+QW7aVi2ScZ8cLmA53HclL9lKXNOlOzT4lVfSOdqusSyCkUOm0MLOCBo/KA5szbZYrTxZaphcIlSn5GYr1YekV+02qdMnGZOxqoAl0sw5AUESSZyjmYkn29KBTEo9C3mzQrdmQNOtYKWTnQc84vPBMyoAq8ga1GCYpLxQVS+1ViyfMAvFy4KT2c6UCaKTMSXOxCoETTHc6xFKzNxFSmYqwB2yIYKb0iaz3tJloCUhYSmgcfrBU+YCfN/28Jr4lFZdKFMkD4TprlnDttdCxSfYRL4ks8sYVFTufh1JJj2xXtLnKmGWFKDp0auEDXpFVm3FaFnuyV5/Eyc3OpfWGdwXVaLOs40pAWNwaprpyJjnzcpR2dCjBdPY7tQCkKTgNQRkNQRvFZ4IwArBAC0nPVsiPA/OLUSttPX5frFCnzvw9rWSHGIunLEldW9fSObB20VgrTRY71RNmTZbgplJLqNCDnmzsGhhaMWEomYAmiUkFs9GyyekSXS3YpHZ9mlnCCXYHfnGipmMsUsTVL/AJhRuUdr6ohBbtHPrzvMgqQzjEwIqCxYGGnD9nAQVAB1Gp3bKG16XbJnMVIMt1OtaaF6Bm2zh7dlxSJSAlIJGdS+dTEpY7VIusqU7YkTLUfdS8FybnmqzISOdfSLAoolpJLJSMzkBCyfxFIHuqK2/Kkn1ygLFFdml5Em9G6LkRhIKlE75N0EVW9bOZayhYfUEjMbxbLpvbt8REtSUjJSvi6QBxddip0rEj3kV5lOoENLGq0LDK1L/wBbOdXuiSEr3IqBvoTs0U9Ri32xKMCkakEc/GKdbEFCsLENvrzgYwZuzon8N76KkGzK95FUOc0nMeB9DFymJKgQQljQ1P2jjnB85QtshizrY9CCDHZcJ/N8o588UpWSTEK5RS6Vs4zNajQ+sTSZmzOPPbeCrZZQpScSmcFOXlr1848F2s/eSabEU9YOPFOStFXlj0xfOmSUzBNXMSlWVXwkmjs7EwavDNBRjBBp7rfVxlG82wYkFCghSTmHZ/Shj2y2QSknCggZuVlXVnjshF1s5pNN6KRfVhMtRCsxruNCIXJLKBUcQAo2p5xcbxmyrSkiWpKpiXIAIJIHvAh3ipTJCUJUQak02rpGoVOtHku0rJxAOrnkNmjf+ZTCg4g7uKCogZc8soPUUfeJJGIUIAGlYw4dZ1JIAUHYBqZRMClXwg+EL+0rUtDKz5e7GAwG8wlICgDs37yjIOtspRQQlnO8ZBAAGzhsKShT12IaPJKiThbCRuX+WcbpkJUxKx/lDGDrquztLRIAU4MwP0HePoICGZtJ4YnzCMEtdT3sRCARuHIMPU8NzAAn2aQNMQ+kOypRtE1endlp/tS6lealDyhbe15qlAqSkqLOQxc1AcHlVxDNpaEtsyXw0VJHaTJYLMWBP0jeVw5IR70xRbZJHzBhJO4wYkdkoH+o5dRFmuu0qmoRMyBDtqetPlC8/wCjU/puq47KlJWQWGbk6+QgZFnsigsoQ2GWpR7xIDAl2ygu8C0pTfEoAeAc+sLUk/hLSSW9kpAriDq7oq5hnLZkVO6znkamrN5CLFc8yb28rspSpuAqJZgO+MPeJNNIQTZcuUgBgVafctpF5/hhaEntkB3SmXnmR3w8BDsP/CWpScU7BKJOXaEs5yZIb1iex2GbgImz0hDskIThJA/MVE+kWCeE5kVgRSUu4Fd9YohAVTEsCVGhfWiiWroxaB7wxskgAMsZl/e7unXeI7RPMteNagAeenIZ6A+JhRenEqSlSZaSf6jQUrQZnKEyNVTKY8c5PSG6pZPvKPh3flX1ij8ZSUpmoWjJSWcfmSd9SxHlF0QkEBROKj1y3yyhNxPZDPlNLDqQcQ+oG5aPMxyqZ0wdMl4bvwTpZSr/ABEsDzH5hDNVmBIVqKDl4RQOG0mZNlywgJwkqWsOFED4DydqReVTdWLsaCPQ52qJTx1K0aWpQX3VM2p1B0MS3BaiQqUo95FRzScjEJmskEAEksfqYUXvev4WYJwTiCWQoDMpU9fBgY0eti8WXKakEEEAg6EOPKK2bltC1OuYgJGSQlwB/bk/WKhef8WVpfsrLkWdai3gwisW/wDideMx8Kpcof8Axor5qJg0CztFluzAoLXOmKI3UyR/lygO9uMrDZ37W0If8qXWfJAMcMX+NtYUubNmLQASStZw00Adj5QnlyNoILOnXbPkT1TJkokgrURiDEAkkONIB4kkpJlhg+L0iqXdaFylOhTbjcRYLstnbTkJLzFZJAzffoIg4NOy6yJqmN+GLgSm2JmCqEAq3YkFIHqfKL+EA+76GA7tsnYpbMmqiPl0EFEJVUelDHNklyZN1egK3zyhaAGVmWV4DTmYyTeCdUmrZF301jf+WmY8wLzokH8o5jcvAE6xKBPk4qzD9YvH9ca10FLHJb7GiZks/EQeY+0bzk40YUzGzyYwql4nDM1Afr9I3tshSpaihWFSRiemQzFYrHO5aaJyxJbTKdwh7K8cK1OTjTi3OevQwRf9lCJ0xDUdx0PeDRYrArtM0thbvYSXLPTQHKE3FyGnJJJJKB6Exb0c2Xq0V6ZIcnEkhy4Iy9I2mS6DvMdCY3mOQ2Igco8wjDWo5wrRTHPkrB5AUZ6ErAo5BGsWMUziq9qETZawpwFVGwi1TGjDs8WXFKRkeAx5BFAZ13prhUXh1whNlyZhXOWEBCDhKqOpW3QfOEdhnhCcSvDc1iRd4qNBLpzaFTQ7Ref57ZSkET0Yg7s+b9KiB5fFVlFO0f8AtSv/AMYqEq3qr3GG8QzASrtCGH7rGbQFAtf81sJmdqEusguyDV9SC1YnXxTJGQUW07qR8zFQtSwoJwDvDbaNhawBhmIIfl9o1m4lktnFMxYZEhDDIEg/UxV71vS0zVATVlEvECJaQAKZOQKmI8BFUkgf1bRPaJLgEqxEMRtGchlEyae6SEnck55ipixcCXn2VomsCCZYHkpJ8AzxWAkFjV64hrkc/SJrAopXiSfeQQfJ/pAXYX0dmtN4gqOAFXSg8zFett8TCooSck4iUVCRl3lbxLLmyzLC5pLFIYAl1U2HWF90zGRMCUklYoGrqA+waGk30HGktsVWmYVPUudTyjSx3fMmEiWl9zoObw+TczDHMGIj4AfmdYc2RSSgYGCdAKNCRxN/yOqfmKKqCF1lsykBMuYrEwo1AQPmRBEya1BU/LrtCziK/UgGWiq/zaI6bmIbnvlCgELZK9zkvm+/KOTPjp3ERY8jjzaJp124FKmysONQ74VRKtdPdPPzil8V8XzZPswhUuYo4iSXZIDJwnwh/wAS8QGSqWAHQa1yU1G6R5b7sstuKJjJWspbmBnUDZ4fFOUFyltEXNN17BOFuKET0iWuk1STTQ0qaZONIzjJa8DIWlKgHUhx3kjTfIPBkng6zycapeJLpY1fLQPUPy3ja08OlcxU04SsjUqAZmY56RReRjk+6A1I5FeBUpRUsk6dOTaQKE7COn/+gu1PaLmhIV3mQl8+ZMH3ZwRZEVUFLU5DqUwoWyS0B+RBezcTlxsCyANRp65QfZeGLUsAokrVzIwjzU0dbslhlyk9xCUlJOQDkdehEFoLHD4jpqPCJvyfiDxRzOwfw/tC2MxaJY5d4+Qp6xdbg4fkWZJCEuv4lq94/YdIarOEvoc+XONLQsCrgHQ/vOJSyzlo1UbFRHMeo67xXeL7+TJRgQfaqFCPhTuftHnEvEarOkJTLVjUKKUGT1G55RzubMUtRUokqJck6xbDhd3InOdF9uXjxJARPThLUUn3fEZjweLDZlhSQpCgoHIgvnUnz+Ucjly/OGd3W1cpTy1lJ5ZHqMjHemQbOpCQFO6cqPq+piObYixALgggg7GhqIR3ZxY7Cclv6k5eI+0WORaUrDoUFDlG4xfoyk/TFNksRlaqAOdSQW1zziv8WzAZqW0QPVSvtF5JjnN92gTJ8xQyxYR0TT6GBJULklcRc9YjWkpPdLA5iN5iY8UlwxhWc+KfBmirPMUCwAiWx29SQ2EqApzEQS56knCVltKadYkV3Mga7xNs9FbQf/MCSyUEH+otHkAzQogEqz1yA6xkGzHtnJckkE/LpEs+ehNVKHQGFcmcpZV2aWGcW6ycAAMZs5yQCQEZPXMqrGr2xrFEi2hQpURKu0uGIiySODJCS+OYX2wp+hg6TcVlTTBiI/Mon0DQjcfoyTfoqEqaBklidBDOzXVPnMEylgbq7o8zFqlzJcqiUIR0SB6wVYLZjU+xbPlGTQzhKrEsrhME+2mgbpRU+f6Qz/l8qQQJctyzhRGI15mgjVS+8XL1/M3oKmJ7yFUU+AeH0g8grH9KdxwgptGIEjGhJI8MJ+UJrGklSRsHPRmiyccoDSF7pKerKB+sV671YZJWdZgHhiAhl2SlpHQbsBVZkLOSUMyak4aGumUGXfetnKU4FJTjLBJLKJ6ZmEHBl5MpVnVkXUjq3eHlXzg2dc8qTaBPQWVXuMC77HQRVEb5FjmTQkEqIAGZMVS9b2fEJTpQfeOqvsI0v61zQUGYn2alMGPuvqRr1gW1WdixGURy5H0d3i4YdsVzEF6DoYa3FdQX7SaPZg5Me+f/ABje7rt7RRKqS0+8cstB9YsD4ggSw0sgFKk5M1ARpAxw9st5Pk0uESO12GWtLjCU7GqR0OkVzh+8LF2qly5glrV3cCjhDP8AC9C7PSM49vYSJP4eWwmTA6yKMir5alm845o0NPFGX9Hnp+zuExeIpAy97wH6tG09bJUeR+UIP4e3VhsomLxPMJKakMkFg1cixPjDPiZPZWWbMStQITRyDUkAac45H4cvTKfogyXQAbAD0iGVPSMQJD4lU1qXy8Y5Rab+tUz3p625HD/+WjoP8OlvZS9VCYpyanJJz8YMfD+sDmMErV2pASWUHGLu5dR1je1SphSrApIWnKmKrZF93ie8lABKxmhQJ/tyV6V8I1mTiDVQAfZn5OY6I+PBehHNlAtd72hThcxQ0IHd8KVix8I3oCBKW2MDuq1UNjzEAcUXa0ztAGSuvIK188/OFNlQUkEHvAuCKN0iqio9EnJ+zoN6XfLnyzLmhwfMHcHQxzC+LhVZ1s+JBPdVoeR2VyjoV22vt095RBFFJFH5vtBlosctaDLUkFJ0+vXnD0A5RLSKAwQmU3Mbw4va5DIVugnuq25HnElkkBM0EgZAFJGJhR1fXxhG6MoORNckyWmWT2eJbnvEAhI0bm0GJsq0jtQWBqCDXoWAEe2yxdgoLkFgrNOaTAlpvbAlRmHCNUjU/wBI1flB3fZrSVJE143+uXKILFaqJORG5I5D1aKkTtlHs2eqYszF0JokflToOu8alcBs5sjvR4Y3wUjVSg1SB1gdVuSKAlX9of1jCKDfQUtIIyqMoGTNJIQxJ3yaI+3mlgmWA5YEl/SHtlkEA4mJNSWaFZ2YlKKpgkqyUZaqbD6xkRTpipcwqNQT4EfeMjFRzw/dntpQIdOIU6Rc75xB1DT/AIit8I2wTbShIdwCpj0i2zFuohnrCz6KY3UrI5EomWHNSM+sLJdnmdpk7GpyBh2sEByG60gX8QnIKBOyXV8okov4WWWga0XcFKBBbfWDLrsgQQxJfP8AQQPaLwRLqshP960o9DX0hXaOKpafcUFf/WhSz4KUyYeMJWJLK2qHBkKKiw1zf6CNb/t0uRL7WYhS8CA4S27ByS0U20cWTphIRKmdZqi3+hDD1hVb7RNmOZi3H5U91FP6deph+KQjm2FXpxPMteEJkiWiWSoOcSi410iGZOKJaFZgqLjkaiBpRHZnCG/4iJaiqUo7EH0jXsVrQ5t6ihSZktRBSQQYt9w3giekTPifvg5g/baKimoHMD5RFJnrs6xMl+I0I2MNZz4506Lvf9mC0KVNmZA4Epy6NqTSAbqs0yaAlTgBqnMJ++wie7LyRaQBL9/4gc0czv4QcbQqSrCUeyHxipfUqG3ONxT2zqjOSVIB4jmlElKJYaU+EncjT7nWJbntqZNj7RaiQl8+rBI9PODbRLlWiWUAgjQj4ToYRXhZzPUizoHsZVCXZ1anm33huO7D+iePg+yiXraFTpipi/eUX6bAchEFjsJmLQgZrUEjxLRZLz4amSwVUUkZtmOZEGcD3c88zCKS00/uVQemKHoR0kW5UtUoJTL90JCQOgA2hRxrOX+CUF0KloA01c6nQRY5kpKiCQ5GR2iucaoKxJlAVUsnyDf90CiaezmolVi//wAP0AyZiSTSY9C2aAPpAi+EGDlZURmEhvIn90gvhwmz/iAsNhCT8x9oA72i1CzIb3RUN50hemwSyjEoFSgCHUcRBT+V8qiBJtvKV4e19oQ4Q1KhwHy8o3ftZYU1SAsAf6VgeQ84ydiVRi7bLnycKnSolgCMlPQ9PvCqxWUIJXMlghyM8iN4nvCekJIUXBTQDN2yAHxO0FIlrTLQZgcrlgTAfz4a+J+kZpgTXtEE2YEKSpCMNaEPhPI6Qyn3kBLxBKlE0wjMHWunWAbXektEvBMWlLUckPTlCT/1hIlH2eKZoQAwI6mB/rM69IYXHZLUqk5RMkOwXVSgS4BetKh4ntllVJII7yR7qviR/STqmEczjS0TH7GUlA3WSo/QQkt96WqYD2k5VfhT3R5DPxgckhuLex7eV8oR3XxHRCTQddors+0GYvFNUCr4UJqE9ANecBiyJFcRKs2Jp5Q3uVKWKyAFQOQrx32yDCs/B50iBIW9W5pFPWGs+cBQ5qMemy4k7EZGFbYY44r0A2nslIwiUcW5OR6xPZZXdyAjyWUqTm2njG6QfhBPPJ/OFdsppG82XUHIjX9I9lWleMBRBCsqNESisM6P9wjxVoBzChzbKN0Y2tU5SSQtIUgxkaJvHJKg/Ns+oMZDAGly2yTZ1matRQMCkhQGIuSMh4GLKm9rOqWmYq1rKVBxXATyYB4UWS4JZS1oJWdUpOFI5PmfSD7JZrPJbs5aEvkWc+anisMUmiU80URzbyWof9LZTN2VNLDr3jGosdrmpafP7J80SqNy7v3je8L1wLZ3o/TlAE6/FEnAAR0i8cH0g/I+B1j4bsqTiZUxW6z9ocTpaJctJly5aXzp+zCK7xOUl0UTqTrWrQ/tQ9iluf7pWG/NJifo2eXdOKlVJLghsJGnOOeIUO+n4k4nB5PWL5dPvir11DfMkxSJkgJm2oa4157EkhuURzpHRgk6FyVMB3sIUK0eNSsJUQC8sgR5ZUJZ1KzSCOeca2RNClW1I5jo7HVmWChLZYRG660gewj2aeTjyMSEwxwz1JkKEqlqEyUopUMm/dekWa6uMUq9nahgOWMVSeozHyivPEcyWDmIKY0cjXZfVrRLlNIIOJ2IqwOZf5RHdSO7gy7zq3O3hFAlJXLLyllJ12PUZQfJ4hmp/wARAVzHdP2h1JFo5E/Zc70UyWJ7rZHMkP6V9I34bsglyQWqvvHpkPQDzipDiCSv3xMS9CSHprkYtUniWyEBpyQNHcfMQyoMnrQ5eK9fU0fiJeLJKC7aYjn/ALRByb9sxynIPIF/lFXvziGziapWIqySGGw583jaNEsibQOzC8b1D1/qqGiCSpM2fM/KqXh8iK+sUi0cTEFpckkbmIF260rL9pgozJ2O5hLiPReJVhlShitCkBYolb1CWYUOo+0KpvFtls6ZaULVMwOksGcGpz5tFfl3Yn3pilK1LmI7YqUU4EJBJLO1BzHOF5JdG4hl48cup5UgJVmFlnPi0JJ14Wy0H2k1eHrQdBl6QXLsYLJoWg2XZDpTrC82NxSKtOu8l3US2hNTzhjKliXLDAA055w0VYMVFUIyIjyz2HCXNTuYFmALSFIDA1Vk0Bzu78ddRnWH9plYhTMVDwnngO5GFQ5UPjGQbIgl/eBelYLs2BKgoqy5R5KmL3SREf4cEuTiJ0GUEAZKndsoFiBo8H2uYtCUiWzqU1csojstnwjvUbaNLJP7aaVA92WKDcnWMCwix2PA5LEnPl0ga0zVLmFLkJTTZ4ZKNCdWhLYVFLFR94uPqIFmDrSUSkPhc5AbkwuM1aq0QOVXhpbwkyiS2TpfcZQuu/usFHNiOhrAMTiR3XUQ25jIjt80LKZae93nU2jRkMai9PU9YRX2e7K6H5x7GR6GM82YpOR6/QxbbplJwJ7oqK0jIyKS6JLsdykgCgiO8P8ACHUxkZEvZX0Q2ZACpbACulIpt/f+5n/veMjIj5B0+N7AbKPYj96wptxqegjIyOU6hpdn+EOpgkRkZDHDl/mzWZHmkZGRiZFG5jIyMYgmiN7IgE1AzjIyGHh2AKWTNYks4pp5RvNlh8hntGRkBnaugmZA9mPtDHkZExgy+T7HxEK7GaCMjIL6MhpZD3jDBOcexkTQzPVREuPIyGFIxA16DuRkZBMIF5w54eHc8YyMhkBht7qIlloi4dyX1HyjIyB7MF3ge4ekBKH/AEqfCPIyMzAdlUVTEhVRsaw3t6BhVQZRkZGAaXakABhGRkZAYT//2Q==);">

      </div>
            <div class="col-sm-12 col-md-6 col-lg img-box with-content" style="background-image: url(data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMTEhUTEhMVFhUXGRoYGBcXFxgdGBkeGB8XGRoXGB0gHSggGBolHRcYITEhJSkrLi4uGB8zODMtNygtLisBCgoKDg0OGhAQGy0iHyUtLy0vLS0vLS0vMjAtLS0tLS0vLS0tLS8tLS0tLy0vLy8tLS0tLS0tLS8tLS0tLS0tLf/AABEIAMIBAwMBIgACEQEDEQH/xAAcAAAABwEBAAAAAAAAAAAAAAAAAgMEBQYHAQj/xABEEAACAQIEAwYCBQsCBgIDAAABAgMAEQQSITEFQVEGEyJhcYEykQcjQlKhFDNTYnKCkrHB0fBD4SRjc5Oi8bLCFRZE/8QAGgEAAgMBAQAAAAAAAAAAAAAAAwQBAgUABv/EADMRAAEEAQMBBQcDBAMAAAAAAAEAAgMRBBIhMUEFMlFh8BMicYGRobEUwdEzNOHxFSNS/9oADAMBAAIRAxEAPwBp2SwILd8UzbZRpoo23/irSeHzq4sbjqP7gi9Q/Z/AhUCZSPO2l/I/5pVlTh6m2bcc9j/OqN4TTS0DdOBEtvib0tmNvIWN/amd8PJoJImINrZgrD2OoPyqTVQuwvbTcW/vTXH8PgmI72NGbT7QDelwQSPI1dQHEHYri8MAsbnKDfU7e9zp5C1VqPgGGmheTuDC7FrLKZFXQ6EDMPCwsdNiT0qw4vC92oWAGNTr9WotpyNrsfWxHUimDcLFg7yZ7nQq3h10+K5JHXfbpVXPI4Cru7kqG4h2fVcNEgghD94FPgVlN8wBuwJ6ak3pr2hyRlYkWPJEAoBjQjNuxAKm3LbmKs8GISK6qMoHUG3nqTaofFyYLEhh3qRsdcytYa8yrWvqRsdalk7CgyROPBVMXi8JYL+So7E2GUBSdtspF/hHTYdKtnAcBEZUIiC5fFo76Zb20LEf4OgouB7IvCxK5JOhXMpF9yytIbH296Nh8W95oIUvORkLfYhDD45GGl7WIjBzHTYeIFfIxrC5xCCGnWAAVSGSbF4yRY9XlkZgTqI47/nG8gLWHM2HppeA4FHBGscLSJlFrhibnmzK10Zibkkre5ovZ/gkWEjyR3LGxeRvicjr0A5KNB+NSTPXks7NM7qb3R91qk2AOgTVu+XYxyDzuje5GYN8lpM8VVfziPH5st1/iW6gepFOWekyaQsHkLqQixKuMyMGHVSCPmKBemk/D4mOYoA33lur/wASkN+NJnDSr8ExI6SqG9gwKsPU5qmh0P1Up7mqr/SPPbAyKDrK0cY/eYE/+Iapn8rlX85Cf2omDgeoIV7+QU1QvpD4yks0EIawQNKwYFTmbwoLMAQQMxsRfUU52bjGTJYOl39N1SR1NKgGpDDhiLtzOgtsKGJRjYDYnU+VGmlCC59OX9TXv7ShKTkkOYBdfn/m2tGkbxBbXvvfkP8ABQwik3ZttevPf8Lcr0thRoznY6j0GnzrhuotPMDipYrd1NLGByjkdB8lIBqx4Htnj4/9fOOkiIw9yAGP8VVLAhmbXlry58tP801ANShXKCTsKkMY4bhUJV3wP0kzjSSCGTqUZk/A56sGC+kPDt+cimTzsrj/AMWzf+NZPg0CJmN/FqSRqb7aC/lT6CYG9r/5/ntVDjRnyVNRWwQ9q8DILd+gvpaQFL35WcC9cxvCY5wrK/eKAcoEjFdbWJAOWSxAILeIEaMKzfh3DJJhePLbXd1HwkLrc6XLC3Wjw8FlDkCPK98pIKqc1g1swI1ykHfnSzsdh2Dle3DorbKnEoAio4mBuG72Msq2taxBLkHXUsxFgNb3Efj5OISOGfC4V2GgYQsxHS2YnSnGD4TxNAMssijoZVf/AOeepFMXxOP4445B+x4vmj//AFoBg8HA/REEordoPryULiTxey2M98w8KLh1RVsT9zN8QA32YdDV64S8pjHfLZ/bXQHW2l9xy2vYXtUJH2qkFxLhTfpFIpPykEdvnT6LtRAdGEqH9aGQqPV1DIP4qj2bhyqukDtgAFgXa/ir4HHYnDKDlWV2X0lJlA+TihWr8bwfCMRO8suLwmdrXvNED4VCi4LXGgFCus+CGYISbJT7hCErbW67+d+dSETSi4snlqQf5Urh4Ao3ynrYf2p4mHPNtPQf2qoCd1BMS836u/Rv70YQSsurqNTbwX09z/lqkiQOX4UR31qVUyeASMMbLa5B67j0IHW9VX6TMW8UCtGSoclXZb32uoOo0NjrysOtTXFjKFd1dVVRmHhNxlGtyDZgehrOe0XbN373CyRoysCoNvECNQdDl3H+9DmIDSLU4mXHFmRNkF2Rt9r+SU7FcaLRuuJErqGASXIzqv6rkAkciNDbyFSHHeFwGMyhkUG1pF1VjyWw8JJ2tvUH2M7TtEhwqd0hZywllYhFuANQB4jcaC6g9RzvOF4WFYSSs0sv6R/s35Rr8MY9NTzJ3rNlyI4WA76vL91o9osc3LeKoX6PrqonhmGxUqAO7RRWGtgs7dbWAEKnrbNb7p1qUThEKgBECEbMl1fU3N2GrXOpve53vTydwEc3FwNKr3ZTjzYhZA48UbZbjnf+RpKaHJfjjJJpl8fZZonjMpi6qWLTJ0mX2WT+iOf4PejYfiEbkgHxDdGBVx5lTrbz2pQtTbFYaOQWdQ1tuo81O6nzFZlg8j6Jik7z0W9RxhmT804kX7kpN+XwyAE6a6MrEnmKNDxNCwSQNE52WSwzHXRGBKOdNlJPUCo0HkbqbT40L10qBvSGOxscKZ3JtcABRd3Y6KiKNXcnQAVQAuNNCnhJcV4gsEZcgsbhURfikZtFRRzYn+9SXZvsuEhf8rRJZsQc84ZQyDSyxKCLFEXwjrqedd7N9nnMgxeMUCUAiGG4K4dW3JI0aYj4mGgGg0uTa8tep7Owv07dTu8ft5JOSTUdlRuI/RhgZLmNXw7dYWsv/ba6fICqtxT6LcWmsEsU4+694358xmVuX3a2O1C1arZHN4KFa858R4FPApXEYeaEfeyZo+p8aZlAPmRvXMMqFRlysulrEEabV6NqnPwbh+OkcNhRnF/rogyEgGw8a5WvzsbjmCRrR2ZVchQVlcUYXYW/l7dKTxqliqAGxNybaAC2l+uvr/OtDx/0YsNcNij+xOgYegdMrD1Iaq7juzWOgF5MMzgbvhz3q/w2EnySmGzxuFXSEQ5Q2IwecAaWHKwIPIX8t9KWweFyKFHvt/YcgB7CjwTozFQfEN0Ojj9pTZl9wKdotHochUtWXsZwYzLISR3YI8DqGRmsdTzUgfaUg6jpapjhvAsMzsph7uRSLqxVwejJmBJFudNewPEFQtCxtnIZT1OxX1Itb0qz4eENLI7KC4OQDS4Uaj53v71k5LT7VNxye7ScsjKNGY2HRT+FgfxpjheOeDNLFJFYkeIb25gfFYgX2rvGcUERmMMrWH2Cb/8Ai1x68vSh2ZaPuhlkEji5Y5iWGYk2N/FYbC/QUEHdTp92ylnxMEy+KxHUi1vQ8qi4o4muYZFkUGxKkEqejdNjVjkw6HdVPqBTf8jjRXyIq3FzlAF7bX60Vj3AoL2tIUT3f6x+dcpcqKFMWllEiHISMLMUH6KQFovQA+JB6aV08fMWmIheO/24zmjPp09Bc0wxHF8NFov1zDpog9/7XFQuM7Qz4i8aXI2McKk/xEbe5ApBrnAW+lsiO/h4nb/f0U9xDtPGn5n6xjzNwg9Rux8qr0naTEgl+/K9QcuUexFhS2E7NTvrIViXp8b+lgci+t29Kd8I4bFBN3cyBpGJMMzahxuVAPhjkXoLZhqOYC0mdE29JsjoFc+yb0vzKNw3jWNnFu5jaNhYyOGjBB6WN39lt5is47RYcR4ggkHUgEbcxpWy46QrG7DkpP4VjHayM5vMf0pOPIdkEudsAsTNyRj5kD2gWDq+hFD4JhwqDENOHw8bSMhDEKLgD9blY7edaQkssX5kEp+gOhXyjJ+H9g6dMtIfRRjY/wAnnjH50tnOm4sBofI8v1qk8PIM7X11rOnyiZzDp2AXpZ8sZf8A2gUOnjujR4sTROQSLAgixDK1r2YHUH+4qG7B45TC6AeNZGzaam50b5ae1WDHaROduX4Vl/ZnifcY46gK91Oumu34gV6GXCH/ABoaOe9+9fT7rzcWSXZrjW3C1cEmjBgNzUb+X30sacpe128I89/lXk3CluJ0Zxy1rksRkUqygqdCGAIPkQd6R/K1UEgaDUsx2tz8qqXF+3LvePB2tscQR4R/0lPxn9Y+H1omNiS5D9MQ/wAKj3ho3T3j2NTBWEcpMpF1w3xgjrqbwp5g5f1TTzsTx7Cd732OYpidQhcf8PEp0yxNqFYjdnsTsNNKoMSAEsSWdjdnY3dj1Ynels1eyw+yI4W242/x9f7SbpC74L0ZG4IBUgg6gjY+Yo1efOD8ZxGFN8NK0Y3KbxH1Q6C99Stm86v3BPpPU2XGRFDt3sQLx8tWT4015DOBzNGfjvb5oa0W1cpvgOIRToJIZEkQ7MjBh8xz8qh+NcSkZmw8K5XawR3zqCRcuF0FyBYghrNZgSulwLklxzFvM5w0I5lWYE3BIHLTwAE5ibjkLnQIYufJGcJhwZJAQrscmthmIW5Ck2BAQEZRbYWauiH8jhEUI/4qQZrKQ2W2rWzABlFiAAFLW0CgXRfhuE7iNsQc00rm6aMSA5IXdc8YswLX2sfMnlyl+Fxd2ojeTO9rm7XIv02OXTpT6q6f+GRp5AXxEnIAmxIv3SlQdBlNutgN7Ufs5isRNZ5CAo0Bs/1lxcsAQot8BDW0u62uCa5cpLifCYMQuWeGOUcs6hreYuLg+YquYr6PoN4JZYeeUt3ifJ7sB5Kwq4V21Wa4t4KggHlZtP2VxkWoSOcDnE2R/wCBzb/zrq8caNwMQHjbQXkDRPcaCzkZZNOVz61pFEkjDAggEHcEXBq7pS8U/dcBpNt2VWGJLC6zTXNtGZALX6gDl51LnBRMAe8Nx9oSEkelyf5U2xHZHDHWNWgOusDFBrzyfm2P7SmmL8BxcWsUsU40ssoMb+ZMiBlPoIx69KBkakvepSeIgHLiGudrgGx5Wy5aaYaNY0ygks1mkf77AAFrXst7XsNKYyY94rifDzR62zBO8Q+eaMtlXzcLS2ExkcozRyI46owYaciQdDRWRtG4QXyOqk4vQoBT1/ChRUFQGG7JwL8eaU/rnw/wrZT7g1NQxKgyqqqo2CgAfIaUpehavDSSvk7xJW18UWm3EMEkyFHBsdQQbMpHwsp5MDqDTu1dC0KyDYXKAw2Jc5sLiD9blOR+Uy/fHRh9pffaqbx7s3iJGOVAfPMAP71o3FOFrOmViVIOZHX4kYbMp6/zGlR/DMaWkbD4gBcQgubfDKmwlj8uRH2TpRTM5rS6P5j9x5fhJT4MU7w592OFEfR72TbDd5LKwLFcoC7C+p9ToPnTrCTquIMbaBjofPp71bY47RkCs97TPkkLXtbWstkz5ckudyQtPHjaGFg4UzxwDIVVt6zjB9k7zZ5XUi/wgG3vfcVd8PDJMoIBsdbnaksscUnd+OefcQxLmf1YbIP1nKit4Z2UWiFrienG6U/SRNcX1uk8LgpsPrADLFziJGdR/wAp23/YY+hG1L4vtJg40DZmkkN7RKCJbjcOpsY7HfNapjDdncXP+ff8ni/QwNeQjo832PSMX/XpXjP0c4SeNVRBA6/BLGLOOfj/AEoPPMb6nUUzD2b7Qh0+3w5+f+FLpa7qzDi2PlxR+usse4hQnJ5FzvIfWw8qbWtTnj3BcTgXyYtboTZMQgPdN0Dfo28j520F6bpHevVY0cTGaYhQSpJJsrgFdkJA0FyTb09a7LC58K+Eblt/YD+tHVFjG9thcnUk7a+pphdwksPGw1Zrk8uQ8hzpRJQTYEf5v/am2KxRuVXoddb32AF9tT8WttOoo3dqmrm5voddNOVyTtfW99SB0rr8FyeYXEvE/eQyPE/3kJBPQNyceTAirrwT6TZksuKiEq7d5FZZPVkJyt10K+QrPe5Zmu50Gyjy533/AM5U5Vao6Fr+QuW7cH43g8YAYXSRls2VhaROhKMMy+tqmK85GLUEXBXVWBIZT1VhYqfMVaOC9vcbB4ZCMTH0kOWQfsyKNRvoykn71KSYrh3d1FrWsfw2OUeNdbWDDRgOgPTy2PMU4iiCgKoAA0AAsB6VXeBdt8JiSFzmKQ/6c1lYnopuUc/sk1ZaWII2KldoUK5ULkSaUKCSbAf57nypFZHOwC/tat7gbfOi4/eMn4Q/i9wwB/iK1W+0HBZs74hZV013KlVXkCN/wo0UbXGiaSuRM+MW1uqug2+am8ZxVYmCPKgJF9VYC3mbm3vTmHGggE2yn7SkMnz5e9ZfPiWc5mYsdNSbnSrZ2c7QJljg7tr/AAgqQQxO5O1r6k703NgljARueqzcXtYSylrvdHTlW6o7H8Ew8zZpIkL8nAyyD0dbMPY04wmhZOS2I8g3L2IPtanDGs87LbBsKC//AFsD4cTiVHIZ0a37zoWPuTXamr12p1HxUaR4Kq8ExonhSUCxIsw+6y3V19mBFPstQeEX8nxrxn81iQZU8pVH1q/vCz+xqw2rx8gANjg7j15cJ4FJhKOFo1q4xoJKlFJqJ4/wcYhQVYxzRnNDKPiRv6qdmXYipKaVUUs7BVG5YgAepOlQGL7UodMNG0x+98EQ/fIu37oPrVo2SOPuBWDdRoC044LxZpQ0Uq93iI9JEGxHKSP70bcjy2NReLxEDzFIYjipgdViGYJ/1HJ7uL3IPkahON4WbEHvJJF7xQQiquWIAm5R/tujWsbtzOg533sJxqDEQZIokgeGyy4dQAIzyKgWBRtww39Qafg7Iie/W415D+fD1atM2aBoJFWkcP2bnmH/ABMvdJ+hw5INujzaN/AF9asPDOFQ4dMkESRrvZRa55ljux8zrT0ChW9FCyIUwUkC4nlFC0a1MMbxaOPOoOeREZ+7QjOcovbUgA25E669KrE/FZ8UyLEfq5EHeQgFWUH4i0mUlo3F8kqZLEc7+EqhWxxDOskZ7uVdUkTwsL2BKuORsQbHqKzPtN9G8kBMvD7umpbDMdR17lj/APE/joKv/DMGuFD3ZUjcqUjsAIy1gUDfaUuSQLADMbaWAlZplUXdlUbXYgC52GtWa4tNhQvPWHxIa4F1ZTZkYEOp5hhuDSeMwwbW9jtfW1juCARWzdrexOHxvjN4pwPDPHo46BxtIvkfYisk47wrEYF8mLWyE2SdfzT9AT/pv5HztT8eQ1+zuVFKL7wIckYJbbra1viJ1O4/CutGqeNzdradfMLTuOIDYW52FJT4UPa+hHPTkQba6EaA28hzFHorkyLSM1lBFiRzAHNWJ1B0t89OtSEUNr+Zv+A+dKYbDhRZRsPc7anqfOksROb5U1b+XmeX486kbblVtCeYLpuTsBz/AM/zyPCrW8Vr+X+b0mngHjYsSfPc6aAnQf39K5hsWz7DXc25WIDLrsddDzsdrV17qhKdNCCCCAQeRFwfapjg3aDF4WwhmLIP9KW7p6KScyexsOlRwFGCVz2NcNwqAnotH4P9IsD2GJU4ZubMc0P/AHAPCP2wtXGOVWAZSGU6gggg+YI3rC1WluGzzYc5sNK8PMqtjG3M5ozdDfW5ADedJyYn/kogk8VtzqCCCAQdCDsfKmOKwWZDGfHGfsliGFujc+W/zqocK+kIiy4yG3/NgDMvq0Zu6/u56ufD+IxToJIZEkQ/aRgR6HofI0qQ5h3ViGvFKH4fwKKGUShZdAbKyhrE6Xut+V/nUj+TpnEiQAOPtsAu/X7R+Xyp8XohkqzpnuNk+SFHjRxjS0AC7+aGGiy3ubsxux6nbQcgAAAPKjM9JZzRhHQ0cCl3PQo4joV1qVVe0+CaWAmL89ERLF+0muX0YXX3p5wrHrPCkyfC6hvTqD6G49qTThWOk+OaHDjpCplk/jkAQf8AbNUHjXB/yPHCCVnlw8wLxd4brnJu11FkzFr7L9ta8+3s6Ux+/tXz+Pr4puMh7w0dVdcZ2mwyEqr9643SIZyPJiPCv7xFROJ45iZPzaJAvVvHJ/CPAp92pqpsLAAAbAaCiSYgKLsQPX/N65mLG3pZ8/4Wwzs9rd3m/siNgwxDys0z8jIb2/ZX4V9hS5NN1xeYjIhI5tsN7XF9+Z/90WfDFicztlOyjTpcEjcafiaZDaTbGtaKYEYsDexBsdfLnrTQwyRyricOQs6aD7si84n6qfwNjTtIkQEgKo5nQfM0QYlS2UG5vYkbA2DAfI3q42NhXeGvZof1WidmuPR4uLvE8LKcskZ+KNhurf0PMU441h3eJhGzKd/DuwF/ADmUi/UMuttbXrL88uHmGJw35wCzoTZZlH2G6N0bl6VpvZ/jUWLhE0R02ZToyMN0ccmH+/On43h4teZy8V2O+jx0Kj8Dw0QqJpSsIVZBKFPhdSxZWkJJysLs2hNi7WNtzpjsiiPCQr3UfhLE5Y1CjZTu3S4vvfWpbiLuI27tA77AE6a6XPUC9yOYFNpOFiTJ33iyggxj8y1+bIdDYXHTU6bWIlVGSNHLmli+sKsCGnzd2oN8xiFvFazaf2Ap/wALwcbhZmk79jezn4Rr9hdl6dfPU03x3DoVYyYhyyBgUi+wCzKq2QfGxZrX595qNaj8V2jeR0SC6r3rRsyZWk8KM2sbLoLi9hc2Rxo2lcuVuvSOLwqSo0ciK6MLMrAFSOhB3pjwxp3KPKuQGJLoGFhISS/hy5hplsSx5iwN7ylcoWTdpvo6lw95eHgyRbnDMfGv/RY7j9RvY1T8LiVe9rgqbMrAhlPNWU6qfI16INVTth2Gw+N+sBMOJAss6DXyWRdpU8jr0IpiLILdjuFBCyaaNiLK2U31PO3l0Psdq5HEFFl/98qdcTwc+DkEWNQISbJMtzDL+yx+Bv1W19aHd0+1zXe8EI2oyXBFyQdQQRmIGnQEaa6mxHmCDUlDCFGg31Pn/n9KOq0YVcABU5XQlW7s/wBiXkAecmNDqFHxn1v8P86S7BcNWWYu4usQBA6sSct/SxPqBWjS4pF3YenP5DWlMjILTpaisZtZUZh+zWDiH5pTbcv4v56Ux4m+HjF1wHeDqI0Ufjr+FPmxDSyAAWUbX6/eP9B6+y2OxIjjJbxabC12PJRtrWe6R53tMBgHI+Sq8I4diAR3TwsNDuMp5X3A9SLU0x/ZF4m77DSNf78ZyyW5XtpIvkbjypWfBB2zHDYiB9gyMjH0K3BZeqi4PnUrwueZNAVmU32Uo1xa9wLhTvcWA/G8snfw7dWkiZy1ReB7UYmOwnQTr9+Oyyj9pCcr+qlT0U1ZeD8WhxN+5kDFbZkN1kS/30YBk9wKaY3BxyHOAY2+0rC1/O+1/ejv2eikAzpcr8LglZEvzR1IZD+yRRCGuFhL2QaKsCRWpS1QSQ4yD4HXEx/clskw/ZkUZXsNAGUHq9O8Dx2KRxG2aKX9FKMrnrl+zIB1QsPOh0rWpOhVT439IuAws74eaXLIlswttmAYfgwoVy6lbKqX0mcE/KMGzIPrYPrUtv4fiUdfDqB1Vat1cNcuBpZFwnG99Esg5jXyI0YfOlpIkB7x8osLZm2A9ToPXyFMJ8D+Q8Qlwu0U31sPTW91HpYj9wdad47BlzGRYFWudSDlKspAI1G4Oltt6y5G6H0vUwTe1hDuv7pWbGog1a9raDU62A0Gw1Gu2tMlxzuxCKbDTYZgVLCRSfgVtFIubEG4vtRsZh4VYtIc19Mu48VhlsNwbA2NxpelleV/gUILaMwJ5E3tp5dee1tagilcuN8/RFj4cxYO7a3UjqCM219LFbAgDcXriNGllijLEDKLAbLcWzMeVgPlypeSJE8Ur89M1gBa5AHkOvz0GhYOII17AiwzAkfELlcy28xtvqNNagvKgUCjpmI8YAPQEkfypKCaXCS/lOGGYn89DylUdOkg5H29XkWovYjyO49aOEFDE5Y6wrTMbKzQ7hX3gnF4sVCs0LZkb5g81YcmHMU/rKEmlwMxxWGUuja4jDj/AFB+kj5CUf8AlWlcJ4nFiYUmhcNG4uCPxBHIg6EHY1qwzNlbbV5meB0TqKj+0PZ/8pdHEmQqCvw3uGZSwuGBAIXkb5lRgQVqQwfDY47kLdiSS7WLnNa+tvIaDpTw1U+1XZp5C02HeRZCBnQSOqyW2IAYAOB7Hy3ojjQtUjaHODSa81a70z4hxaCG3fSpGTsGYAnzA3IrJf8A8jOt17/EDcEd9KCOotm8JpiqaliXYndmdmY+bEklj5m5oByRWwW3H2DITbnjT5LbMDj4phmikSQDcowNvI22NOaxTDzNGweNijjZlOvp0I8jcHmKteH7fyRqO+gD9ZEfL/EpBt6g29KlmQ13OyXyux5YjbPeH3V4x+BjmjaKZFkjYWZWFwayztF2Gnwd5MGHxGHFyYCc08Y/5RP51f1T4ul6nV7fSM4ywKEG65yXI/VNgFI8wb9RvVo4ZxqHELeJrkfEp0df2h/XY8iaNHOL9wpCfDmi/qNpYxhMSkq3RrjY20YHmCN1YdDU1hCkUcUx7k2cwyLJbxXuysNyp+AF9R4zpprbe1HYqHFMZ4j3GJ/SoBZ+gmTaQefxDTWqDi2nhJweNXuxIQwA1glYaZopLXzWA8B120NNSSe2bQ2KXYNDrPCu3DeKRSG0Vl0vZoVtbnqo1Hn71ZMNJdbWWx5qBlN+YrPcC7xsrpoym4/t6HatB4a8eIUMPBJ9oCwb+zD1HyoMkGjjcIol1co86mO7jUc70xwaPiZFdhljjbXzZToo9DufK3pMS4EspViGB5EZT7Fdvkai8Kk+FAUsZYhoqsv1ijkAw0ceozUA7HfhSHCj4qwlARYio7EcOVG72Oykat0IF9fa5/H3dYTHJIPCfY/5Y+1ObUTlC3CZ4aaPEISpBGqkjWx2NjbX12IPnReF4NoowjOXIv4iLewGtgOn/qnWHw6RqFjVVUbKoAA9AKUqQoPkuVB9ssXh4cHNNikV441zZWAOZvsqt9mLWAPU1NsayD6R+MnF41cHHrDhTnl6PLbwx+eQakdTblUOcGiyhucGiyqdD2VSYCXEsTM/ie7EkE6hSSSTlFl1J2rlS7ym+/TmaFZpyH33issyvJu/yt9NcoUK01sKkfStwZpcMMTEPrcKe8B/U0zj0Fg37pqB4biRLGki7ML+nUexBHtWqOoIIIuDoQeflWRcNwn5HjZ8A18n56A9Ubdfbb1RzSWay2ah0Wj2dkaHFh4P5UlDgkX4VA35dTek+JMyKrJmNnXMqrcsCQraWvoDm0t8NSyJ6Vxo6xzNvZ3WqX7UFV14LIXY5go8S3PiLAjLdhoLkEm973y8hq/i4NEqlQtwRaxJItfMbDYXOptzp/Lio00JBP3V1b0sNjrSKmZ28KhFvu1yWUEbDQgkXOo0FuZIWTM93WkMOY07boSZVF2IA1Nz5an8NfakUxZZgqIzWZkY2sFKkqd99R/bY2kIcD4UEh7wqQczAXuNjYC3+CnIiA/2oBlaPNSZiU27umEUsnD5GxECl4GN8RAu/nPEOTAbjmPwmglHIqIcp0T9QQJqkbRVpwGNjmjWWJw8bjMrDYg/18uVL3rMY8W/C5DNGpbBSNeeJRcwk7zxD7v3lHr6aNhcUkiLJGwZHAZWU3BB2Ir0sE7Jma2rHfGWGioPtT2WXEjPHZZhz5Pbk/8ARt/Ws2xMDRsUkUqynVTuP9vMaGtszVC9o+AR4pPF4ZF+BxuPI/eU9PlY1SWG92rV7O7UdjkMk3Z+PXgspVSdqVUNbUXFPcFwaQ4hsLKUhmGqBma0q/eiIWzeY3HTe1mwnYoj85NpbZF8Xs7bD0W/QilvYvd0W1N2tjAbG/kVSkwQ3Ru7A11+Eev3fbTyomC4mM2hysp0dCRy3VtDYj2I5b1Oca+j2cEdxIJVv/qm0i+YJurNtr4fSnXC/o6a3182Xnli1Pnd3Gp/d96kY7yhHtDFc23useFEn19k44d2wZLCYiRebAWkA+8VGkltyFCm2wJ3teLwUGLhySok0MgBsbMpB1DA/iGHtUXhOxODTeNnPV5HP4AhR8qnuGcNjhXJEgVbk2BNrnc6nmdacia9opxteeynY7nXCCB5/wCys/x3ZrEYA54g+LwnNN8TCP1f06Dp8Q86sHZ3ERTxrNh3DqdmU6jqCN1YcwdRereBVa4z2TBlOKwT/k2KNszAXimtss8ez8/GLML7002Q1RSJCnoJWtrrS2YEaiq1wftMDIMNi4/ybFH4UY3jmt9qB9pB+row5irKKqaXKCxXCGfEobAQrZyQxDFlN1UjmAQDf1HSrBei0L1UClYuJRqKTXaZ8V4jHh4XnmYJHGpZieg6dSdgOZNSqqv/AEhdpzgsOBFZsVMe6w8fMubAuR91bgk7XIHOsu4bhO4Sxa75szt8TO7fE19t9bE3286PJiJMfiGxkt1Zxlw8RItFEDsb/bfU6dbeQUhiGU3La/D0JuBa2+1/cDrSGTLqOkcBZ+RJqOkcLjTDqo8vGP5XoU1aQjQEgdLmhS30Su69Au4AuSAOp2pi/GIs4jBZmLFfAjsoYAkozgZEbQ6MRVebhE8sjMM3dnEo5ScsbLGUYSQg3KG4ZctlFm5W1luGcIliRIjiPq4wQuWMB20IBkYlgx1voFuQL6XB11uJTgPGxilEkaWjKqysXQnXXKyqTlYCxtfnbfSqz9K3DG7qPHQi8uEbMR96M/GD5De/IF6tfCuDRYcARhvCixgs7MQqaKoubADytT2eEOrKwBVgVIOxBFiD7GocARRUg0bCpGCxKyxpImquAw9Dr8/7UMZgu9FizKNQQpte4I3303qD7LxnC4jEcOkJ+qPeQk/aifUfIkX8yelWm1eSyGmGUt8FrNkD2gpthcEifCNepuTz5nzY+5J501468wjvArFr38OW9lBbLYnXMVC6a2Y21qTvXDQRIQ6zuoPFKBwOAmXMwyKxRxrc5mZs0bOLAgqCVPPXyFTEcYXRRYDYUrei2qr5S47rhsug0Grh30ojCh6l1ojW2t/nSoPAYxuFSE6nh8hJZdScM5+0g5xMdxyPyadC114QwIYAqQQQdQQdCCOYo+Nlux36hx1HihytDwrXDMrKGUhlIBBBuCDqCDzBBo7Gsz4TxFuESCOUluHSN4HNycIzfYc/oSdjyv8APTEIIuDcHYjY35jrXrYpWSsD2GwUg4UaKiuO8EixSZJQQQcyOps8bDZ0bcGonhvHJcNKuF4iRmY5YMSBaOboj8o5uVtmO2pANuC014pwyLERNDMgeNhYqf5jofOrEEbhRq6FLMtcC1G9nuGTYdDFLOZlB+qZh9YE5K7X8ZHXp+EvarLkmEpZRXFFBjYE2v5dalVJQllVRmYgAcybCksLjVclQGBABsyMps17GzAcwflWddq+0c2YIpscqsXG4zqDaP7gsbX+I9bU67EcaRVRWYZk7xSpIDMJGV1ZSTZrEMCu/iv5UJszXO0haLuy5m4wyPGthvsequvGOEQ4qMxYiMOh1sdweTKRqrDkQQarffYvh2kveYzBi1pQC2KgG31qj8+g++viGtwd6tEGPjYgBtTsGBUn0BAv7U6oyzU04fj4p41lhkWSNtVZSCD/AL+XKnNqrXEezDxyNiOHOsEzHM8RH/DTn/moPgf/AJi69c21L8F7TpK/cTocPigLmGQjxAbvC/wzJ5jUcwK5Qp6RwoJJAAFyToABuSeQrEu13aP/APKTZUJ/IIW8Nv8A+iQX+sI5xryHPfyEl207SNxKR8HhXIwiG08q3vOy6mGM/owNWPO3TeMaAJEgAKpchBa2g3Retj/OlcibSNLeUtkS0NLUg191sNdwTcXHw+lj+GprrxsFD5TYkWNiVuLg76HX1o4uRca2ubXGbmL76m3+WsaZXtvfpbrz6bUjSzyaS4QnWzeyAj2Njf51yiHDMenT4lG2m19NqFcutb9QoVwmthbSFCgTXK5cqD9KOBaPueJRC74ZrSgbtE5sR7E+2YnlT6DEK6K6G6sAwI5gi4NWnG4dJI3jkAZHUqwOxDCxHyNZd2NZoHxHDpSS+GcmMn7cTm6n8QT0zgcqxu18fUwSjpz8E1jvr3VaXNcvQri67EG2hsdvWvOpslGrqpfamGP4vh4Pzs8aHoWGb2G9NoPpD4bF/qPIx+7GbegLW+dNYuHJkOrgeKG59DZWnD8K5ubeQ/qafw4FBso99f51Sn+lGHuzImGmIvazFFJ8wLnSoWX6RcXiQRAi4dTpm+Jz6Eiw/hr0UWJBCNgPjyUMRTyGq5WicU4phsMAZ5ES/wAK2ux/ZRQWb2FQs/bLh+xWX17lx+BAb8KoeG4Ue9aV3d2tdmdsznrry2tU3g5opFaKExFwfEjjxcvEP7ioke07aQQmhhBoBeTfkrRg3wmOicQOsq6pJG4II5FWVgCL8iRryNQPBuIvwiVcLiWLYB2ywTNvh2O0Mp/R/dbl6Xy0jGYyXhvEUnW4uozjYODoQRtsBWz4zDQ47DXyh45UvY8wdbHoR8wRVGwiEe1gG3Vvj8PA/nhJzx6TRNjxU1XDWddnuMPw2VMFi3LYVzlwuJb7PTDzHkRsrbW+Q0W9PxSslYHsNgpUggrlJzYhEIDuqljZQzAXPQX3Oo+dK1QO1HH2jmuAGDl1ZTsY0vEUvut5O8a410XpXPeGCyj4+O/IkEbOf4V5xGJVBdj6Abm29h/XYc6pHaHtqBdIrN7+H95h8f7K2X9ZhpVcxPEMRjJO7jB8X2QxOg1u7sblR56DpVn7OdkFWztlkbfOR9Uv/TX/AFT+sfDzF6F7R8mzNh4rVGDBhjXlm3dGD9/X1VXj4RisUe9YfH8LPcZrfcVQWIAt8K2tS+H7HylwJGRRfULdpD5Klgb+bWA3J0rVcNhFS5Fyx3Y6sfU9PIaDkKcVIxm9VDu3sjiMBo6bcevh8lBcD4CsKKgGVFYOEvclhs0jbZtvCtlH61TldpHFYhI0aSRlRFBZmYgKANySdhR6WK97nuLnGyUqTWP9uOOLxV/yTDFBh4SWkxTC+Zl+xhyBfrd13G1xbMr2g7Qy8WJhw7PDw++V5bWfFE6d0l/hjNtb7jfS4okWHXD5YTGoRQPBdwF+11vqQfwO9AmnDNhyl5Za2Cb8Mwth3YW8YUKO7IRAFOhPhsTruRck8yaLLiW5FlvcnML2Nzdibb6dNwdKdTTAaKCpFmW6rcaA2IA8Q9dwOo1bYqVgpRnI1GgykE7XuD66+fnqhqvqkCU1yADxWsbNcFvACQRY6g+h6e9EEQRmWwOh5kBb7Mbi9xr86fLAdQMwANyrNcG9xmAuNfDrY/K1qNHhGUrYaaA6AklrDY76agG+x6G00qkFQc6gsSbe17aac65Vhi4oiDKEbTpLlGuvwgEA6/8ArahU+74/b/KHp81sBNFJrjGi3rUK3AEa9cJrlGC1ylEIrO/pPwbYeXD8UiBPckRYgD7UTnQ+eUkj1ZelaRam/EsAk8UkMgukilGHkwt86q5gc0tPBXXSrsJDAMpBUgEHkQdQfS1ZZ2uQqzjvHBLkyBXOg1yqpvdsqgAgm11PIird2Jd0WXh8wzSYSQJa9s8R1Rl9NNNrZRSnZ/sMqySTYr6xmYlUOqqtzbMftmx228tTXnYRHhl5kO/Ffj+U4x97rPOGdjjIgfJOyk3HdwltOvO/XanjcAw8Khm7zOD8MiZCfKzC4O/lW44FQrKALDYAbDoBUhiMOjizqrDowBH41p4U7sqMuut6pT+pax3cCyReFiVVmGQKR4WJ118gN6iZeLQxFopGAI5GwB0HO2nPetGxHYxEMjRE5G3itqPNGvcEchz28xR+z8GFw/FDBjY4ZRIq9zI6g921zZGFsozXtci9wBzojYfe0uTn64aS5u5TXhvEpsURHgMKZL6F9o15XZrZdL7ak8qssv0TI8A7zEP+VXzmVAAgbkoFswUdb35+VaTFGFFlAAGwAsBR6abE1vCzpst8nkFhfaHhsiFIOIlnyjLFiBmzqSdBKAbSoeTjxaNubirF9FXHGjJwUx0B+qN7jXUAHYqRqD10tVu7ecME2EfQZlsVPTUX9ufqoPKqUi4eBsFibBBNbOjlrAl1DZeejsCCNBbzoItjq9borXMkjIPo8q3cf4bFOskEqho20Kn5gjoRyNQXZjj0uAlTAY5y0T+HCYpvtdIJjykGwPOrNiWu7HzqP4zwmLExNDOoZGHuDyZTyYda8vjdonFyHVuwk7fPket0FzNQ81cKp3F+yHfNds2mYAoV1DMz+JWAsQWIuG1tfS9hH9j+Py4WVeHY98xOmFxJ/wBVRtG55SDQa76eRbQb160GOZgcNwdwqRTSQP1MNFUfhvY1oXzKZCCCrK3dZWVviVrMTY+Qq3cMikVSJSD4jlAJbKullLEAubgm5AOoGtrl3Xau1gaKCmfJkndqkNn5fshQoVSu1vb+OBzhsIv5Ti/0anwRfrTMPhA+7v6VYmkBT/aTtFh8DCZsTIEXZRu7n7qLux/lzsKzDi8+K4mWfGI0OES5TCg2JI2bEn7TAkHuxax9DdPDYB3xAxONkbFYgryXwQ5r+CNToLbE/jvTnEOSxJIs2gJ1tltpubW232pGbK6M+qVkn291dSQ5YkUKuXVPEBqBrcixVL3tY6206118KpchDn3uSbjw7C5AOtuVugvaj4iFwyEoGY30uL30+ICxG+xsNulIRIvxWNxbSwFzvpoQRY2/G3Kk+eUqXeKPmZjo6hm1sAQbra4OhPiGvO51O16Nh4hmdLWRtLkqWGXS2oJW599Rvax5Kotma1xyt8SsT4s9wSw2tamckutypB1vlHnewHkP51I2VbpKiItbM1lYkKWByFQDYX1ykHNbzO/UgGiNe2UWuufQm5tcNqfQgan1rkczKrJYZW1u1rC4ygqSPDbMLsDrceVF77IpCZiF3uRZToCcovfXZtLXq42VSnRZN2lsbC4MrXBtsfCdtq5TaN1AAaONjbcmS56H4ulCqHTfeXavILZK5M6opdyFVQSSTYADUknkKWVKSx0GeN0vbMrLfpcEXrXdYFhbVqA4h22wkIvI5AsTcgDbyYhr9Ba55XqxwuGUMNQQCD5HUV5t+kfBhZkkuB3i7ea6e+hHyrWOyXa8nA4YtE7N3VrgaHIcvXe1j8+hsGKYGMSOPKvo1d0K+2oVQYPpIWWVoYo8zLa5U5gL38htap+DijlWMhVTuNbBV5FyTYH+x32Bw4FQ6JzW6j+Qq99IEBwuJg4ogOVSIcSBzjc2Vj+yfxy9KtSEEAggg6gjYg86d47hyTQPBL4ldCjedxYkXub86pX0f4x1jlwM5+uwb90T96PeJx5ZdB5ZetYnbGNYEo6bFTG7orYKf4ebMPPnUazURZCDcVl4WacZ98g8q7malNVn/wBIvAwCuJVFZcwEqlb3VgUawHxXB208QHM6XOLHj7Wnnypp2lwbYnDSQxSKjOAAx5agn8BXpG5MM7fccD+foqMLo3WmXYvizyRmGb87CFGYG6yRsPq5Qd9VGt+YPWrJeqz2X7IxYNjLnaSdkRHkY2BCBQMqjRR4RVgfEKN2FMGRrG28gKr6LvdTXjvDBiYHgZ3RXABKGxsCCR6G1j5E1GzcKw8ccMXdq5hULG0gDOgFhe5HxaDXyFSM+P08PzNR5rB7R7VZpLIdyev8fyiMYeqIooxrpFACvMIyjePcFixcLQyg2OoZdGRhs6nkR/tReyHaGWOQYDHNeYD6mY/DOo0Gv6QC1xz9dTK2qM7R8OhmgbvmEYXxLLfKYiNmDXFuXPWtTs3tF2M/Q7dh+3mEN7NSulR/G+NwYSMyYiRUXlf4m8lXdj6Vm3CfpAxssJhhjSaVHKHFa9yQL2a1hd7DUe9taaYHh5/KDiMV3mJn+IM9/DluRkTUItxr01ta9eufkMZyUm6Rrdk84z2jx+PFoA+Cwjf6ht+UyLzI/Qr577WNjSfCcLDhY2hiUANqwa4Yk82OY5jYA/veQqQfGs6qgN72DMM3w75SedgCTem5bMxYHMFJIsbmxIuCLX1zAX57cqRlmc887JV8mookv3NSSfhJYhjoQCA1+u3NrcqSUtlAbdNLE20Umysb+IDXYaAnkdCYVjmbws2jZQCNDa+v6oF7/wDuhisOyCxNiCylSVJBsua3IDXe/oBQh4oPmkIpSC1m+EfCfCGAv4bA6nW+/XfmZcS2UA5bfELgX0I5/a9L9a4kGQa31B2ytc6Wvrpo2+hH8kcQBuBpY5SL6kbnWx5HWo4QyF1yfh0H6p67a3NlO9N2ICltmGUgeE9QTfTJyNrH+tIO2nK511I5Anfr5c6TnUZkBYagEsuoUHqLXuPerNCikrPOXGuUXHxWttchfW5te+uldwMAJ8bWTLmJW+aw+yL6db/snpTNivLU9Btpcnf219a5IDya+mhPS3O40+VXHiq1vZT3Fxh3LmVBfYMXuByHwbWtbyttQqPEp+/bl8tOlCpvyXbL0QKLKLixoUK1Dwtpec/pOY5sOLm3dk+9wL1ovZSJRw7h5AAJha5AGviXfrQoVlP/ALD6flG6lUj6OTbiWJtp4JdtNpBarYJmkxHCo5GLxvJiC6OSysYyShZToxWwtfa2lChWo3gfJDC1QbVm/EdO0S20zYK7W+1Z2tfrsPkKFClc/wDtn/BSzvBWtqJQoV4o8JlCitQoVy5EeglChUHlclBtRaFCqO4UhGoChQoBXLrVkn0mSFuJYeJiWj+rOQm6XLAE5Tpcg2rlCtfsb+7+R/CFN/TKt3aKFY2SONVRBGtlUAKL5r2A0FIwjRDz8RvzuMljQoVrn+o711WO/vJbi0KgJZQLmO9gNbqSb+9M+8Ocrc5Q1wL6C+QaDlpQoUYcu9dQqy8pnFIVCFSQQ5sQbEXy319zSFvD7n/6f3oUKoe6PXgh9VyCQjYnSxGuxuNRTbEHw39f6V2hVR3fXkuTOQ6kcrCiD7f7I/mtChRh0XJHEMbnU9KVwx8JHK66exoUKnqVA5SGIHiNChQrlUr/2Q==);">

      </div> -->
    </div>

            {{-- <div style="width:1px" class=" img- hidden-sm-down with-content" style="background-image: url({{asset('img/portfolio/5.jpg')}});">
                <a href="#section-membership" class="content invisible d-flex justify-content-center align-items-center">
                <div>
                    <h4 class="text-uppercase mb-4 font-weight-bold"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">You can bet from anywhere in the world!</font></font></h4>
                    <p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Join us and start winning odds betting! </font><font style="vertical-align: inherit;">One of the undeniable advantages of this business is that you can run it from almost every corner of the world!</font></font></p>
                    <span class="btn btn-primary btn-sm btn-light"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Start winning</font></font></span>
                </div>
                </a>
            </div> --}}
            </div>
  </div>





    <!--==========================
      Why Us Section
    ============================-->
    <section id="why-us" class="wow fadeIn">
      <div class="container">
        <header class="section-header">
          <h3>PROČ ZVOLIT NÁS?
            </h3>
          <p>{{$content['why-use-us-subtitle']}}</p>
        </header>

        <div class="row row-eq-height justify-content-center">

          <div class="col-lg-4 mb-4">
            <div class="card wow bounceInUp">
                <span>{{$content['why-use-us-title-1']}}</span>
              <div class="card-body">
                {{-- <h5 class="card-title">Corporis dolorem</h5> --}}
                <p class="card-text">{{@$content['why-use-us-subtitle-reason1']}}</p>
                {{-- <a href="#" class="readmore">Read more </a> --}}
              </div>
            </div>
          </div>

          <div class="col-lg-4 mb-4">
            <div class="card wow bounceInUp">
                    <span>{{@$content['why-use-us-title-2']}}</span>
              <div class="card-body">
                {{-- <h5 class="card-title">Voluptates dolores</h5> --}}
                <p class="card-text">{{$content['why-use-us-subtitle-reason2']}}</p>
                {{-- <a href="#" class="readmore">Read more </a> --}}
              </div>
            </div>
          </div>

          <div class="col-lg-4 mb-4">
            <div class="card wow bounceInUp">
                    <span>{{@$content['why-use-us-title-3']}}</span>
              <div class="card-body">
                {{-- <h5 class="card-title">Eum ut aspernatur</h5> --}}
                <p class="card-text">{{$content['why-use-us-subtitle-reason3']}}</p>
                {{-- <a href="#" class="readmore">Read more </a> --}}
              </div>
            </div>
          </div>

        </div>

        {{-- <div class="row counters">

          <div class="col-lg-3 col-6 text-center">
            <span data-toggle="counter-up">274</span>
            <p>Clients</p>
          </div>

          <div class="col-lg-3 col-6 text-center">
            <span data-toggle="counter-up">421</span>
            <p>Projects</p>
          </div>

          <div class="col-lg-3 col-6 text-center">
            <span data-toggle="counter-up">1,364</span>
            <p>Hours Of Support</p>
          </div>

          <div class="col-lg-3 col-6 text-center">
            <span data-toggle="counter-up">18</span>
            <p>Hard Workers</p>
          </div>

        </div> --}}

      </div>
    </section>



      </section>
    <!--==========================
      Portfolio Section
    ============================-->
    <!-- <section id="portfolio" class="clearfix">
      <div class="container">

        <header class="section-header">
          <h3 class="section-title">Our Portfolio</h3>
        </header>

        <div class="row">
          <div class="col-lg-12">
            <ul id="portfolio-flters">
              <li data-filter="*" class="filter-active">All</li>
              <li data-filter=".filter-app">App</li>
              <li data-filter=".filter-card">Card</li>
              <li data-filter=".filter-web">Web</li>
            </ul>
          </div>
        </div>

        <div class="row portfolio-container">

          <div class="col-lg-4 col-md-6 portfolio-item filter-app">
            <div class="portfolio-wrap">
              <img src="img/portfolio/app1.jpg" class="img-fluid" alt="">
              <div class="portfolio-info">
                <h4><a href="#">App 1</a></h4>
                <p>App</p>
                <div>
                  <a href="img/portfolio/app1.jpg" data-lightbox="portfolio" data-title="App 1" class="link-preview" title="Preview"><i class="ion ion-eye"></i></a>
                  <a href="#" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-web" data-wow-delay="0.1s">
            <div class="portfolio-wrap">
              <img src="img/portfolio/web3.jpg" class="img-fluid" alt="">
              <div class="portfolio-info">
                <h4><a href="#">Web 3</a></h4>
                <p>Web</p>
                <div>
                  <a href="img/portfolio/web3.jpg" class="link-preview" data-lightbox="portfolio" data-title="Web 3" title="Preview"><i class="ion ion-eye"></i></a>
                  <a href="#" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-app" data-wow-delay="0.2s">
            <div class="portfolio-wrap">
              <img src="img/portfolio/app2.jpg" class="img-fluid" alt="">
              <div class="portfolio-info">
                <h4><a href="#">App 2</a></h4>
                <p>App</p>
                <div>
                  <a href="img/portfolio/app2.jpg" class="link-preview" data-lightbox="portfolio" data-title="App 2" title="Preview"><i class="ion ion-eye"></i></a>
                  <a href="#" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-card">
            <div class="portfolio-wrap">
              <img src="img/portfolio/card2.jpg" class="img-fluid" alt="">
              <div class="portfolio-info">
                <h4><a href="#">Card 2</a></h4>
                <p>Card</p>
                <div>
                  <a href="img/portfolio/card2.jpg" class="link-preview" data-lightbox="portfolio" data-title="Card 2" title="Preview"><i class="ion ion-eye"></i></a>
                  <a href="#" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-web" data-wow-delay="0.1s">
            <div class="portfolio-wrap">
              <img src="img/portfolio/web2.jpg" class="img-fluid" alt="">
              <div class="portfolio-info">
                <h4><a href="#">Web 2</a></h4>
                <p>Web</p>
                <div>
                  <a href="img/portfolio/web2.jpg" class="link-preview" data-lightbox="portfolio" data-title="Web 2" title="Preview"><i class="ion ion-eye"></i></a>
                  <a href="#" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-app" data-wow-delay="0.2s">
            <div class="portfolio-wrap">
              <img src="img/portfolio/app3.jpg" class="img-fluid" alt="">
              <div class="portfolio-info">
                <h4><a href="#">App 3</a></h4>
                <p>App</p>
                <div>
                  <a href="img/portfolio/app3.jpg" class="link-preview" data-lightbox="portfolio" data-title="App 3" title="Preview"><i class="ion ion-eye"></i></a>
                  <a href="#" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-card">
            <div class="portfolio-wrap">
              <img src="img/portfolio/card1.jpg" class="img-fluid" alt="">
              <div class="portfolio-info">
                <h4><a href="#">Card 1</a></h4>
                <p>Card</p>
                <div>
                  <a href="img/portfolio/card1.jpg" class="link-preview" data-lightbox="portfolio" data-title="Card 1" title="Preview"><i class="ion ion-eye"></i></a>
                  <a href="#" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-card" data-wow-delay="0.1s">
            <div class="portfolio-wrap">
              <img src="img/portfolio/card3.jpg" class="img-fluid" alt="">
              <div class="portfolio-info">
                <h4><a href="#">Card 3</a></h4>
                <p>Card</p>
                <div>
                  <a href="img/portfolio/card3.jpg" class="link-preview" data-lightbox="portfolio" data-title="Card 3" title="Preview"><i class="ion ion-eye"></i></a>
                  <a href="#" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-web" data-wow-delay="0.2s">
            <div class="portfolio-wrap">
              <img src="img/portfolio/web1.jpg" class="img-fluid" alt="">
              <div class="portfolio-info">
                <h4><a href="#">Web 1</a></h4>
                <p>Web</p>
                <div>
                  <a href="img/portfolio/web1.jpg" class="link-preview" data-lightbox="portfolio" data-title="Web 1" title="Preview"><i class="ion ion-eye"></i></a>
                  <a href="#" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                </div>
              </div>
            </div>
          </div>

        </div>

      </div>
    </section>#portfolio -->

    <!--==========================
      Clients Section
    ============================-->
    <section id="testimonials" class="section-bg">
      <div class="container">

        <header class="section-header">
          <h3>Reference</h3>
        </header>

        <div class="row justify-content-center">
          <div class="col-lg-8">
            @isset($reviews)
                @if(count(@$reviews))
                    @foreach ($reviews as $review)
                            @if(!empty($review->fullname))
                            <div class="owl-carousel testimonials-carousel wow fadeInUp">

                                    <div class="testimonial-item">
                                      <img src="{{getImageLink($review->path)}}" class="testimonial-img" alt="">
                                    <h3>{{$review->fullname}}</h3>
                                      <h4>{{$review->title}}</h4>
                                      <p>
                                        {{$review->description}}
                                      </p>
                                    </div>
                            @endif
                    @endforeach

              @endif
              @endisset



            </div>

          </div>
        </div>


      </div>
    </section><!-- #testimonials -->

    <div class="container pt-3 pb-5">
            <div class="row justify-content-center">
                <div class="col-sm-12 text-center">
                    <h3>Kalkulačka možných výher</h3>
                    <p class="lead">Spočítejte si, kolik s námi můžete vyhrát!</p>
                </div>
            </div>
            <div class="row pt-3 pb-3">
                <div class="col">
                    <div class="calculator">
        <div class="row no-gutters">
            <div class="col">
                <div class="col-calc">
                    <form>

                        <div class="form-group row hidden-error">
                            <label for="calc-q1" class="col-lg-8 col-form-label">Kolik můžete vsadit na tiket?</label>
                            <div class="col-md-8 col-lg-4">
                                <div class="input-with-currency">
                                    <input type="text" class="form-control" value="100" name="second-calc" id="calc-q1" placeholder="">
                                    <span class="currency">Kč</span>
                                </div>
                            </div>
                            <span class="msg">Výše sázky na tiket nemůže být větší než kapitál</span>
                        </div>
                        <div class="form-group row">
                            <label for="calc-q5" class="col-lg-8 col-form-label">Jak velkou míru rizika jste schopni podstoupit?</label>
                            <div class="col-md-8 col-lg-4 mb-3">

                                <div class="custom-control custom-checkbox mt-2">
                                    <input type="radio" id="calc-q5-a2" value="2.35" name="calc-q5" class="custom-control-input">
                                    <label class="custom-control-label" for="calc-q5-a2">Nízkou</label>
                                </div>
                                <div class="custom-control custom-checkbox mt-2">
                                    <input type="radio" id="calc-q5-a3" value="2.85" name="calc-q5" class="custom-control-input" checked="">
                                    <label class="custom-control-label" for="calc-q5-a3">Střední</label>
                                </div>

                                <div class="custom-control custom-checkbox mt-2">
                                    <input type="radio" id="calc-q5-a5" value="3.45" name="calc-q5" class="custom-control-input">
                                    <label class="custom-control-label" for="calc-q5-a5">Vysokou</label>
                                </div>
                            </div>
                        </div>


                    </form>
                    <hr>
                    <div class="row">
                        <div class="col">Na základě Vámi zadaných dat kalkulačka vypočítá výhry, kterých byste měli s pomocí našich tipů dosáhnout</div>
                    </div>
                    <div class="row row-results">
                        <div class="col-sm">
                            <div class="time">Za den</div>
                            <div id="result-day" class="result"><span>235</span> Kč</div>
                        </div>
                        <div class="col-sm">
                            <div class="time">Za týden</div>
                            <div id="result-week" class="result"><span>1645</span> Kč</div>
                        </div>
                        <div class="col-sm">
                            <div class="time">Za měsíc</div>
                            <div id="result-month" class="result"><span>7050</span> Kč</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="col-help pt-0 pt-sm-4">
                    <h3> Jak funguje naše kalkulačka výher?</h3>
    <p>Na základě Vámi zadaných dat kalkulačka vypočítá výhry, kterých byste měli s pomocí našich tipů dosáhnout</p>
    <p>Kalkulačka možných výher počítá s naší ilustrační historií tipů a celkovou úspěšností, kterou se snažíme držet co nejvýše.</p>
                    <p>Důležitá je také Vaše zvolená míra rizika, které jste ochotní při sázení podstoupit. Ovšem ani u nejrizikovějších možností byste se však neměli obávat výraznějších ztrát, kalkulačka naopak počítá s jistou rezervou a proto reálné výhry tak mohou být ještě mnohem vyšší!</p>
</div>
            </div>
        </div>

    </div>            </div>
            </div>

            <div class="row pt-3 pt-sm-5 pb-3">
                <div class="col text-center">
                    <a href="{{route('register')}}" class="btn btn-lg btn-primary px-5">Začít vyhrávat</a>

                </div>
            </div>
        </div>


        @if(isset($content['show-package']))
    <section id="services" class="section-bg">
      <div class="container">

        <header class="section-header">
          <h3>Druhy členství</h3>
          <p>{{$content['membership-plan-subtitle']}}</p>
        </header>

        <div class="row">
                @if(isset($levels))
                    @if(count($levels))

                    <div id="generic_price_table" class="w-100">
                            <section>
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <!--PRICE HEADING START-->
                                                {{-- <div class="price-heading clearfix">
                                                    <h1>Bootstrap Pricing Table</h1>
                                                </div> --}}
                                                <!--//PRICE HEADING END-->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="container">
                                            <div class="row">
                            @foreach ($levels as $level)

                                    <div class="col-md-4">

                                        <!--PRICE CONTENT START-->
                                        <div class="generic_content clearfix">

                                            <!--HEAD PRICE DETAIL START-->
                                            <div class="generic_head_price clearfix">

                                                <!--HEAD CONTENT START-->
                                                <div class="generic_head_content clearfix">

                                                    <!--HEAD START-->
                                                    <div class="head_bg"></div>
                                                    <div class="head">
                                                        <span style="color:white !important">{{$level->name}}</span>
                                                    </div>
                                                    <!--//HEAD END-->

                                                </div>
                                                <!--//HEAD CONTENT END-->

                                                <!--PRICE START-->
                                                <div class="generic_price_tag clearfix">
                                                    <span class="price">

                                                        <span class="currency">{{currency_format($level->price)}}</span>
                                                        {{-- <span class="cent">/{{$level->period}}</span>
                                                        <span class="month">dní</span> --}}
                                                    </span>
                                                </div>
                                                <!--//PRICE END-->

                                            </div>
                                            <!--//HEAD PRICE DETAIL END-->

                                            <!--FEATURE LIST START-->
                                            <div class="generic_feature_list">
                                                <ul class="na">
                                                    <li style="color:black"><span></span> {!! $level->description !!}</li>

                                                </ul>
                                            </div>
                                            <!--//FEATURE LIST END-->

                                            <!--BUTTON START-->
                                            <div class="generic_price_btn clearfix">
                                                <a class="" href="{{route('register')}}">Získej tipy</a>
                                            </div>
                                            <!--//BUTTON END-->

                                        </div>
                                        <!--//PRICE CONTENT END-->

                                    </div>



                                {{-- <div class="card mb-4 box-shadow">
                                <div class="card-header">
                                    <h4 class="my-0 font-weight-normal">{{$level->name}}</h4>
                                </div>
                                <div class="card-body">
                                    <h1 class="card-title pricing-card-title">{{currency_format($level->price)}} <small class="text-muted">/ {{$level->period}} dní</small></h1>
                                    <ul class="list-unstyled mt-3 mb-4">
                                    <li>{{$level->description}}</li>

                                    </ul>
                                    <div class='text-center'><a class='btn btn-sm btn-outline-primary' href='{{route('register')}}'>Získej tipy</a></div>
                                </div>
                                </div> --}}



            <!--BLOCK ROW START-->

            <!--//BLOCK ROW END-->




                            @endforeach
                        </div>
                        </div>
                    </section>

                </div>

                          </div>



                    @endif


                @endif



            </div>



      </div>
    </section><!-- #services -->
    @endif




    <section id="could-win" class="gray pt-4 pb-5 line-bottom okbg">
        <div class="container">
            <div class="row p-3 p-lg-0 justify-content-center">
                <div class="col-lg">
                    <h3>Kolik jste mohli u nás dle historie vybraných tipů vyhrát za posledních 30 dní?</h3>
                    <p class="lead">
                            Spočítejte si, kolik jste mohli dle naší ilustrační historie vybraných tipů výše vyhrát za posledních 30 dní, když byste vsadili každou v ní zobrazenou sportovní událost:
                        </p>
                </div>
                <div class="col-lg col-md-8">

                    <div class="monthly">
                        <div class="row">
                            <div class="col">
                                <div class="form-group row">
                                    <label class="col-form-label">Vaše sázka</label>
                                    <div class="col-sm-6">
                                        <input name="bet" data-course="326.3" type="text" class="form-control" placeholder="100">
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group row justify-content-end">
                                    <label class="col-form-label pr-3">Měna</label>
                                    <div class="btn-switcher" role="group">
                                        <input type="radio" value="Kč" name="currency" id="option-czk" checked autocomplete="off">
                                        <label class="btn" for="option-czk">
                                            Kč
                                        </label>
                                        <input type="radio" value="EUR" name="currency" id="option-eur" autocomplete="off">
                                        <label class="btn" for="option-eur">
                                            EUR
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row border-top align-items-center pt-3">
                            <div class="col-sm-7 pl-0 pr-1">
                                    Při vaší sázce
                                <span class="bet">
                                    <span class="amount">100</span><span class="currency"> Kč</span>
                                </span>
                                na tiket by byl Váš zisk za posledních 30 kalendářních dní roven dle naší ilustrační historie tipů výše částce:
                            </div>
                            <div class="col-sm text-right pl-1 pr-0">
                                <span class="won">
                                    <span class="amount">+ 326</span>
                                    <span class="currency">Kč</span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- <div class="row pt-lg-5 pb-4">
                <div class="col text-center">
                    <a href="https://oksazky.cz/en#section-membership" class="btn btn-lg btn-primary mr-sm-3 px-5">Začít vyhrávat</a>
                    <a href="https://oksazky.cz/en/reference" class="btn btn-lg btn-primary btn-outline-primary px-5">Naše reference</a>
                </div>
            </div> --}}
        </div>
    </section>




    @if(isset($tips))

                <section class="dark py-5" id="tip-history" data-section="history" data-menu="section-history">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-sm-6 text-center">
                                <h3>Historie vybraných tipů</h3>
                                <div class="lead"><p>
                                Díky pečlivým analýzám, bohatým zkušenostem a komplexnímu týmu se můžeme pyšnit velmi zajímavou a dlouhodobou úspěšností! Historie tipů obsahuje některé z našich minulých tipů, podívejte se a přesvěčte se sami o naší úspěšnosti! Mezi naše klienty se můžete přidat <a style="color:red" href="{{route('register')}}">ZDE s 30 denní garancí ZDARMA!</a>
                                </p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="table-responsive">
                                    <table class="table table-dark mt-4 tabler">
                                        <thead>
                                            <tr>
                                                <th class="py-2">Název doporučeného tipu</th>
                                                <th>Kurz</th>
                                                <th class="py-2">Datum a čas</th>
                                                <th>Náš tip</th>
                                                {{-- <th>Analýza</th> --}}
                                                <th>Výsledek</th>
                                                <th class="py-2">Fotografie tiketu</th>
                                                {{-- <th class=" nowrap  py-2 text-center">
                                                    Při sázce
                                                                                        <span class="bet ">
                                                        <form action="https://oksazky.cz/en/partial/history" style="display:inline;">
                                                            <input name="amount" type="text" value="1 000" class="bet">
                                                            <input type="hidden" name="page" value="1">
                                                        </form>
                                                    Kč</span>
                                                    <i class="icon-info-circle cursor-pointer" data-toggle="tooltip" title="" data-original-title="Zadejte vaši obvyklou sázku"></i>
                                                    <br> byste vyhráli:
                                                </th> --}}
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if(count($tips))
                                                @foreach($tips as $tip)
                                                    @if($tip->show == 1)
                                                    <tr class="history-tip ok">
                                                        <td data-title="Název doporučeného tipu" class="text-center">{{$tip->name}}</td>
                                                            <td data-title="Kurz" class="text-center">{{$tip->course}}</td>
                                                            <td data-title="Datum a čas" class="nowrap date text-center">{{getDateFormat($tip->date, 'd-M-Y')}}  <span style='font-size:12px'>{{getDateFormat($tip->time, 'h:i A')}}</span>
                                                            </td>
                                                            <td data-title="Náš tip" class="text-center">{{$tip->tip}}</td>
                                                            {{-- <td data-title="Analýza" class="show" data-show-row="#analysis-0">
                                                                <span>Zobrazit</span>
                                                            </td> --}}
                                                            <td data-title="Výsledek" class="result @if($tip->result == 1) bg-success @else bg-danger @endif text-center">@if($tip->result == 1) OK <i class="fa fa-check text-white"></i> @else KO <i class="fa fa-times text-white"></i> @endif</td>
                                                            <td data-title="Fotografie tiketu" class="show text-center" data-show-row="#ticket-0">
                                                                <span>
                                                                        <a  class="image-popup-no-margins btn btn-xs text-white"  href="{{getImageLink($tip->photo)}}"> <i class='fa fa-eye' style="color:White"></i> View </a>
                                                                </span>
                                                            </td>
                                                        {{-- <td data-title="Při sázce 1 000 Kč byste vyhráli" data-currency="Kč" data-mod="2.8"><span class="amount">+ 2 800 Kč</span></td> --}}
                                                    </tr>
                                                    @endif
                                                @endforeach
                                            @endif

                                        </tbody>
                                    </table>
                                    <div class='w-50 text-center m-auto'>
                                    {{$tips->links()}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

    @endif
<img src='{{asset('img/intro-bg.jpg')}}' style='visibility:hidden;width:1px' />
<img src='{{asset('img/intro-bg2.jpg')}}' style='visibility:hidden;width:1px' />
<img src='{{asset('img/intro-bg3.jpg')}}' style='visibility:hidden;width:1px' />
<img src='{{asset('img/intro-bg4.jpg')}}' style='visibility:hidden;width:1px' />
<img src='{{asset('img/intro-bg5.jpg')}}' style='visibility:hidden;width:1px' />
<img src='{{asset('img/intro-bg5.jpg')}}' style='visibility:hidden;width:1px' />


<section id="about" class="dark py-5">
    <div class="container">

      <header class="section-header">
        <h3>ČASTO KLADENÉ OTÁZKY</h3>
        {{-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p> --}}
      </header>

      <div class="container">
          <div class="row">

              <div class="col-lg-12">
                  <div class="tab-content" id="faq-tab-content">
                      <div class="tab-pane show active" id="tab1" role="tabpanel" aria-labelledby="tab1">
                          <div class="accordion" id="accordion-tab-1">
                              @if(isset($faq))
                                  @if(count($faq))
                                      @foreach($faq as $key => $value)
                                          <div class="card">
                                              <div class="card-header" id="accordion-tab-1-heading-{{$loop->iteration}}">
                                                  <h5>
                                                      <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#accordion-tab-1-content-{{$loop->iteration}}" aria-expanded="false" aria-controls="accordion-tab-1-content-{{$loop->iteration}}">
                                                          {{$key}}
                                                      </button>
                                                  </h5>
                                              </div>
                                            <div class="collapse @if($loop->iteration == 1) show @endif" id="accordion-tab-1-content-{{$loop->iteration}}" aria-labelledby="accordion-tab-1-heading-{{$loop->iteration}}" data-parent="#accordion-tab-1">
                                                  <div class="card-body">
                                                      <p> {{$value}}</p>

                                                  </div>
                                              </div>
                                          </div>


                                      @endforeach
                                  @endif
                              @endif



                          </div>
                      </div>

                  </div>
              </div>
          </div>
      </div>

    </div>
</section><!-- #about -->


{{-- <section id="about">
        <header class="section-header">
                <h3>Review</h3>
              </header>

    <div class="container">
      <div class="grid">
              @isset($reviews)
              @if(count($reviews))
                  @foreach($reviews as $review)
                      @if($loop->iteration%2 || $loop->iteration == 4)
                  <div class="grid-item">
                          <a  data-lightbox="portfolio" class="image-popup-no-margins btn btn-xs text-white"  href="{{getImageLink($review->path)}}"><img src="{{getImageLink($review->path)}}" class="d-block w-100 img-fluid" alt="..."></a>
                  </div>
                  @else
                  <div class="grid-item grid-item--width2">
                          <a  data-lightbox="portfolio" class="image-popup-no-margins btn btn-xs text-white"  href="{{getImageLink($review->path)}}"><img src="{{getImageLink($review->path)}}" class="d-block w-100 img-fluid" alt="..."></a>

                  </div>
                  @endif



                  @endforeach
              @else


              @endif
          @else

          @endisset

      </div>
  </div>
</section> --}}



<section id="contact" class="section-bg">
        <div class="container-fluid">

          <div class="section-header">
            <h3>Kontaktujte nás</h3>
          </div>

          <div class="row wow fadeInUp">

            {{-- <div class="col-lg-6">
              <div class="map mb-4 mb-lg-0">
                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d12097.433213460943!2d-74.0062269!3d40.7101282!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xb89d1fe6bc499443!2sDowntown+Conference+Center!5e0!3m2!1smk!2sbg!4v1539943755621" frameborder="0" style="border:0; width: 100%; height: 312px;" allowfullscreen></iframe>
              </div>
            </div> --}}

            <div class="col-lg-6 m-auto">
              <div class="row">
                <div class="col-md-5 info">
                  <i class="ion-ios-location-outline"></i>
                  <p>{{$contact['address']}}</p>
                </div>
                <div class="col-md-4 info">
                  <i class="ion-ios-email-outline"></i>
                  <p>{{$contact['email']}}</p>
                </div>
                <div class="col-md-3 info">
                  <i class="ion-ios-telephone-outline"></i>
                  <p>{{$contact['phone-number']}}</p>
                </div>
              </div>

              <div class="form">
                  @if(session('success'))
                  <div class='text-success text-center'>Your message has been sent. Thank you!</div>
                  @elseif(session('error'))
                      <div class='text-danger text-center' >{{session('error')}}</div>
                 @endif


              <form action="{{route('contact.store')}}" method="post" role="" class="">
                  @csrf
                  <div class="form-row">
                    <div class="form-group col-lg-6">
                      <input type="text" name="name" required class="form-control" id="name" placeholder="Vaše jméno" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                      <div class="validation"></div>
                    </div>
                    <div class="form-group col-lg-6">
                      <input type="email" class="form-control" required name="email" id="email" placeholder="Váš email" data-rule="email" data-msg="Please enter a valid email" />
                      <div class="validation"></div>
                    </div>
                  </div>
                  <div class="form-group">
                    <input type="text" class="form-control" required name="subject" id="subject" placeholder="Nadpis"  />
                    <div class="validation"></div>
                  </div>
                  <div class="form-group">
                    <textarea class="form-control" name="message" rows="5" required   placeholder="Zpráva"></textarea>
                    <div class="validation"></div>
                  </div>
                  <div class="text-center"><button type="submit" title="Odeslat zprávu">Odeslat zprávu </button></div>
                </form>
              </div>
            </div>

          </div>

        </div>
      </section><!-- #contact -->



@endsection


@push('scripts')
<script src="{{asset('assets/plugins/Magnific-Popup-master/dist/jquery.magnific-popup-init.js')}}"></script>

<script src="{{asset('assets/plugins/Magnific-Popup-master/dist/jquery.magnific-popup.min.js')}}"></script>

  <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.js"></script>
    <script>
        $(document).ready(function(){
            i = 0;
            img = [
               '{{asset('img/intro-bg.jpg')}}',
               '{{asset('img/intro-bg2.jpg')}}',
               '{{asset('img/intro-bg3.jpg')}}',
               '{{asset('img/intro-bg4.jpg')}}',
               '{{asset('img/intro-bg5.jpg')}}',
               '{{asset('img/intro-bg5.jpg')}}'

            ]
            setInterval(function(){


                      if(i == img.length)
                            i = 0;

                      //change background imagaes
                $('#intro').css({
                        "background-image": `url(${img[i]})`,

                    }).fadeIn();
                    console.log(i);
                    i++;

            }, 3000);
            $('[name=currency]').click(function(){
                var currency = $('[name=currency]:checked').val();
                $('.bet .currency').text(currency);
          $('.won .currency').text(currency);
            });

            $

        $('[name=bet]').keyup(function(){
          var value = $(this).val();
          var profit = value * $(this).attr('data-course');
          var currency = $('[name=currency]:checked').val();
          console.log(currency);
          $('.bet .amount').text(value);
          $('.won .amount').text("+ "+ Math.ceil(profit));
          $('.bet .currency').text(' '+currency);
          $('.won .currency').text(' ' +currency);



        });

        $('[name=second-calc]').keyup(function(){
          var value = $(this).val();
          if(value > 0){
            var day = value * $('[name=calc-q5]').val() * 1;
          var week = value * $('[name=calc-q5]').val() * 7;
          var month = value * $('[name=calc-q5]').val() * 30;

          $('#result-day span').text(Math.ceil(day));
          $('#result-week span').text(Math.ceil(week));
          $('#result-month span').text(Math.ceil(month));
          }else{
            $('#result-day span').text(0);
          $('#result-week span').text(0);
          $('#result-month span').text(0);
          }





        });

        $('[name=calc-q5]').change(function(){
          var value = $('[name=second-calc]').val();
          if(value > 0){
            var day = value * $(this).val() * 1;
          var week = value * $(this).val() * 7;
          var month = value * $(this).val() * 30;

          $('#result-day span').text(Math.ceil(day));
          $('#result-week span').text(Math.ceil(week));
          $('#result-month span').text(Math.ceil(month));
          }else{
            $('#result-day span').text(0);
          $('#result-week span').text(0);
          $('#result-month span').text(0);
          }





        });
        });
    </script>


<script src="https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.min.js"></script>
{{-- <script src="{{asset('assets/plugins/Magnific-Popup-master/dist/jquery.magnific-popup-init.js')}}"></script>

<script src="{{asset('assets/plugins/Magnific-Popup-master/dist/jquery.magnific-popup.min.js')}}"></script> --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.0/js/lightbox.min.js"></script>


<script>
$('.grid').masonry({
  // options
  itemSelector: '.grid-item',
  columnWidth: 200
});
</script>
@endpush
