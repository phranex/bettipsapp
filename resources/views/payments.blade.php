@extends('layouts.master')

@push('styles')
<style>
.carousel-indicators li,.carousel-control-next-icon, .carousel-control-prev-icon{
    background-color:Red;
}

#analyze-carousel .carousel-control-next-icon{
    width: 20px;
    height: 36px;
}
</style>

@endpush


@section('content')



  <main id="main">

    <!--==========================
      About Us Section
    ============================-->
    <section id="about">
      <div class="container">

        <header class="section-header">
          <h3>Automatické platby</h3>
        {{-- <p>{{$content['how-it-works-subtitle']}}</p> --}}
        </header>

        {{-- <div class="row about-container">

          <div class="col-lg-6 content order-lg-1 order-2">
            <p>
                {{$content['how-it-works-description']}}
            </p>

            <div class="icon-box wow fadeInUp">
              <div class="icon"><i class="fa fa-shopping-bag"></i></div>
              <h4 class="title"><a href="#">Step 1</a></h4>
              <p class="description"> {{$content['how-it-works-step1']}}</p>
            </div>

            <div class="icon-box wow fadeInUp" data-wow-delay="0.2s">
              <div class="icon"><i class="fa fa-photo"></i></div>
              <h4 class="title"><a href="#">Step 2</a></h4>
              <p class="description">{{$content['how-it-works-step2']}}</p>
            </div>

            <div class="icon-box wow fadeInUp" data-wow-delay="0.4s">
              <div class="icon"><i class="fa fa-bar-chart"></i></div>
              <h4 class="title"><a href="#">Step 3</a></h4>
              <p class="description">{{$content['how-it-works-step3']}}</p>
            </div>

          </div>

          <div class="col-lg-6 background order-lg-2 order-1 wow fadeInUp">
            <img src="img/about-extra-2.svg" class="img-fluid" alt="">
          </div>
        </div> --}}
        <div class="row">
            <!-- Column -->
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            @isset($content)
                                {!! $content !!}
                            @endisset
                        </div>
                    </div>
                </div>
            </div>




      </div>
    </section><!-- #about -->






</main>

@endsection


@push('scripts')


@endpush
