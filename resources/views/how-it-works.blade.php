@extends('layouts.master')

@push('styles')
<style>
.carousel-indicators li,.carousel-control-next-icon, .carousel-control-prev-icon{
    background-color:Red;
}
.carousel-control-next, .carousel-control-prev{
    margin: 0 -100px;
}

.text.light p{
    text-align: justify;
}

@media (max-width: 767px) {

    .carousel-control-next, .carousel-control-prev{
        margin: unset;
        display: none;
    }

    .text.light{
        padding: 3rem 1rem !important;
    }
}


#analyze-carousel .carousel-control-prev-icon {
    width: 20px;
    height: 36px;
    background:url({{asset('img/previous.png')}}) no-repeat 50% 50%;
}
#analyze-carousel .carousel-control-next-icon{
    width: 36px;
    height: 36px;
    background: url({{asset('img/next.png')}}) no-repeat 50% 50%;
}
#analyze-carousel h2 span {
    display: inline-block;
    background: #ef042f;
    padding: 5px 15px;
    border-radius: .25rem;
    color: #fff;
    margin-right: 20px;
}
</style>

@endpush


@section('content')



  <main id="main">

    <!--==========================
      About Us Section
    ============================-->
    <section id="about">
      <div class="container">

        <header class="section-header">
          <h3>JAK FUNGUJEME</h3>
        <p>{{$how}}</p>
        </header>

        {{-- <div class="row about-container">

          <div class="col-lg-6 content order-lg-1 order-2">
            <p>
                {{$content['how-it-works-description']}}
            </p>

            <div class="icon-box wow fadeInUp">
              <div class="icon"><i class="fa fa-shopping-bag"></i></div>
              <h4 class="title"><a href="#">Step 1</a></h4>
              <p class="description"> {{$content['how-it-works-step1']}}</p>
            </div>

            <div class="icon-box wow fadeInUp" data-wow-delay="0.2s">
              <div class="icon"><i class="fa fa-photo"></i></div>
              <h4 class="title"><a href="#">Step 2</a></h4>
              <p class="description">{{$content['how-it-works-step2']}}</p>
            </div>

            <div class="icon-box wow fadeInUp" data-wow-delay="0.4s">
              <div class="icon"><i class="fa fa-bar-chart"></i></div>
              <h4 class="title"><a href="#">Step 3</a></h4>
              <p class="description">{{$content['how-it-works-step3']}}</p>
            </div>

          </div>

          <div class="col-lg-6 background order-lg-2 order-1 wow fadeInUp">
            <img src="img/about-extra-2.svg" class="img-fluid" alt="">
          </div>
        </div> --}}
        @isset($content)
        <div id="analyze-carousel" class="carousel slide d-non d-md-block" data-ride="carousel">
                <div class="carousel-inner">
                    @if(count(@$content))
                    @php $i = 0; @endphp
                        @foreach($content as $key => $value)

                            {{-- <div class="carousel-item @if($loop->iteration == 0)active @endif">
                                <h2><span>Krok {{$loop->iteration}}:</span> {{$key}}</h2>
                                <div class="py-5 text light">
                                    <p><span style="font-size: 12.0pt; font-family: 'Calibri',sans-serif; mso-ascii-theme-font: minor-latin; mso-fareast-font-family: Calibri; mso-fareast-theme-font: minor-latin; mso-hansi-theme-font: minor-latin; mso-bidi-font-family: 'Times New Roman'; mso-bidi-theme-font: minor-bidi; mso-ansi-language: CS; mso-fareast-language: EN-US; mso-bidi-language: AR-SA;">
                                    {{$value}}<strong><br> <!-- [if !supportLineBreakNewLine]--><br> <!--[endif]--></strong></span></p>
                                </div>
                            </div> --}}
                            <div class="carousel-item @if($i == 0) active @endif">
                                    <h2><span>Krok {{$i + 1}}:</span> {{$key}}</h2>
                                    <div class="py-5 text light">
                                            <p><span style="font-size: 12.0pt; font-family: 'Calibri',sans-serif; mso-ascii-theme-font: minor-latin; mso-fareast-font-family: Calibri; mso-fareast-theme-font: minor-latin; mso-hansi-theme-font: minor-latin; mso-bidi-font-family: 'Times New Roman'; mso-bidi-theme-font: minor-bidi; mso-ansi-language: CS; mso-fareast-language: EN-US; mso-bidi-language: AR-SA;">
                                            {{$value}}<strong><br> <!-- [if !supportLineBreakNewLine]--><br> <!--[endif]--></strong></span></p>
                                    </div>
                                </div>
                                @php $i++; @endphp
                        @endforeach
                    @endif


                </div>
                <ol class="carousel-indicators">
                    @for($i = 0; $i < count($content); $i++)
                <li data-target="#analyze-carousel" data-slide-to="{{$i}}" class="@if($i == 0)active @endif"></li>
                    @endfor

                </ol>
                <a class="carousel-control-prev" href="#analyze-carousel" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Předchozí</span>
                </a>
                <a class="carousel-control-next" href="#analyze-carousel" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Další</span>
                </a>
            </div>

        @endisset




      </div>
    </section><!-- #about -->






</main>

@endsection


@push('scripts')


@endpush
