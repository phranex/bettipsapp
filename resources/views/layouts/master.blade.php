<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from technext.github.io/newbiz/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 13 Mar 2019 13:47:24 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
  <meta charset="utf-8">
  <title>{{config('app.name')}} </title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Favicons -->
  <link href="img/favicon.png" rel="icon">
  <link href="img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Montserrat:300,400,500,700" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="{{asset('lib/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="{{asset('lib/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
  <link href="{{asset('lib/animate/animate.min.css')}}" rel="stylesheet">
  <link href="{{asset('lib/ionicons/css/ionicons.min.css')}}" rel="stylesheet">
  <link href="{{asset('lib/owlcarousel/assets/owl.carousel.min.css')}}" rel="stylesheet">
  <link href="{{asset('lib/lightbox/css/lightbox.min.css')}}" rel="stylesheet">
  <link href="{{asset('assets/plugins/Magnific-Popup-master/dist/magnific-popup.css')}}" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="{{asset('css/style.css')}}" rel="stylesheet">
  <link href="{{asset('css/custom.css')}}" rel="stylesheet">
  <style>
      h3{
        text-transform: uppercase;
        font-weight: bold;
    }

        .btn-get-started{
            background: #ffe900;
        border: 2px solid #ffe900;
        color: #000;
        font-family: "Montserrat", sans-serif;
        font-size: 14px;
        font-weight: 600;
        letter-spacing: 1px;
        display: inline-block;
        padding: 10px 32px;
        border-radius: 50px;
        transition: 0.5s;
        margin: 0 20px 20px 0;
        cursor: pointer;
        }
        .plus18 {
    width: 40px;
    display: inline-block;
}
        .mobile-nav a{
            color:#ffe900;
        }

        .btn-get-started:hover{
            background: none;
        border-color: #000;
        color: #000;
        }
        </style>

    @stack('styles')
</head>

<body>

  <!--==========================
  Header
  ============================-->
  <header id="header" class="fixed-top">
    <div class="container">

      <div class="logo float-left">
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <h1 class="text-light"><a href="#header"><span>{{config('app.name')}}</span></a></h1> -->
        <a href="#intro" class="scrollto"><img src="{{asset('img/logo.png')}}" alt="" class="img-fluid"></a>
      </div>

      <nav class="main-nav float-right d-none d-lg-block">
        <ul>
          <li class="active"><a href="{{route('index')}}">Úvod</a></li>
          {{-- <li><a href="{{route('about')}}">About Us</a></li> --}}
          <li><a href="{{route('how-it-works')}}">Jak fungujeme</a></li>
          <li><a href="{{route('faq')}}">Dotazy</a></li>
          {{-- <li><a href="{{route('team')}}">Team</a></li> --}}
          {{-- <li><a href="#">Články</a> --}}

          </li>
          <li><a href="{{route('contact')}}">Kontakt</a></li>
          <li><a href="{{route('reviews')}}">REFERENCE</a></li>
          @guest
          <li><a href="{{route('login')}}">Přihlášení</a></li>
          <li><a href="{{route('register')}}">Registrace</a></li>
          @else
        <li><a href="{{route('user.dashboard')}}"> <i class="fa fa-user-circle"></i>  {{auth()->user()->name}}</a></li>
          @endguest
          {{-- <li class="drop-down"><a href="#">Start Winning</a>
            <ul>




            </ul>
          </li> --}}
        </ul>
      </nav><!-- .main-nav -->

    </div>
  </header><!-- #header -->

  @yield('content')



  </main>

  <!--==========================
    Footer
  ============================-->
  <footer id="footer">
    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-4 col-md-6 footer-info">
            <h3>{{config('app.name')}}</h3>
          <p>{{$content2['footer-description']}}</p>
          <img style='width:50%;' src="img/about-img2.png" class="img-fluid" alt="">
          </div>

          <div class="col-lg-2 col-md-6 footer-links">
            <h4>Užitečné odkazy</h4>
            <ul>
            <li><a href="{{route('index')}}">Úvod</a></li>
              <li><a href="{{route('how-it-works')}}">Jak fungujeme</a></li>
              <li><a href="{{route('payments')}}">Automatické platby</a></li>
              <li><a href="{{route('terms')}}">Všeobecné obchodní podmínky</a></li>
              <li><a href="{{route('privacy')}}">Ochrana a zpracování osobních údajů</a></li>
              <li><a href="{{route('guarantee')}}">30 denní garance spokojenosti</a></li>
            </ul>
          </div>

          <div class="col-lg-3 col-md-6 footer-contact">
            <h4>Kontakt</h4>
            <p>
              {{$contact['address']}} <br>
              {{-- New York, NY 535022<br>
              United States <br> --}}
              {{-- <strong>Phone:</strong> +1 5589 55488 55<br> --}}
              <strong>Email:</strong> {{$contact['email']}}<br>
            </p>

            <div class="social-links">
              <a href="{{@$content2['twitter']}}" class="facebook"><i class="fa fa-twitter"></i></a>
              <a href="{{@$content2['facebook']}}" class="facebook"><i class="fa fa-facebook"></i></a>
              <a href="{{@$content2['instagram']}}" class="facebook"><i class="fa fa-instagram"></i></a>
              {{-- <a href="#" class="instagram"><i class="fa fa-instagram"></i></a>
              <a href="#" class="google-plus"><i class="fa fa-google-plus"></i></a>
              <a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a> --}}
            </div>

          </div>

          {{-- <div class="col-lg-3 col-md-6 footer-newsletter">
            <h4>Our Newsletter</h4>
            <p>Tamen quem nulla quae legam multos aute sint culpa legam noster magna veniam enim veniam illum dolore legam minim quorum culpa amet magna export quem marada parida nodela caramase seza.</p>
            <form action="#" method="post">
              <input type="email" name="email"><input type="submit"  value="Subscribe">
            </form>
          </div> --}}

        </div>
      </div>
    </div>
    <div class="footer-line">
            <div class="col pt-2 text-center">
                <span class="light">Web je určen pouze pro uživatele starší 18 let.</span>
                <img src="img/18-years.png" alt="18+" class="plus18 pl-1">
            </div>
        </div>

    <div class="container">
      <div class="copyright">
        &copy; Práva <strong>{{config('app.name')}}</strong>. Všechny práva vyhrazena
      </div>
      <div class="credits">
        <!--
          All the links in the footer should remain intact.
          You can delete the links only if you purchased the pro version.
          Licensing information: https://bootstrapmade.com/license/
          Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=NewBiz
        -->
        {{-- Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a> --}}
      </div>
    </div>

  </footer><!-- #footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
  <!-- Uncomment below i you want to use a preloader -->
  <!-- <div id="preloader"></div> -->

  <!-- JavaScript Libraries -->
  <script src="{{asset('lib/jquery/jquery.min.js')}}"></script>
  <script src="{{asset('lib/jquery/jquery-migrate.min.js')}}"></script>
  <script src="{{asset('lib/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
  <script src="{{asset('lib/easing/easing.min.js')}}"></script>
  <script src="{{asset('lib/mobile-nav/mobile-nav.js')}}"></script>
  <script src="{{asset('lib/wow/wow.min.js')}}"></script>
  <script src="{{asset('lib/waypoints/waypoints.min.js')}}"></script>
  <script src="{{asset('lib/counterup/counterup.min.js')}}"></script>
  <script src="{{asset('lib/owlcarousel/owl.carousel.min.js')}}"></script>
  <script src="{{asset('lib/isotope/isotope.pkgd.min.js')}}"></script>
  <script src="{{asset('lib/lightbox/js/lightbox.min.js')}}"></script>
  <!-- Contact Form JavaScript File -->
  <script src="{{asset('contactform/contactform.js')}}"></script>

  <!-- Template Main Javascript File -->
  <script src="{{asset('js/main.js')}}"></script>
  @stack('scripts')

</body>

<!-- Mirrored from technext.github.io/newbiz/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 13 Mar 2019 13:49:37 GMT -->
</html>
