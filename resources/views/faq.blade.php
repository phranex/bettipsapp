@extends('layouts.master')

@push('styles')

<style>
    // Just for kicks and layout
body {
  margin-top: 30px;
  background-color: #eee;
}


// FAQ
.faq-nav {
    flex-direction: column;
    margin: 0 0 32px;
    border-radius: 2px;
    border: 1px solid #ddd;
    box-shadow: 0 1px 5px rgba(85, 85, 85, 0.15);

    .nav-link {
        position: relative;
        display: block;
        margin: 0;
        padding: 13px 16px;
        background-color: #fff;
        border: 0;
        border-bottom: 1px solid #ddd;
        border-radius: 0;
        color: #616161;
        transition: background-color .2s ease;

        &:hover {
            background-color: #f6f6f6;
        }

        &.active {
            background-color: #f6f6f6;
            font-weight: 700;
            color: rgba(0,0,0,.87);
        }

        &:last-of-type {
            border-bottom-left-radius: 2px;
            border-bottom-right-radius: 2px;
            border-bottom: 0;
        }

        i.mdi {
            margin-right: 5px;
            font-size: 18px;
            position: relative;
        }
    }
}

// TAB CONTENT
.tab-content {
    box-shadow: 0 1px 5px rgba(85, 85, 85, 0.15);

    .card {
        border-radius: 0;
    }

    .card-header {
        padding: 15px 16px;
        border-radius: 0;
        background-color: #f6f6f6;

        h5 {
            margin: 0;

            button {
                display: block;
                width: 100%;
                padding: 0;
                border: 0;
                font-weight: 700;
                color: rgba(0,0,0,.87);
                text-align: left;
                white-space: normal;

                &:hover,
                &:focus,
                &:active,
                &:hover:active {
                    text-decoration: none;
                }
            }
        }
    }

    .card-body {
        p {
            color: #616161;

            &:last-of-type {
                margin: 0;
            }
        }
    }
}


// BORDER FIX
.accordion {
    > .card {
        &:not(:first-child) {
            border-top: 0;
        }
    }
}

.collapse.show {
    .card-body {
        border-bottom: 1px solid rgba(0,0,0,.125);
    }
}
</style>
@endpush


@section('content')



  <main id="main">

    <!--==========================
      About Us Section
    ============================-->
    <section id="about">
      <div class="container">

        <header class="section-header">
          <h3>ČASTO KLADENÉ OTÁZKY</h3>
          {{-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p> --}}
        </header>

        <div class="container">
            <div class="row">

                <div class="col-lg-12">
                    <div class="tab-content" id="faq-tab-content">
                        <div class="tab-pane show active" id="tab1" role="tabpanel" aria-labelledby="tab1">
                            <div class="accordion" id="accordion-tab-1">
                                @if(isset($content))
                                    @if(count($content))
                                        @foreach($content as $key => $value)
                                            <div class="card">
                                                <div class="card-header" id="accordion-tab-1-heading-1">
                                                    <h5>
                                                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#accordion-tab-1-content-1" aria-expanded="false" aria-controls="accordion-tab-1-content-1">
                                                            {{$key}}
                                                        </button>
                                                    </h5>
                                                </div>
                                                <div class="collapse show" id="accordion-tab-1-content-1" aria-labelledby="accordion-tab-1-heading-1" data-parent="#accordion-tab-1">
                                                    <div class="card-body">
                                                        <p> {{$value}}</p>

                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    @endif
                                @endif

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

      </div>
    </section><!-- #about -->






</main>

@endsection


@push('scripts')


@endpush
