<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tip extends Model
{
    //
    public function level()
    {
        return $this->belongsTo('App\Level', 'level_id');
    }
    public function package()
    {
        return $this->belongsTo('App\Package');
    }

}
