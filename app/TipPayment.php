<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipPayment extends Model
{
    //
    public function create($request){
        $this->user_id = auth()->id();
        $this->payment_id = $request->payment;
        $this->tip_id = $request->tip;
        return $this->save();
    }
}
