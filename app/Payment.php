<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    //




    public function create($user,$request, $ref, $amount, $type, $status = null,$paypal = null) : bool {
        $this->user_id = $user;
        $this->level_id = isset($request->level)?$request->level:null;
        $this->reference_number = $ref;
        $this->amount = $amount;
        $this->type = $type;
        $this->paypal_subscription_id = $paypal;
        if(isset($status))
            $this->status = $status;
        return $this->save();
    }


    public function createRecord($user, $ref,$amount, $type,$status =null){
        $this->user_id = $user;

        $this->reference_number = $ref;
        $this->amount = $amount;
        $this->type = $type;
        if(isset($status))
            $this->status = $status;
        return $this->save();
    }

    public function level()
    {
        return $this->belongsTo('App\Level', 'level_id');
    }


}
