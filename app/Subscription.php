<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    //
    /**
     * @return bool
     */
    public function membership(): bool
    {
        return $this->hasOne('\App\Membership');
    }
}
