<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    //
    public function levels()
    {
        return $this->hasMany('App\Level', 'package_id');
    }
}
