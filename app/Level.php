<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Level extends Model
{
    //

    public function tips()
    {
        return $this->hasMany('App\Tip', 'level_id');
    }

    public function package()
    {
        return $this->belongsTo('App\Package', 'package_id');
    }



    public function latestTips()
    {
        # code...
        $this->tips->where('status',1)->get();
    }
}
