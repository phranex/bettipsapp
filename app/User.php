<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','verifyToken','verified'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function updateUser($request){
        $this->name = $request->input('name');

        return $this->save();


    }

    public function memberships()
    {
        return $this->hasMany('App\Membership', 'user_id');
    }

    public function level()
    {
        return $this->belongsTo('App\Level', 'level_id');
    }

    public function activeMembership()
    {
        return $this->hasMany('App\Membership', 'user_id')
                ->where('status', 1)->get();
    }

    public function isSubscribedToMainPackage()
    {
        $memberships = $this->memberships;
        if(!empty($memberships)){
            foreach($memberships as $membership){
                $main = $membership->level->package->show_packages;
                if($main == 1)
                    return true;
            }
        }
        return false;
    }
}
