<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
        Schema::defaultStringLength(191);
        view()->composer(['layouts.master'], function($view){
            //get all inactive transactions
            $content = null;
            $contact = null;
            $indexSetting = \App\Setting::where('name','index')->first();
            if($indexSetting)
            $content = unserialize($indexSetting->value);
            $contactSetting = \App\Setting::where('name','contact')->first();
            if($contactSetting)
            $contact = unserialize($contactSetting->value);

                $view->with(['content2' => $content, 'contact' => $contact]);
            });

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
