<?php


namespace App\Library\Paypal;

use App\Library\Paypal\Paypal;
use App\Library\Consume;


class Plan
{
    public function create($package,$request)
    {


        try {
            $url = 'billing/plans';
            $data  = [
                'product_id' => $package->product_id,
                'name' => $request->name,
                'description' => strip_tags($request->description),
                "status" => "ACTIVE",

                "payment_preferences" => [
                    "auto_bill_outstanding" => true,
                    // "setup_fee" => [
                    // "value": "10",
                    // "currency_code": "USD"
                    // },
                    "setup_fee_failure_action" => "CONTINUE",
                    "payment_failure_threshold" => 3,
                    ],
                "billing_cycles" => $this->createBillingCycle($request)
            ];

            // array_merge($data,$this->createBillingCycle($request));
            $remove_escaped = str_replace('\"','"',json_encode($data));
            $remove_extra_quotes = str_replace('["', '[', $remove_escaped);
            $data = str_replace('"]', ']', $remove_extra_quotes);

            $client = Consume::getInstance();
            $response = $client->getResponseViaCurl('POST',$url,$data);
            return (object) [
                'status' => 200,
                'message' => 'success',
                'data' => $response
            ];
        } catch (\Throwable $th) {
           logger($th);
            return (object) [
                'status' => 0,
                'message' => $th->getMessage(),
                'data' => ''
            ];
        }

    }

    public function createBillingCycle($request)
    {
        # code...

       $x =  [
            "frequency" => [
                "interval_unit" => "DAY",
                "interval_count" => $request->period
            ]
            ,
            "tenure_type" => "REGULAR",
            "sequence" => 1,
            "total_cycles" =>  0,
            "pricing_scheme" => [
                "fixed_price" => [
                "value" => $request->price,
                "currency_code" => "CZK"
                ],
            ]
            ,

        ];

        $data = array(json_encode($x,true));
        return $data;

    }
}
