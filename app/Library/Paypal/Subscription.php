<?php


namespace App\Library\Paypal;

use App\Library\Consume;


class Subscription
{

    public function showDetails($id)
    {
        # code...
        try {
            $url = "billing/subscriptions/$id";
            $client = Consume::getInstance();
            $response = $client->getResponseViaCurl('GET',$url);
            return (object) [
                'status' => 200,
                'message' => 'success',
                'data' => $response
            ];

        } catch (\Throwable $th) {
            //throw $th;
            logger($th);
            return (object) [
                'status' => 0,
                'message' => $th->getMessage(),
                'data' => ''
            ];
        }
    }

}
