<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PackageController extends Controller
{
    //

    public function getLevels()
    {
        # code...
        if(is_numeric(request('id'))){
            $package = \App\Package::find(request('id'));
            if($package){
                return array('status' => 1, 'data' => $package->levels);
            }

            return array('status' => 0, 'data' => []);
        }
    }


}
