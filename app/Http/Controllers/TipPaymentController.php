<?php

namespace App\Http\Controllers;

use App\Payment;
use App\TipPayment;
use Illuminate\Http\Request;

class TipPaymentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:web');


       // $this->isVerified  = auth()->user()->verified;

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        if(auth()->user()->blocked)
            return redirect(route('user.blocked'));
        if(!empty(request('tip')) && !empty(request('payment'))){
            $payment = Payment::find(request('payment'));

            if(!$payment) return  redirect(route('user.dashboard'))->with('error', 'Unable to complete tip purchase. Please try again.');
            $tipPayment = new \App\TipPayment;
            if($tipPayment->create($request)){
                return redirect(route('user.dashboard'))->with('success', 'You successfully bought a tip');
            }

            return  redirect(route('user.dashboard'))->with('error', 'Unable to complete tip purchase. Please try again.');
        }
        return  redirect(route('user.dashboard'))->with('error', 'An unexpected error occurred. Please try again.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TipPayment  $tipPayment
     * @return \Illuminate\Http\Response
     */
    public function show(TipPayment $tipPayment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TipPayment  $tipPayment
     * @return \Illuminate\Http\Response
     */
    public function edit(TipPayment $tipPayment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TipPayment  $tipPayment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TipPayment $tipPayment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TipPayment  $tipPayment
     * @return \Illuminate\Http\Response
     */
    public function destroy(TipPayment $tipPayment)
    {
        //
    }
}
