<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Mail\ActivationMail;
use Mail;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/user/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('verifyEmail');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'verifyToken' => \Illuminate\Support\Str::random(40),
            'verified' => 0,
        ]);

        $this->sendVerificationEmail($user);
        return $user;

    }


    public function sendVerificationEmail($user){
        // $when = \Carbon\Carbon::now()->addSeconds(5);
            try{
                Mail::to($user)->send(new ActivationMail($user));

            }catch(Exception $e){
                return  session(['error' => 'Verification mail could not be sent']);
            }



    }


    public function verifyEmail($email, $token){
        $email = base64_decode($email);
         $user = \App\User::where(['email' => $email, 'verifyToken' => $token])->first();
        if($user){
            $status = $user->update([
                'verified' => '1',
                'verifyToken'=> null
            ]);
        }else{
            $status = 0;
        }

        return view('user.verifyEmail', compact('status'));
    }
}
