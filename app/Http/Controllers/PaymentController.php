<?php /** @noinspection PhpUnreachableStatementInspection */

namespace App\Http\Controllers;

use App\Library\Paypal\Order;
use App\Library\Paypal\Subscription;
use App\Payment;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    const format = 'PR';
    public function __construct()
    {
        $this->middleware('auth:web');

       // $this->isVerified  = auth()->user()->verified;

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // $request->validate([
        //     $request->level => 'required|numeric'
        // ]);
        //check sub id
        $subscriptionId = $request->subscription_id;
        $subscription = new Subscription;
        $sub = $subscription->showDetails($subscriptionId);


        if(empty($request->level_id) || $request->level_id <= 0 || empty($request->subscription_id))
            return back()->with('error', 'An unexpected error occurred');

        $payment = new Payment;
        $level = \App\Level::find($request->level_id);
        if(empty($level))
            return redirect(route('user.dashboard'))->with('error', 'An unexpected error occurred');

        $ref = $this->generatePaymentReference(self::format);
        if($sub->data->status == 'ACTIVE')
        $status = 'completed'; //temporarily. will be removed when gateway is used {pending,completed,declined}
        else
            $status = 'pending';
        /***
            check for duplicates. todo
         */
        $paym = Payment::where('user_id',auth()->id())->where('level_id',$request->level)->where('paypal_subscription_id',$subscriptionId)->first();
        if(empty($paym)){
            if($payment->create(auth()->id(),$request, $ref, $level->price, 'Membership Upgrade', $status, $request->subscription_id)){
                //create a successful subscription
                //notify admin of initiated payment.
                //for now we will create a memnership level for the logged in user
                return redirect(route('user.membership.store', [$payment->level_id,$payment->id,$request->subscription_id]));
            }
        }
        return back()->with('error', 'Duplicate Payment');

    }

    public function storeCredit(Request $request)
    {
        //

        // $request->validate([
        //     $request->level => 'required|numeric'
        // ]);

        // get user balance

        $balance = getUserCreditBalance(auth()->id());

        if(empty($request->level) && $request->level < 0)
            return back()->with('error', 'An unexpected error occurred');

        $payment = new Payment;
        $level = \App\Level::find($request->level);

        if(empty($level) )
            return redirect(route('user.dashboard'))->with('error', 'An unexpected error occurred');
        if($balance < $level->price){
            return back()->with('error', 'Insufficient credit.');
        }

        $ref = $this->generatePaymentReference(self::format);
        if($payment->create($request, $ref, $level->price, 'Membership Upgrade: Paid with Credit')){
            //create a new credit transaction
            $credit = new \App\Credit;
            $req = (object) ['amount' => -$level->price, 'user_id' => auth()->id()];
            $type = 'Top up Transaction: Debit';
            if( $credit->create($req,$type)){
                $payment->status = 'completed';
                $payment->save();
                return redirect(route('user.membership.store', [$payment->level_id,$payment->id]));
            }

            $payment->delete();
            return back()->with('error', 'Unable to upgrade. Please try again later');

            //notify admin of initiated payment.


            //for now we will create a memnership level for the logged in user
          ;

        }
    }

    public function storeTip(Request $request)
    {
        //

//    return request()->all();
        // $request->validate([
        //     $request->level => 'required|numeric'
        // ]);
        if(empty($request->tip) || $request->tip < 0 || empty($request->order_id))
            return back()->with('error', 'An unexpected error occurred');

        $payment = new Payment;
        $tip = \App\Tip::find($request->tip);
        if(empty($tip))
            return redirect(route('user.dashboard'))->with('error', 'An unexpected error occurred');
        $ref = $this->generatePaymentReference(self::format);
        //check order status from payapal
        $order = new Order;
        $ord = $order->showDetails($request->order_id);

        if($ord->data){
            if($ord->data->status != 'COMPLETED' || number_format($ord->data->purchase_units[0]->amount->total) != $tip->amount || $ord->data->purchase_units[0]->amount->currency != 'CZK')
                return back()->with('error', 'An unexpected error occurred');
            $status = 'completed'; //temporarily. will be removed when gateway is used
            if($payment->create(auth()->id(),$request, $ref, $tip->amount, 'zakoupený balíček', $status)){
                //redirect to gateway
                //zakoupený balíček => Tip purchase


                //notify admin of initiated tip payment.


                //for now we will create a tip payment for the logged in user
                return redirect(route('tip-payment.store', [$request->tip,$payment->id]));

            }
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function showInvoice()
    {
        //
        return view('payment.show-invoice');
    }

    public function createMembershipLevel(Payment $payment){
        if($payment->createMembershipLevel){
            return redirect(route('user.dashboard'))->with('success', 'Vaše členství bylo úspěšně aktivováno');
        }

        return  redirect(route('user.dashboard'))->with('error', 'Your membership level was not successfully upgraded. Please try again.');
    }

    public  function  generatePaymentReference($format){
        $time = strtotime(\Carbon\Carbon::now());


        do {
            $pseudo = rand(1000, 4000);
        } while (strlen((string)$pseudo) < 4);

        $pseudoRegistrationNumber = $format. $pseudo . $time;

        return $pseudoRegistrationNumber;
    }
}
