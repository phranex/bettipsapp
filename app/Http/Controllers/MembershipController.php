<?php

namespace App\Http\Controllers;

use App\Membership;
use App\Subscription;
use Illuminate\Http\Request;

class MembershipController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:web');


       // $this->isVerified  = auth()->user()->verified;

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        if(!empty(request('level')) && !empty(request('subscription')) && !empty(request('payment'))){
            //create new sub
            $subscription = new Subscription;
            $subscription->payment_id = $request->payment;
            $subscription->user_id = auth()->id();
            $subscription->paypal_subscription_id = $request->subscription;
            if($subscription->save()){
                $mem = Membership::where('user_id',auth()->id())->where('subscription_id',$subscription->id)->first();
                if(empty($mem)){
                    $membership = new Membership;
                    if($membership->create($request,$subscription->id)){
                        //create subscription

                        return redirect(route('user.dashboard'))->with('success', 'Vaše členství bylo úspěšně aktivováno');
                    }
                }
                return  redirect(route('user.dashboard'))->with('error', 'Duplicate membership record');

            }

            return  redirect(route('user.dashboard'))->with('error', 'Your membership level was not successfully upgraded. Please try again.');
        }
        return  redirect(route('user.dashboard'))->with('error', 'An unexpected error occurred. Please try again.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
