<?php

namespace App\Http\Controllers;

use App\User;
use App\Tip;
use Illuminate\Http\Request;

class UserController extends Controller
{
    private $user;
    public function __construct()
    {
        $this->middleware('auth:web')->except('logout');
        $this->middleware('subscribedToMain')->except('upgrade');

       // $this->isVerified  = auth()->user()->verified;

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if(auth()->user()->blocked)
        return redirect(route('user.blocked'));
        //get this users active membership and show tips based on that
        $active_memberships = auth()->user()->activeMembership();
        //get
        auth()->user()->isSubscribedToMainPackage();



        $tips  = [];
        $packages = [];
        $grouped_tips = [];
        $grouped_premium_tips = [];
        //dd($active_memberships);
        foreach($active_memberships as $level){
            //get level
            $level_id = $level->level_id;
            $level = \App\Level::find($level_id);

            if($level){
                array_push($packages, $level->package_id);
            }

        }

       $tips = Tip::whereIn('package_id',$packages)->where('status', 1)->orderBy('package_id')->get();
       //dd($tips);
       $tips_num = count($tips);
       if($tips_num){
           foreach($tips as $tip){
                $package = \App\Package::find($tip->package_id);
               if(!isset($grouped_tips[$package->name])){


                        $grouped_tips[$package->name] = array();
               }
               array_push($grouped_tips[$package->name],$tip);
           }

       }
       //dd($grouped_tips);

       $premium_tips = Tip::where('status', 1)->where('single_purchase_tip',1)->whereNotIn('package_id', $packages)->get();

       if(count($premium_tips)){
            foreach($premium_tips as $tip){
                $package = \App\Package::find($tip->package_id);
                if(!isset($grouped_premium_tips[$package->name])){
                    $grouped_premium_tips[$package->name] = array();
                }
                array_push($grouped_premium_tips[$package->name],$tip);
            }

        }
        //dd($grouped_premium_tips);


        return view('user.index', compact('grouped_tips', 'grouped_premium_tips', 'active_memberships'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function profile()
    {
        # code...
        if(auth()->user()->blocked)
        return redirect(route('user.blocked'));
        return view('user.profile');
    }

    public function balance()
    {
        # code...
        //get this users payments from payment table
        if(auth()->user()->blocked)
        return redirect(route('user.blocked'));
        $balance = \App\Credit::where('user_id',auth()->id())->pluck('amount')->sum();
        $payments = \App\Payment::where('user_id', auth()->id())->paginate(10);

        return view('user.balance', compact('payments','balance'));
    }

    public function upgrade()
    {
        # code...
        if(auth()->user()->blocked)
        return redirect(route('user.blocked'));
        if(auth()->user()->isSubscribedToMainPackage())
            $packages = \App\Package::where('show_packages', 0)->get();
        else
            $packages = \App\Package::where('show_packages',1)->get();
        return view('user.upgrade', compact('packages'));
    }
    public function invoice()
    {
        # code...
        if(auth()->user()->blocked)
        return redirect(route('user.blocked'));
        return view('user.invoice');
    }


    public function update(Request $request){
        if(auth()->user()->blocked)
        return redirect(route('user.blocked'));
        $this->validate($request,[
            'name' => 'string|',
            ]);
        $user = \App\User::find(auth()->id());
        if($user->updateUser($request)){
            return back()->with('success', 'Successfully updated');
        }
        return back()->with('error', 'An unexpected error occurred');

    }

    public function instructions()
    {
        # code...
        if(auth()->user()->blocked)
        return redirect(route('user.blocked'));
        $instruction = \App\Setting::where('name','instruction')->first();
        if($instruction){
            $content = unserialize($instruction->value);
            $content = $content['editordata'];
            return view('user.instructions', compact('content'));
        }
       return back()->with('error', 'There\'s  currently no information');
    }

    public function changePasswordMethod(Request $request)
    {
        if(auth()->user()->blocked)
        return redirect(route('user.blocked'));
        $this->validate($request, [
            "old_password" => "required",
            "password" => "required|confirmed",
        ]);
        $old = request('old');
        $user = \App\User::find(auth()->id());

        if(Hash::check($old,$user->password)){
            $user->password = Hash::make(request('password'));
            $user->save();
            auth()->logout();
            return back();
        }else{
            return back()->with('error', 'Your password is incorrect');
        }
    }

}
