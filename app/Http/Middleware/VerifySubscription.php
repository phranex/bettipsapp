<?php

namespace App\Http\Middleware;

use Closure;

class VerifySubscription
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth()->check()){
            if(!auth()->user()->isSubscribedToMainPackage())
                return redirect()->route('user.upgrade')
                    ->with('error', 'Access Denied: Please subscribe to the main plan');
        }

        return $next($request);
    }
}
