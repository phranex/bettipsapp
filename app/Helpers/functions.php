<?php
const format = 'PR';


function getDateFormat($string,$format){
        echo \Carbon\Carbon::parse($string)->format($format);
}

function when($string){
    echo \Carbon\Carbon::parse($string)->diffForHumans();
}


function checkIfBought($id){
    $isPaid = \App\TipPayment::where('user_id', auth()->id())->where('tip_id',$id)->first();

    if(isset($isPaid))
        return true;

    return false;
}

function createPaymentRecord($user,$amount, $type,$status =null){
    $payment = new \App\Payment;
    $ref = generatePaymentReference(format);
    return $payment->createRecord($user,$ref,$amount,$type, $status);

}

function currency_format($amt){
    $eur = ceil($amt/25);
    echo $amt . ' CZK | '. $eur . ' EUR';
}


function  generatePaymentReference($format){
    $time = strtotime(\Carbon\Carbon::now());


    do {
        $pseudo = rand(1000, 4000);
    } while (strlen((string)$pseudo) < 4);

    $pseudoRegistrationNumber = $format. $pseudo . $time;

    return $pseudoRegistrationNumber;
}

function getUserCreditBalance($id){
    return $balance = \App\Credit::where('user_id',$id)->pluck('amount')->sum();
}

function getImageLink($link){

    return env('ADMIN_URL').'/storage/'.$link;
}

function checkIfBlocked(){
    if(auth()->user()->blocked)
            return redirect(route('user.blocked'));
}

// function getPackageName($id){

//     $package = \App\Package::find($id);

//     if(!empty($package))

//     echo $package->name;
// }


?>
