<?php

return [
    'env' => [
        'live_base_url' => env('PAYPAL_LIVE'),
        'dev_base_url' => env('PAYPAL_DEV')
    ],
    'key' => env('PAYPAL_KEY'),
    'secret' => env('PAYPAL_SECRET'),
];
