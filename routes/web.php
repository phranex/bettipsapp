<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $indexSetting = \App\Setting::where('name','index')->first();
    $content = null;
    if($indexSetting)
    $content = unserialize($indexSetting->value);
    $levels = null;

    $banners =  \App\Setting::where('name','banner')->pluck('value')->toArray();
    $banners = array_map('unserialize', $banners);
    // dd($banners);
    $packages = \App\Package::where('show_packages', 1)->first();
    if($packages)
        $levels = $packages->levels;

    $tips = \App\Tip::where('result', '!=', null)->paginate(10);

    $faq = \App\Setting::where('name','faq')->first();
    if($faq)
    $faq = unserialize($faq->value);
    //$indexSetting = \App\Setting::where('name','faq')->first();
    $reviews = \App\Review::where('isInstagram', 1)->get();
    // dd($reviews);

    $contact = \App\Setting::where('name','contact')->first();
    if($contact)
    $contact = unserialize($contact->value);


     //dd($packages);
    return view('welcome', compact('content','levels', 'tips','banners', 'faq','reviews','contact'));
})->name('index');

Route::get('/reference', function(){
    $reviews = \App\Review::where('isInstagram', 0)->paginate(20);
    $index = \App\Setting::where('name','index')->first();
    $review = unserialize($index->value);
    $review = @$review['review-description'];
    return view('reviews', compact('reviews','review'));
})->name('reviews');

Route::get('/about-us', function () {
    return view('about');
})->name('about');

Route::get('/how-it-works', function () {
    $indexSetting = \App\Setting::where('name','how')->first();
    $index = \App\Setting::where('name','index')->first();
    $how = null;
    if($index){
    $how = unserialize($index->value);
    $how = @$how['hiw-description'];
    }
    $content = null;
    if($indexSetting)
    $content = unserialize($indexSetting->value);

    // dd($content);

    return view('how-it-works',compact('content','how'));
})->name('how-it-works');

Route::get('/faq', function () {
    $indexSetting = \App\Setting::where('name','faq')->first();
    $content = null;
    if($indexSetting)
    $content = unserialize($indexSetting->value);

    return view('faq', compact('content'));
})->name('faq');

Route::get('/team', function () {
    return view('team');
})->name('team');

Route::get('/blocked', function () {
    if(auth()->user()->blocked == 0)
        return redirect(route('user.dashboard'));
    return view('user.blocked');
})->name('user.blocked');

Route::get('/contact', function () {
    $content = null;
    $contact = null;
    $indexSetting = \App\Setting::where('name','contact')->first();
    if($indexSetting)
    $content = unserialize($indexSetting->value);
    $index = \App\Setting::where('name','index')->first();
    if($index){
        $contact = unserialize($index->value);
        $contact = @$contact['contact-description'];
    }


    return view('contact', compact('content','contact'));
})->name('contact');

Route::get('/auto-payments', function(){
    $indexSetting = \App\Setting::where('name','payments')->first();
    $content = null;
    if($indexSetting){
        $content = unserialize($indexSetting->value);
        $content = $content['editordata'];
    }
    return view('payments',compact('content'));
})->name('payments');

Route::get('/terms', function(){

    $indexSetting = \App\Setting::where('name','terms')->first();

    $content = null;
    if($indexSetting){
        $content = unserialize($indexSetting->value);
        $content = $content['editordata'];
    }
    return view('terms',compact('content'));
})->name('terms');


Route::get('/guarantee', function(){

    $indexSetting = \App\Setting::where('name','guarantee')->first();
    $content = null;
    if($indexSetting){
        $content = unserialize($indexSetting->value);
        $content = $content['editordata'];
    }

    return view('guarantee',compact('content'));
})->name('guarantee');


Route::get('/privacy', function(){

    $indexSetting = \App\Setting::where('name','personal')->first();
    $content = null;
    if($indexSetting){
        $content = unserialize($indexSetting->value);
        $content = $content['editordata'];
    }
    return view('privacy',compact('content'));
})->name('privacy');

Route::post('/contact/send', function (Request $request) {
    request()->validate([
        'name' => 'required',
        'email' => 'required',
        'subject' => 'required',
        'message' => 'required'
    ]);
    $subject = request()->subject;
    $mess = request()->message;
    $name = request()->name;
    $email = request()->email;
    try{
        \Mail::send('emails.contact', ['name' => $name, 'email' => $email, 'mess' =>$mess], function($message) use ($subject)
        {
            $message->to('xeonecek@gmail.com');
            $message->subject($subject);
        });
        return back()->with('success', 'Mail sent.');
    }catch(\Exception $e){

        return back()->with('error', 'Mail could not be sent. Please try again later.');
    }


})->name('contact.store');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


//User Dashboard
Route::get('/user/dashboard', 'UserController@index')->name('user.dashboard');
Route::get('/user/profile', 'UserController@profile')->name('user.profile');
Route::get('/user/balance', 'UserController@balance')->name('user.balance');
Route::get('/user/membership-upgrade', 'UserController@upgrade')->name('user.upgrade');
Route::get('/user/instructions', 'UserController@instructions')->name('user.instructions');

Route::post('/user/change-password', 'UserController@changePasswordMethod')->name('user.change-password');
Route::post('/user/update', 'UserController@update')->name('user.update');
Route::get('/users/logout', function () {
    auth()->logout();
    return redirect(route('login'));
})->name('user.logout');




Route::get('/verify-email/{email}/{token}', 'Auth\RegisterController@verifyEmail')->name('user.verifyEmail');



Route::get('/user/payment/invoice', 'PaymentController@invoice')->name('payment.show-invoice');

Route::post('/user/payment/store/{level?}/{subscription_id?}', 'PaymentController@store')->name('payment.store');
Route::get('/user/payment/store-credit/{level?}', 'PaymentController@storeCredit')->name('payment.store-credit');


Route::post('/user/payment/store/tip/{tip?}/{order?}', 'PaymentController@storeTip')->name('payment.tip.store');
Route::get('/user/tip-purcase/store/{tip}/{payment}', 'TipPaymentController@store')->name('tip-payment.store');
Route::get('/user/package/get-level', 'PackageController@getLevels')->name('package.get-levels');


//mmebership

Route::get('/user/membership/create/{level}/{payment}/{subscription?}', 'MembershipController@store')->name('user.membership.store');


//credit
Route::post('user/credit/topup', 'CreditController@store')->name('credit.store');

/***
 * 1. create a middleware to restrict access to platform until user has an active basic subscription
 * 2. Create subscriptions module
 * 3. Restructing how plans are created from the admin
 * 4.
 */




